#!/bin/bash

if [ ! -z $1 ] ; then

    projects=(fpm-gui fpm-notifier fpmd)

    for project in "${projects[@]}" ; do
        echo "Change version into src/$project/setup.py to $1"
        sed 's/version = "\(.*\)"/version = "'$1'"/g' -i src/$project/setup.py

    done

    projects=(fpm-gui fpm-notifier)

    for project in "${projects[@]}" ; do
        echo "Change version into src/$project/${project/-/_}/__init__.py to $1"
        sed 's/__version__ = "\(.*\)"/__version__ = "'$1'"/g' -i src/$project/${project/-/_}/__init__.py

    done

    echo "Change version into dist/FrugalBuild to $1"
    sed 's/pkgver=\(.*\)/pkgver='$1'/g' -i dist/FrugalBuild


    mkdir fpm-gui-$1

    cp -R {docs,i18n,src} fpm-gui-$1
    cp create_mo.sh fpm-gui-$1

    echo "Create fpm-gui-$1.tar.bz2 archive"
    tar -jcf fpm-gui-$1.tar.bz2 fpm-gui-$1

    rm -Rf fpm-gui-$1

else
    echo "Package version missing"

fi
