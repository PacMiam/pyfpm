#!/bin/bash

translation=(es_ES fr_FR)
projects=(fpm-gui fpm-notifier fpmd)

# Create .pot files
for lang in "${translation[@]}" ; do

    if [ ! -d i18n/$lang ] ; then
        mkdir -p i18n/$lang
    fi

    for project in "${projects[@]}" ; do

        if [ ! -f i18n/$lang/$project.po ] ; then
            msginit -i i18n/$project.pot -o i18n/$lang/$project.po
        fi
    done
done


echo "Update locales"

for project in "${projects[@]}" ; do
    xgettext -k_ -i --strict -s --omit-header -o i18n/$project.pot \
        --copyright-holder="Frugalware" --package-name=$project \
        --package-version="1.1.6" \
        src/$project/${project/-/_}/*/*.py \
        src/$project/${project/-/_}/*.py

    for lang in "${translation[@]}" ; do
        msgmerge -s -U i18n/$lang/$project.po i18n/$project.pot
    done
done
