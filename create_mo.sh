#!/bin/bash

translation=(es_ES fr_FR)
projects=(fpm-gui fpmd)

for project in "${projects[@]}" ; do
    echo "==> Create translation binary for $project"

    for lang in "${translation[@]}" ; do

        if [ ! -d $project/$project/i18n/$lang/LC_MESSAGES ] ; then
            mkdir -p /usr/share/locale/i18n/$lang/LC_MESSAGES
        fi

        echo "    -> $lang translation"
        msgfmt i18n/$lang/LC_MESSAGES/$project.po -o \
            /usr/share/locale/$lang/LC_MESSAGES/$project.mo
    done
done
