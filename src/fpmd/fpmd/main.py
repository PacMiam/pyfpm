# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                               FPMd
#
#  A daemon to use libpacman from dbus write in python 2.7.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os import geteuid
from sys import version_info
from sys import exit as sys_exit

# ------------------------------------------------------------------
#   Method
# ------------------------------------------------------------------

def main ():
    """
    Start fpmd and manage arguments
    """

    if not version_info[0] == 2:
        return "FPMd needs python 2.7 to run"

    # Only root can run fpmd
    if geteuid() == 0:

        from gobject import MainLoop
        from daemon import Daemon

        # Create a loop for FPMd
        loop = MainLoop()
        Daemon(loop)

        # Start FPMd loop
        loop.run()

        return "Close FPMd"

    else:
        return "FPMd needs to be run as root"


if __name__ == "__main__":
    sys_exit(main())
