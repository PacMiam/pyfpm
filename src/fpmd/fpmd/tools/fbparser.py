# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import version_info
from subprocess import STDOUT
from subprocess import check_output

# Files
from os.path import exists
from os.path import expanduser

# Log
from logging import getLogger
from logging.config import fileConfig

# Pyfpm
from fpmd.tools.utils import getTranslation

# Regex
from re import sub
from re import search
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpmd")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Parser (object):

    def __init__ (self, path):
        """
        Constructor

        :param str path: FrugalBuild path
        """

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__files_cache = list()

        self.__data = dict()

        self.__regex = {
            "\$\{?pkgname\}?": "name",
            "\$\{?pkgver\}?": "version",
            "\$\{?url\}?": "url"}

        self.__path = path

        # ------------------------------------
        #   Log
        # ------------------------------------

        self.__log = getLogger()

        # ------------------------------------
        #   FrugalBuild
        # ------------------------------------

        if exists(path):

            # Python 2
            if(version_info[0] < 3):
                with open(str(path), 'r') as FBFile:
                    self.__clean(FBFile)

            # Python 3
            else:
                with open(str(path), 'r', errors="ignore") as FBFile:
                    self.__clean(FBFile)

            self.__parse()

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    def get (self, option=None):
        """
        Return value from option in parser dictionnary

        If no option is set, return the dictionnary instead

        :param str option: Option in the informations dictionnary

        :return: dictionnary or option value
        :rtype: dict or str or None
        """

        if option is None:
            return self.__data

        return self.__data.get(option)


    @property
    def nobuild (self):
        """
        Get nobuild status for current FrugalBuild

        :return: Nobuild status
        :rtype: bool
        """

        parseOption = self.get("options")

        if parseOption is not None and "nobuild" in parseOption:
            return True

        return False

    # ------------------------------------
    #   File
    # ------------------------------------

    def __clean (self, FBFile):
        """
        Remove useless jumpline like backslash in FrugalBuild
        """

        bracket = False
        backslash = False

        index = 0

        for line in FBFile:

            # Get current line without final \n
            lineClean = line[:-1]

            modifyLine = False

            if not bracket and not backslash:

                # Check if there is a start bracket but no final bracket
                if not lineClean.find('(') == -1 and lineClean.find(')') == -1:
                    bracket = True

                # Check if there is a final backslash
                elif search("\s?\\\s?$", line) is not None:
                    backslash = True

                # Don't take empty line
                if len(lineClean) > 0:

                    # Remove backslash from line
                    lineClean = lineClean.replace('\\', '')

                    self.__files_cache.append(
                        lineClean.lstrip().rstrip())
                    index += 1

            elif bracket:

                # Check if there is a final backet
                if not lineClean.find(')') == -1:
                    bracket = False

                modifyLine = True

            elif backslash:
                # Check if there is another backslash
                if search("\s?\\\s?$", line) is None:
                    backslash = False

                modifyLine = True


            if modifyLine:

                # Remove backslash from line
                lineClean = lineClean.replace('\\', '')

                # Add current line to previous line
                self.__files_cache[index - 1] += \
                    " %s" % lineClean.lstrip().rstrip()


    def __parse (self):
        """
        Store all data from cache in a dictionnary
        """

        regexBuild = "^\s*build\s*\(\)\s*"
        regexComment = "^\s*#\s*"
        regexOptions = "^options\s*\+?=\s*\(?\s*(.*)\s*\)?\s*$"
        regexNormalOption = "(.*)=(.*)$"
        regexListOptions = "(.*)=\((.*)\)$"

        regexSplitOptions = "options\s?\+?=\('?\"?(.*)\)$"
        regexSplitInclude = "^\s*Finclude\s*(.*)\s*$"

        regexCompiling = "^\s*#\s*Compiling [tT]ime\s*:\s*([\d\.]+)\s*SBU\s*$"
        regexMaintainer = "^\s*#\s*Maintainer\s*:\s*(.*)\s*<.*>\s*$"
        regexContributor = "^\s*#\s*Contributor\s*:\s*(.*)\s*<.*>\s*$"

        correctName = dict(
            pkgname="name",
            pkgver="version",
            pkgrel="rel",
            pkgdesc="description",
            required="required_by" )

        buildCache = list()
        optionsCache = list()
        includeCache = list()

        maintainersCache = list()
        contributorsCache = list()

        ifValue = False
        buildValue = False


        for value in self.__files_cache:

            # The current line was commented
            if not search(regexComment, value) is None:

                # Compiling time: x SBU
                if not search(regexCompiling, value) is None:
                    self.__data["SBU"] = findall(regexCompiling, value)[0]

                # Maintainer: developper <mail>
                if not search(regexContributor, value) is None:
                    result = findall(regexContributor, value)[0]
                    contributorsCache.append(result.rstrip())

                # Contributor: developper <mail>
                if not search(regexMaintainer, value) is None:
                    result = findall(regexMaintainer, value)[0]
                    maintainersCache.append(result.rstrip())

            # The current line is an option
            elif not search(regexOptions, value) is None:
                options = findall(regexOptions, value)[0][:-1]

                options = sub('[\'\"]', '', options)

                optionsCache.extend(options.split())

            # Build function
            elif not search(regexBuild, value) is None:
                buildValue = True

            elif not search('^\s*}\s*', value) is None:
                buildValue = False

            # If functions
            elif not buildValue and (not search("^\s*if\s*", value) is None or \
                not search("^\s*else\s*", value) is None):
                if not buildValue:
                    ifValue = True

            elif not buildValue and not search("^\s*fi\s*", value) is None:
                if not buildValue:
                    ifValue = False

            # Other lines
            else:

                # Not a build or a if function
                if not buildValue and not ifValue:

                    if search("^_?F.*", value) is None and \
                        search("^sub.*", value) is None:

                        # Get var=(value) data
                        if not search(regexListOptions, value) is None:
                            result = findall(regexListOptions, value)[0]

                            name = result[0]
                            if name in correctName.keys():
                                name = correctName[name]

                            options = result[1].replace('\'', '')
                            options = options.replace('\"', '')

                            self.__data[name] = options.split()

                        # Get var=value data
                        elif not search(regexNormalOption, value) is None:
                            result = findall(regexNormalOption, value)[0]

                            name = result[0]
                            if name in correctName.keys():
                                name = correctName[name]

                            options = sub('[\'\"]', '', result[1])

                            self.__data[name] = options

                    # Get Finclude data
                    elif not search(regexSplitInclude, value) is None:
                        includeCache.extend(findall(regexSplitInclude, value))

                elif buildValue:
                    if search("^\s*\{\s*$", value) is None:
                        buildCache.append(value)


        # Some options has been founded in the FrugalBuild
        if len(optionsCache) > 0:
            self.__data["options"] = optionsCache

        # Some Finclude has been founded in the FrugalBuild
        if len(includeCache) > 0:
            self.__data["include"] = includeCache

        # Build function has been founded in the FrugalBuild
        if len(buildCache) > 0:
            self.__data["build"] = buildCache

        # Some maintainers has been founded in the FrugalBuild
        if len(maintainersCache) > 0:
            self.__data["packager"] = maintainersCache

        # Some contributors has been founded in the FrugalBuild
        if len(contributorsCache) > 0:
            self.__data["contributors"] = contributorsCache

        # Add rel value if not found in FrugalBuild
        if self.get("rel") is None:
            self.__data["rel"] = "1"

        # Add url value if not found in FrugalBuild
        if self.get("url") is None:
            self.__data["url"] = self.__url()


        # Change all bash vars in parserInfo with correct values
        for key in self.__data.keys():

            value = self.__data[key]

            if type(value) is list:

                for element in value:
                    value[value.index(element)] = self.__replace(element)

                self.__data[key] = value

            else:
                self.__data[key] = self.__replace(self.__data[key])


    def __replace (self, element):
        """
        Replace bash vars with correct values
        """

        for regex in self.__regex.keys():

            if self.__regex[regex] in self.__data.keys():
                data = self.__data[self.__regex[regex]]

                try:
                    if search(regex, element) is not None and data is not None:

                        if type(data) is list:
                            data = str().join(data)

                        element = sub(regex, data, element)

                except Exception as error:
                    self.__log.error("Error occur during string replace for "
                        "%s: %s" % (data, error))

        return element


    def __url (self):
        """
        Create url from Finclude data

        :return: Package url
        :rtype: string
        """

        result = check_output(". /usr/lib/frugalware/fwmakepkg && "
            ". /etc/makepkg.conf && source %s && echo $url" % self.__path,
            shell=True)

        if len(result) > 0:
            return result[:-1]

        return None
