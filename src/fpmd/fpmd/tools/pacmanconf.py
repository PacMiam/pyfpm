# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           pacmanconf.py
#
#  Manage pacman-g2 configuration files
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os import mkdir
from os import remove
from os.path import exists
from os.path import expanduser

from glob import glob

from hashlib import new as hash_new

# Log
from logging import getLogger
from logging.config import fileConfig

# Pyfpm
from fpmd.tools import *
from fpmd.tools.utils import getType
from fpmd.tools.utils import getTranslation

# Regex
from re import match
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpmd")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class PacmanConfig (object):

    def __init__ (self, path=PACMAN_CONF_FILE):
        """
        Constructor
        """

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__index = 0

        self.__hash_files = dict()

        # ------------------------------------
        #   Public variables
        # ------------------------------------

        self.path = path

        self.options = dict()
        self.repositories = list()

        # ------------------------------------
        #   Log
        # ------------------------------------

        self.__log = getLogger()

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        self.__parse()

        self.__load_repositories()

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    @property
    def hash (self):
        return self.__hash_files


    def option (self, section, option=None):
        """
        Get a specific option value from pacman-g2 configuration

        :param str section: Configuration section or Section option
        :param str option: Section option (Default: None)

        :return: data
        :rtype: str or None
        """

        if section in self.options and option is None:
            return self.options[section]

        elif option is None:
            option = section
            section = "options"

        try:
            return self.options[section].value.get(option)

        except KeyError:
            self.__log.error(_("The '%s' section not exist" % section))

        return None


    def repository (self, index):
        """
        Get a specific repository in repositories list
        """

        if not index in range(len(self.repositories)):
            return None

        return self.repositories[index]

    # ------------------------------------
    #   Configuration
    # ------------------------------------

    def __parse (self):
        """
        Read all the config file content
        """

        data = list()
        section = str()

        if exists(self.path):

            # ------------------------------------
            #   Parse configuration file
            # ------------------------------------

            pipe = open(self.path, 'r')
            pipe.seek(0)

            for line in pipe.readlines():
                value = None

                # Remove line return
                line = line[:-1]

                if len(line) > 0:

                    # Commented line
                    if line[0] == "#":
                        if not line.find('=') is -1 or \
                            not line.find('[') is -1:
                            value = line[1:].split('=')
                            value.append(True)

                    # Uncommented and not empty line
                    else:
                        value = line.split('=')
                        value.append(False)

                if not value is None:
                    data.append(value)

            pipe.close()

            # ------------------------------------
            #   Set data into cache
            # ------------------------------------

            for value in data:

                # Get section (ex: [options])
                if not value[0].find('[') is -1 and type(value[1]) is bool:
                    section = value[0][1:-1]

                    self.options[section] = PacmanOption(dict(), value[1])

                # Special cases (No value for this options)
                elif value[0] in ["NoPassiveFtp", "UseSyslog"]:
                    self.options["options"][value[0]] = PacmanOption(
                        None, value[1])

                # Get option (ex: Include = /path/to/repos/frugalware)
                else:
                    option = value[0].strip()

                    if option == "Include":

                        # Add a new Include option to cache
                        option = "Include%s" % self.__index
                        self.__index += 1

                        currentSection = self.options["options"].value

                    else:
                        currentSection = self.options[section].value

                    currentSection[option] = PacmanOption(
                        value[1].strip(), value[2])


    def generate_configuration (self, hashmode="SHA1"):
        """
        Generate a new pacman-g2 config file

        :return pacman-g2 paths:
        :rtype: array(str)
        """

        data = list()

        if not exists(TMP_PATH):
            try:
                mkdir(TMP_PATH)

            except Exception as error:
                self.__log.error(_("Can't create %(path)s: %(error)s") % {
                    path: TMP_PATH, error: str(error) })

        else:
            try:
                for element in glob("%s/*" % TMP_PATH):
                    remove(element)

            except Exception as error:
                self.__log.error(_("Can't clean %(path)s: %(error)s") % {
                    path: TMP_PATH, error: str(error) })

        # ------------------------------------
        #   Configuration file
        # ------------------------------------

        try:
            self.__log.debug(
                _("Start to create pacman-g2 configuration files"))

            pipe = open(TMP_CONF_FILE, 'w')
            pipe.seek(0)

            pipe.write(PACMAN_CONF_HEADER)

            pipe.write(self.__generate_section("options"))

            pipe.close()

            # ------------------------------------
            #   Hash
            # ------------------------------------

            pipe = open(TMP_CONF_FILE, 'r')

            sha1 = hash_new(hashmode)
            sha1.update(pipe.read())

            self.__hash_files[TMP_CONF_FILE] = sha1.hexdigest()

            pipe.close()

        except Exception as error:
            self.__log.error(
                _("Can't generate configuration files: %s") % str(error))

        # ------------------------------------
        #   Repositories files
        # ------------------------------------

        for repository in self.repositories:

            try:
                pipe = open("%s/%s" % (TMP_PATH, repository.name), 'w')
                pipe.seek(0)

                pipe.write("#\n# %s repository\n#\n\n" % repository.name)

                pipe.write("[%s]\n" % repository.name)
                for mirror in repository.mirrors:

                    commented = ''
                    if mirror.commented:
                        commented = '#'

                    pipe.write("%sServer = %s\n" % (commented, mirror.value))

                pipe.close()

                # ------------------------------------
                #   Hash
                # ------------------------------------

                pipe = open("%s/%s" % (TMP_PATH, repository.name), 'r')

                sha1 = hash_new(hashmode)
                sha1.update(pipe.read())

                self.__hash_files["%s/%s" % (
                    TMP_PATH, repository.name)] = sha1.hexdigest()

                pipe.close()

            except Exception as error:
                self.__log.error(_("Can't generate %(name)s repository file: "
                    "%(error)s") % dict(name=repository.name,
                    error=str(error)))


    def __generate_section (self, section="options"):
        """
        Create the text content for a specific section

        :param str section: Configuration section title (Default: options)

        :return: Config data
        :rtype: str
        """

        section_text = str()

        data = self.options[section]

        if len(data.value) > 0:

            # ------------------------------------
            #   Section
            # ------------------------------------

            commented = ''
            if data.commented:
                commented = '#'

            section_text += "\n%s[%s]\n" % (commented, section)

            # ------------------------------------
            #   Options
            # ------------------------------------

            for option in data.value:

                # Include options are special and need a specific work
                if option.find("Include") == -1:

                    # Special cases
                    if option in ["NoPassiveFtp", "UseSyslog"]:

                        if data.commented:
                            section_text += '#'

                        section_text += '%s\n' % option

                    # Normal cases
                    else:

                        if data.value[option].commented:
                            section_text += '#'

                        section_text += '%s = %s\n' % (
                            option, data.value[option].value)

            # ------------------------------------
            #   Repositories
            # ------------------------------------

            if section == "options":

                for repository in self.repositories:

                    if repository.path is None:
                        repository.path = "%s/%s" % (
                            PACMAN_REPOS_PATH, repository.name)

                    if repository.commented:
                        section_text += '#'

                    section_text += 'Include = %s\n' % repository.path

        return section_text

    # ------------------------------------
    #   Repositories
    # ------------------------------------

    def __load_repositories (self):
        """
        Get the repositories list

        :return: Repositories list
        :rtype: array(boolean status, str name, array(bool serverStatus, \
            str serverName))
        """

        for section in self.options:
            data = self.options[section].value

            for option in data:

                if section == "options" and not option.find("Include") == -1:

                    repository = PacmanRepository(
                        int(option.split("Include")[-1]),
                        data[option].value.split('/')[-1],
                        data[option].commented)

                    repository.path = data[option].value

                    self.__parse_repository(repository)

                    self.repositories.append(repository)

                elif not match("^Server$", option) is None:

                    repository = PacmanRepository(-1, section,
                        data[option].commented)

                    repository.mirrors.append(PacmanOption(
                        data[option].value, data[option].commented))

                    self.repositories.append(repository)

        # Sort repositories list
        self.repositories = sorted(self.repositories, key=lambda x: x.index)

        self.__index = len(self.repositories)


    def __parse_repository (self, repository):
        """
        From a pacman-g2 repositories file, get all the repositories

        :param str fileName: Repository file name

        :return: Repository servers list
        """

        if exists(repository.path):

            pipe = open(repository.path, 'r')
            pipe.seek(0)

            for line in pipe.readlines():

                line = line[:-1]

                if not line.find("Server") == -1:

                    commented = False
                    if line[0] == "#":
                        commented = True

                    path = line.split('=')[-1].strip()
                    if path[-1] == '/':
                        path = path[:-1]

                    repository.mirrors.append(PacmanOption(path, commented))

            pipe.close()


    def repository_add (self, repository):
        """
        Add a new repository into pacman-g2 config

        :param PacmanRepository repository: Repository instance
        """

        if type(repository) is PacmanRepository:
            repository.index = self.__index

            data = self.options["options"].value
            data["Include%s" % self.__index] = PacmanOption(
                "%s/%s" % (PACMAN_REPOS_PATH, repository.name),
                repository.commented)

            self.__index += 1

            self.repositories.append(repository)

        else:
            self.__log.error(
                _("Not a PacmanRepository object, can't add repository"))


    def repository_remove (self, repository):
        """
        Add a new repository into pacman-g2 config

        :param PacmanRepository repository: Repository instance
        """

        if type(repository) is PacmanRepository:
            self.repositories.remove(repository)

        else:
            self.__log.error(
                _("Not a PacmanRepository object, can't remove repository"))


class PacmanOption (object):

    def __init__ (self, value, commented):
        """
        Constructor
        """

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.value = value
        self.commented = commented


class PacmanRepository (object):

    def __init__ (self, index, name, commented):
        """
        Constructor
        """

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.name = name
        self.path = None
        self.index = index
        self.mirrors = list()
        self.commented = commented

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------


    def mirror (self, index):
        """
        Get a specific mirror in mirrors list
        """

        if not index in range(len(self.mirrors)):
            return None

        return self.mirrors[index]

    # ------------------------------------
    #   Methods
    # ------------------------------------

    def mirror_add (self, path, commented):
        """
        Add a mirror

        :param str path: Mirror path
        :param bool commented: Mirror status
        """

        self.mirrors.append(PacmanOption(path, commented))
