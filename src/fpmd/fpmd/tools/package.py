# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           package.py
#
#  Invoke FPMd methods
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Dbus
from dbus import Interface
from dbus import SystemBus
from dbus import DBusException
from dbus.mainloop.glib import DBusGMainLoop

# Files
from os import popen
from os.path import exists
from os.path import basename
from os.path import expanduser

from glob import glob

# Log
from logging import getLogger
from logging.config import fileConfig

# Fpmd
from fpmd import FW_STABLE
from fpmd import FW_CURRENT

from fpmd.tools import *
from fpmd.tools.utils import color
from fpmd.tools.utils import formatSize
from fpmd.tools.utils import splitRegex
from fpmd.tools.utils import checkString
from fpmd.tools.utils import getTranslation
from fpmd.tools.fbparser import Parser

# System
from sys import version_info

# ------------------------------------------------------------------
#   Fix
# ------------------------------------------------------------------

if(version_info[0] >= 3):
    long = int

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpmd")

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def start_daemon ():
    """
    Run dbus connection
    """

    DBusGMainLoop(set_as_default=True)

    packageBus = SystemBus()

    try:
        packageBus.get_object(BUSNAME, BUSPATH, introspect=False)

    except DBusException:
        return None

    return packageBus

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Database (object):

    def __init__ (self):
        """
        Init Package class

        :return: Object Package
        :rtype: Package
        """

        DBusGMainLoop(set_as_default=True)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.debug = False
        self.interface = None

        self.__cache_installed_packages = list()

        # ------------------------------------
        #   Log
        # ------------------------------------

        self.__log = getLogger()

        # ------------------------------------
        #   Dbus
        # ------------------------------------

        try:
            self.__dbus = start_daemon()

            self.__dbus_object = self.__dbus.get_object(
                BUSNAME, BUSPATH, introspect=False)

        except DBusException:
            self.__log.critical(_("Can't connect to FPMd daemon"))

        # ------------------------------------
        #   Fpmd methods
        # ------------------------------------

        # Daemon
        self.reload_daemon = self.__dbus_object.get_dbus_method(
            "reload_daemon", BUSNAME)

        # Configuration
        self.__configuration_update = self.__dbus_object.get_dbus_method(
            "configuration_update", BUSNAME)
        self.configuration_value = self.__dbus_object.get_dbus_method(
            "configuration_value", BUSNAME)

        # Repositories
        self.repositories = self.__dbus_object.get_dbus_method(
            "repositories", BUSNAME)

        # Groups
        self.groups = self.__dbus_object.get_dbus_method(
            "groups", BUSNAME)

        # Packages list
        self.packages = self.__dbus_object.get_dbus_method(
            "packages", BUSNAME)
        self.__packages_installed = self.__dbus_object.get_dbus_method(
            "packages_installed", BUSNAME)
        self.packages_search = self.__dbus_object.get_dbus_method(
            "packages_search", BUSNAME)
        self.packages_upgrade = self.__dbus_object.get_dbus_method(
            "packages_upgrade", BUSNAME)

        # Packages data
        self.__package = self.__dbus_object.get_dbus_method(
            "package", BUSNAME)
        self.package_sha1sums = self.__dbus_object.get_dbus_method(
            "package_sha1sums", BUSNAME)
        self.package_files = self.__dbus_object.get_dbus_method(
            "package_files", BUSNAME)
        self.package_status = self.__dbus_object.get_dbus_method(
            "package_status", BUSNAME)
        self.__package_fpm = self.__dbus_object.get_dbus_method(
            "package_fpm", BUSNAME)

        # Nobuild
        self.nobuild = self.__dbus_object.get_dbus_method(
            "nobuild", BUSNAME)
        self.nobuild_reset = self.__dbus_object.get_dbus_method(
            "nobuild_reset", BUSNAME)
        self.nobuild_fst_path = self.__dbus_object.get_dbus_method(
            "nobuild_fst_path", BUSNAME)
        self.nobuild_source_name = self.__dbus_object.get_dbus_method(
            "nobuild_source_name", BUSNAME)

        # Transactions
        self.__system_cache = self.__dbus_object.get_dbus_method(
            "system_cache", BUSNAME)
        self.__system_upgrade = self.__dbus_object.get_dbus_method(
            "system_upgrade", BUSNAME)
        self.__system_sync = self.__dbus_object.get_dbus_method(
            "system_sync", BUSNAME)
        self.__system_remove = self.__dbus_object.get_dbus_method(
            "system_remove", BUSNAME)

        self.__dbus_object.connect_to_signal(
            "sendSignal", self.signal_received)

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    @property
    def dbus (self):
        return self.__dbus_object

    @property
    def installed (self):
        return self.__cache_installed_packages

    # ------------------------------------
    #   Configuration
    # ------------------------------------

    def configuration_update (self, path, hashmode="SHA1"):
        """
        Change pacman-g2 config

        :param dict(str=str) path: Generated files and them hash sums
        :param str hashmode: Hash mode to check file differences

        :return: Result
        :rtype: bool
        """

        self.__configuration_update(path, hashmode)

        return True


    # ------------------------------------
    #   Repositories
    # ------------------------------------

    def repositories_index (self, repository=None):
        """
        Get frugalware repo index (stable/current)

        :param str repository: Repository name (default "")

        :return: Repository index
        :rtype: int
        """

        repositories = self.repositories()

        if repository is None:
            if FW_STABLE in repositories:
                return self.repositories(FW_STABLE)

            elif FW_CURRENT in repositories:
                return self.repositories(FW_CURRENT)

        else:
            if repository in repositories:
                return self.repositories(repository)

        return None


    def repository_official (self):
        """
        Get Frugalware actual repo name (stable or current)

        :return: Repository name
        :rtype: str or None
        """

        repoList = self.repositories()

        if FW_STABLE in repoList:
            return FW_STABLE

        elif FW_CURRENT in repoList:
            return FW_CURRENT

        else:
            return None

    # ------------------------------------
    #   Installed packages
    # ------------------------------------

    def packages_installed (self, reset=False):
        """
        Return installed packages list

        :param bool reset: Reset installed packages list

        :return: Packages list
        :rtype: list
        """

        if reset or len(self.__cache_installed_packages) == 0:
            self.__cache_installed_packages = self.__packages_installed()

        return self.__cache_installed_packages

    # ------------------------------------
    #   Package
    # ------------------------------------

    def __get_missing_dependancies (self, repository, name):
        """
        Get missing dependencies for a specific package

        :param str repository: Repository name
        :param str name: Package name

        :return: Packages list
        :rtype: list
        """

        listPackages = list()

        data = self.__package(repository, name)
        if data is None:
            return listPackages

        data = Package(data)

        # Get package dependencies
        listDependencies = data.depends

        if not listDependencies is None and len(listDependencies) > 0:
            listDependencies.sort()

            for depend in listDependencies:

                # Split a package when there is a version condition
                package = splitRegex(REGEX_MISSING_DEPENDENCIES, depend)
                if len(package) > 1:
                    depend = package[0]

                # Get only not installed dependencies
                if not self.package_installed(depend)[0]:
                    listPackages.append(str(depend))

        return listPackages


    def package (self, repository, name):
        """
        Get informations about the package

        :param str repository: Repository name
        :param str name: Package name

        :return: Package informations
        :rtype: Package or None
        """

        fstData = None
        fstFrugalbuild = None

        updated = False

        # Read data from pacman database
        data = self.__package(repository, name)
        if data is None:
            return None

        data = Package(data)

        if exists("/usr/bin/repoman"):

            # Get correct fst repository name
            fstRepository = self.nobuild_source_name(repository)

            # Set fst source path
            fstSource = "/var/fst/%s/source" % fstRepository

            if exists(fstSource):

                if data.name is None:
                    data.nobuild = True

                    data.group = self.__get_nobuild_group(repository, name)

                else:
                    folders = glob("%s/*" % fstSource)

                    for element in folders:
                        folders[folders.index(element)] = basename(element)

                    for group in data.groups:

                        if group in folders:
                            data.group = group
                            break

                fstFrugalbuild = "%s/%s/%s/FrugalBuild" % (
                    fstSource, data.group, name)

                if exists(fstFrugalbuild):

                    # Parse FrugalBuild
                    fstData = Parser(fstFrugalbuild).get()

                    if data.nobuild:
                        data.refresh(fstData)

                        status, version = self.package_installed(data.name)

                        if version is None:
                            version = data.version

                        data.installed = status
                        data.version = "%s-%s" % (
                            version, fstData.get("rel", "1"))

                    elif data.installed:
                        status, version = self.package_installed(data.name)

                        data.installed = status
                        data.version = version

                        updated = True

                    if data.url is None:
                        data.url = fstData.get("url", None)

                    data.frugalbuild = fstFrugalbuild

        # Encode url
        if (not data.installed and not data.nobuild) or updated:

            if not data.frugalbuild is None and not data.url is None:

                if type(data.url) == list:
                    data.url = data.url[0]

        # Depends
        if not data.frugalbuild is None and type(data.depends) == list and \
            type(fstData.get("rodepends")) == list:
            data.depends = data.depends.extend(fstData.get("rodepends"))

        # Size
        if data.installed:
            data.size = formatSize(data.size)

        elif not data.nobuild:
            data.compress_size = formatSize(data.compress_size)
            data.uncompress_size = formatSize(data.uncompress_size)

        # SBU
        if not fstFrugalbuild is None and not fstData is None:
            data.sbu = fstData.get("SBU", "0")

        return data


    def package_fpm (self, name, version):
        """
        Look in packages cache if wanted fpm exist

        :param str name: Package name
        :param str version: Package version

        :return: Cache status
        :rtype: bool
        """

        # Get system arch from /bin/arch
        arch = popen("arch").readlines()[0][:-1]

        return self.__package_fpm(name, version, arch)


    def package_missing_dependancies (self, repository, name):
        """
        Get all not installed packages for a specific package

        :param str repository: Repository name
        :param str name: Package name

        :return: Packages list
        :rtype: list
        """

        listPackages = list()

        packagesToCheck = self.__get_missing_dependancies(repository, name)

        while len(packagesToCheck) > 0:
            data = packagesToCheck[0]

            if not data in listPackages:
                listPackages.append(data)

                for package in self.__get_missing_dependancies(
                    repository, data):

                    if not package in listPackages:
                        packagesToCheck.append(package)

            packagesToCheck.remove(data)

        return listPackages


    def package_update_version (self, pkgName, pkgVersion, updateList):
        """
        Get the updated package version if available

        :param str pkgName: Package name
        :param str pkgVersion: Package version
        :param array(str,str) updateList: Updated packages list

        :return: Update version
        :rtype: str
        """

        for package in updateList:
            if package[0] == pkgName and not package[1] == pkgVersion:
                return package[1]

        return None


    def package_installed (self, pkgName):
        """
        Return pkgName install status

        :param str pkgName: Package name

        :return: Package status
        :rtype: bool
        """

        for package in self.packages_installed():
            if package[0] == pkgName:
                return True, package[1]

        return False, None


    def system_reboot_needed (self, flags):
        """
        Check if some packages from base group has been modify

        :param dict() flags: Pacman flags

        :return: Reboot status
        :rtype: bool
        """

        if not flags.get("install_download", False):

            listInstallation = flags.get("list_installation", list())
            listRemove = flags.get("list_remove", list())

            for listPackages in [listInstallation, listRemove]:
                if len(listPackages) > 0:
                    for name in listPackages:
                        data = self.__package(self.repository_official(), name)
                        if data is None:
                            return False

                        data = Package(data)
                        if not data.groups is None and "base" in data.groups:
                            return True

        return False

    # ------------------------------------
    #       Nobuild packages
    # ------------------------------------

    def __get_nobuild_group (self, repository, name):
        """
        Get group for a specific nobuild package

        :param str repository: Source repository name
        :param str name: Package name

        :return: Group name
        :rtype: str
        """

        nobuildList = self.nobuild(repository)

        for group in nobuildList:
            if name in nobuildList[group]:
                return group

        return None

    # ------------------------------------
    #       Signals
    # ------------------------------------

    def signal_received (self, signal):
        """
        Received signal from Fpmd
        """

        if not self.interface is None:

            message = ""
            endMode = False
            parent = True

            mode = signal[0]
            action = signal[1]

            # Start action
            if mode == "start":

                # Update action
                if action == "update":
                    self.interface.setMaxProgress(signal[2])


            # Event message
            elif mode == "event":

                # Install action
                if action == "install":
                    print (", ".join(signal))

                # Update action
                elif action == "update":
                    repositoryName = signal[2]

                    message = _("Update %s repository" % repositoryName)

                self.interface.updateProgressbar(message)


            # Progress message
            elif mode == "progress":

                # Update action
                if action == "update":
                    repositoryName = str(signal[2])
                    error = signal[3]

                    if error == 0:
                        message = \
                            _("%s was updated successfully" % repositoryName)

                    elif error == 1:
                        message = _("%s is up-to-date" % repositoryName)

                    elif error == -1:
                        message = _("Failed to update %s" % repositoryName)

                if len(message) > 0:
                    message = "» %s" % message

                    parent = False


            # End action
            elif mode == "end":

                if action == "install":
                    message = _("Finish to install packages")

                elif action == "remove":
                    message = _("Finish to remove packages")

                elif action == "update":
                    message = _("Finish to synchronize pacman-g2 databases")

                self.interface.updateProgressbar(message)

                self.interface.closeInstance()

                endMode = True


            # Show message
            if len(message) > 0:
                self.interface.showMessage(message, parent)


    # ------------------------------------
    #       Transactions
    # ------------------------------------

    def __check_authentification (self, service, timeout=86400):
        """
        Use polkit to know if user is authorized to use function

        :return: Auth status
        :rtype: bool
        """

        proxy = self.__dbus.get_object(AUTHNAME, AUTHPATH)

        (is_authorized, is_challenge, details) = proxy.CheckAuthorization(
            ("system-bus-name", dict(name=self.__dbus.get_unique_name())),
            service, dict(), 1, str(), dbus_interface=INTERFACE,
            timeout=timeout)

        return bool(is_authorized)


    def system_upgrade (self):
        """
        Update pacman-g2 databases

        :return: Action status
        :rtype: bool
        """

        try:
            self.__system_upgrade()
            return True

        except:
            return False


    def system_cache (self, mode=0):
        """
        Clean pacman-g2 cache

        :param int mode: Clean mode (default only old package (0))

        :return: Action status
        :rtype: bool
        """

        try:
            self.__system_cache(mode)
            return True

        except:
            return False


class Package (object):

    def __init__ (self, data):
        """
        Constructor
        """

        self.name = None
        self.version = None
        self.description = None
        self.url = None
        self.install_date = None
        self.packager = None

        self.sbu = None
        self.frugalbuild = None
        self.group = None

        self.groups = None
        self.depends = None
        self.provides = None
        self.replaces = None
        self.required_by = None
        self.conflicts = None

        self.installed = False
        self.nobuild = False

        self.size = 0
        self.compress_size = 0
        self.uncompress_size = 0

        self.refresh(data)


    def refresh (self, data):
        """
        Refresh data for a nobuild package
        """

        self.name = data.get("name", None)
        self.version = data.get("version", None)
        self.description = data.get("description", None)
        self.url = data.get("url", None)

        self.install_date = data.get("install_date", None)

        self.packager = data.get("packager", None)
        self.sbu = data.get("SBU", None)
        self.frugalbuild = data.get("frugalbuild", None)

        self.groups = data.get("groups", None)

        self.depends = data.get("depends", None)
        self.provides = data.get("provides", None)
        self.replaces = data.get("replaces", None)
        self.required_by = data.get("required_by", None)
        self.conflicts = data.get("conflicts", None)

        self.installed = data.get("installed", False)

        self.size = float(data.get("size", 0))
        self.compress_size = float(data.get("compress_size", 0))
        self.uncompress_size = float(data.get("uncompress_size", 0))


    def __str__ (self):
        """
        Print all informations about Package
        """

        text = color(u"\u2716 ", "red")
        if bool(self.installed):
            text = color(u"\u2714 ", "green")

        text += u"%s - %s" % (self.name, self.version)
        text += u"\nDescription: %s" % (self.description)
        text += u"\nWebsite: %s" % (self.url)

        return text


class Command (object):

    def __init__ (self):
        """
        Constructor
        """

        self.command = str()

        self.__command_sync = False
        self.__command_cache = False
        self.__command_chroot = False
        self.__command_upgrade = False
        self.__command_repoman = False
        self.__command_package = False

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    def sync (self):
        self.__command_sync = not self.__command_sync

    def cache (self):
        self.__command_cache = not self.__command_cache

    def chroot (self):
        self.__command_chroot = not self.__command_chroot

    def upgrade (self):
        self.__command_upgrade = not self.__command_upgrade

    def repoman (self):
        self.__command_repoman = not self.__command_repoman

    def package (self):
        self.__command_package = not self.__command_package

    # ------------------------------------
    #   Methods
    # ------------------------------------

    def generate (self, flags=dict()):
        """
        Generate correct command from flags
        """

        # Upgrade databases
        if self.__command_upgrade:

            self.command = u"pacman -Sy"
            if flags.get("repoman", False):
                self.command += u" && repoman update"

        # Merge a package
        elif self.__command_repoman:
            if flags.get("name") == None:
                return False

            self.command = u"repoman merge %s" % (flags.get("name"))

        # Install a local package
        elif self.__command_package:
            if flags.get("path") == None:
                return False

            flag = u"-A"
            if flags.get("status", False):
                flag = u"-U"

            self.command = u"pacman %s --noconfirm %s" % (
                flag, flags.get("path"))

        # Update 32 bits chroot
        elif self.__command_chroot:
            self.command = u"fw32-update && fw32-upgrade"

        # Clean packages cache
        elif self.__command_cache:
            flag = u"c"
            if flags.get("mode", False):
                flag = u"cc"

            self.command = u"pacman -S%s --noconfirm" % (flag)

        # Install or remove packages from databases
        elif self.__command_sync:

            # Packages to remove
            if len(flags.get("list_remove", list())) > 0:

                flag = str()
                if flags.get("delete_noconfirm", False):
                    flag += u" --noconfirm "
                if flags.get("delete_deps", False):
                    flag += u" -d "
                if flags.get("delete_force", False):
                    flag += u" -f "

                self.command += u"pacman -R %s %s" % (
                    flag, ' '.join(flags.get("list_remove")))

            if len(flags.get("list_installation", list())) > 0:

                if len(flags.get("list_remove", list())) > 0:
                    self.command += u" && "

                # pacman has an update available
                if "pacman-g2" in flags.get("list_installation"):
                    self.command += "pacman -S pacman-g2"

                    flags.get("list_installation").remove("pacman-g2")

                    if len(flags.get("list_installation")) > 0:
                        self.command += " && "

                if len(flags.get("list_installation")) > 0:

                    flag = str()
                    if flags.get("install_noconfirm", False):
                        flag += u" --noconfirm "
                    if flags.get("install_deps", False):
                        flag += u" -d "
                    if flags.get("install_download", False):
                        flag += u" -w "
                    if flags.get("install_force", False):
                        flag += u" -f "

                    self.command += u"pacman -S %s %s" % (
                        flag, ' '.join(flags.get("list_installation")))

        # No command
        else:
            return False

        return True
