# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           __init__.py
#
#  PyFPM tools - Common values
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   DBus
# ------------------------------------------------------------------

BUSNAME = "org.frugalware.fpmd"
OBJPATH = "/org/frugalware/fpmd/object"

# ------------------------------------------------------------------
#   Path
# ------------------------------------------------------------------

FST_ROOT = "/var/fst"

DBPATH_FILE = "/var/lib/pacman-g2"
LOGPATH_FILE = "/var/log/pacman-g2.log"
CACHEDIR_FILE = "/var/cache/pacman-g2/pkg"

CONFIGURATION_PATH = "/etc/pacman-g2.conf"
REPOSITORIES_PATH = "/etc/pacman-g2/repos"

FPMD_LOG_PATH = "/var/log/fpmd.log"
FPMD_LOG_CONFIG = "/etc/fpmd/logging.conf"

# ------------------------------------------------------------------
#   Binaries
# ------------------------------------------------------------------

FW32_UPDATE_BIN = "/usr/sbin/fw32-update"
FW32_UPGRADE_BIN = "/usr/sbin/fw32-upgrade"

REPOMAN_BIN = "/usr/bin/repoman"

# ------------------------------------------------------------------
#   Pacman options
# ------------------------------------------------------------------

DBPATH_FILE_NAME = "DBPath"
LOGPATH_FILE_NAME = "LogFile"
CACHEDIR_FILE_NAME = "CacheDir"

FW_LOCAL = "local"
FW_STABLE = "frugalware"
FW_CURRENT = "frugalware-current"

# ------------------------------------------------------------------
#   Regex
# ------------------------------------------------------------------

INSTALLED = "^([\w?\-?\.?\+?]+)-([\d+\.?|\d+~?|\w]+-\d+)+$"
