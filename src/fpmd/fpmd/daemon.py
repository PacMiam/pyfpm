# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                               FPMd
#
#  A daemon to use libpacman from dbus write in python 2.7.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import exit as sys_exit
from subprocess import call
from gobject import threads_init as gobject_threads_init

# Dbus
from dbus import SystemBus
from dbus import DBusException
from dbus import Int32 as dbus_Int32
from dbus import String as dbus_String
from dbus.service import BusName
from dbus.service import signal as BusSignal
from dbus.service import method as BusMethod
from dbus.service import Object as BusObject
from dbus.mainloop.glib import DBusGMainLoop
from dbus.mainloop.glib import threads_init as dbus_threads_init

# Files
from os import listdir
from glob import glob
from shutil import move
from shutil import copyfile
from os.path import isdir
from os.path import isfile
from os.path import exists
from os.path import dirname
from os.path import basename
from hashlib import new as hash_new
from ConfigParser import NoOptionError
from ConfigParser import SafeConfigParser

# Log
from logging import getLogger
from logging.config import fileConfig

# Pacman
from ctypes import cdll
from ctypes import c_int
from ctypes import c_char_p
from pacman import *

# Regex
from re import match as re_match
from re import findall as re_findall

# Tools
from fpmd import *
from fpmd.tools.pacmanconf import PacmanConfig

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Daemon (BusObject):

    def __init__ (self, loop):
        """
        Constructor
        """

        self.loop = loop

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__repositories = list()
        self.__databases = list()

        self.__sourceName = { FW_STABLE: "stable", FW_CURRENT: "current" }

        self.__default_paths = {
            "DBPath": DBPATH_FILE,
            "LogFile": LOGPATH_FILE,
            "CacheDir": CACHEDIR_FILE }

        # Nobuild cache
        self.__nobuild = dict()
        self.nobuildListDone = False

        # Installed packages
        self.__installed = list()

        # ------------------------------------
        #   Logging module
        # ------------------------------------

        if exists(FPMD_LOG_CONFIG):
            fileConfig(FPMD_LOG_CONFIG)
            self.__log = getLogger("fpmd")

        else:
            self.__log = getLogger("fpmd")
            self.__log.critical("%s not exist" % FPMD_LOG_CONFIG)
            sys_exit()

        # ------------------------------------
        #   Pacman configuration
        # ------------------------------------

        self.config = PacmanConfig(CONFIGURATION_PATH)

        # ------------------------------------
        #   DBus
        # ------------------------------------

        self.__log.info("Start DBus loop")

        # Dbus loop
        DBusGMainLoop(set_as_default=True)

        gobject_threads_init()
        dbus_threads_init()

        # Dbus connection
        pacmanBus = SystemBus()

        BusObject.__init__(self, BusName(BUSNAME, bus=pacmanBus), OBJPATH)

        try:
            proxy = pacmanBus.get_object(BUSNAME, OBJPATH, introspect=False)

        except DBusException:
            self.__log.critical("DBus interface is not available")
            sys_exit()

        self.__load_daemon()

    # ------------------------------------
    #   Daemon
    # ------------------------------------

    def __load_daemon (self):
        """
        Start daemon correctly
        """

        self.__log.info("Initialize pacman instance")

        # Initialize pacman-g2 instance
        if pacman_initialize('/') == -1:
            self.__log.error(
                "Can't initialize fpmd: %s" % str(pacman_print_error()))
            self.__close_daemon()

        # Set some important pacman-g2 options
        if pacman_set_option(OPT_LOGMASK, LOG_WARNING) == -1:
            self.__log.error(
                "Can't set log mask: %s" % str(pacman_print_error()))
            self.__close_daemon()

        # Get repositories from pacman configuration file
        self.__load_repositories()

        # Set installed packages cache
        self.__load_installed_packages()

        # ------------------------------------
        #   NoBuild
        # ------------------------------------

        if exists(REPOMAN_BIN):

            for repository in self.__repositories:

                if exists("%s/%s" % (FST_ROOT,
                    self.nobuild_source_name(repository))):

                    if not repository in self.__nobuild:
                        self.__nobuild[repository] = dict()

                    self.__load_nobuild(repository)


    def __close_daemon (self):
        """
        Close FPMd instance properly
        """

        pacman_release()

        self.loop.quit()


    @BusMethod (BUSNAME)
    def reload_daemon (self):
        """
        Reset pacman-g2 instance and update informations
        """

        # Close actual pacman-g2 instance
        pacman_release()

        # Reset cache arrays
        self.__repositories = list()
        self.__databases = list()
        self.__installed = list()

        self.__nobuild = dict()

        self.__load_daemon()


    # ------------------------------------
    #   Conversion
    # ------------------------------------

    def __char_to_str (self, value):
        """
        Get string from void char*
        """

        return str(void_to_char(value))


    def __long_to_str (self, value):
        """
        Get long from void char*
        """

        return str(void_to_long(value))


    def __list (self, package, typeInfo):
        """
        Get informations about a package and put them into a string
        """

        content = list()

        index = pacman_list_first(void_to_PM_LIST(
            pacman_pkg_getinfo(package, typeInfo)))

        while not index == None:

            name = self.__char_to_str(
                pacman_list_getdata(void_to_PM_LIST(index)))

            if not name in content:
                content.append(name.encode('utf-8'))

            index = pacman_list_next(void_to_PM_LIST(index))

        if len(content) > 0:
            return content

        return ""


    def __unicode (self, package, typeInfo, error="strict"):
        """
        Get unicode string for a specific package information
        """

        try:
            return unicode(self.__string(package, typeInfo), errors=error)

        except UnicodeDecodeError:
            return self.__string(package, typeInfo)

        return None


    def __string (self, package, typeInfo):
        """
        Get string for a specific package information
        """

        return self.__char_to_str(pacman_pkg_getinfo(package, typeInfo))


    def __long (self, package, typeInfo):
        """
        Get long value for a specific package information
        """

        return self.__long_to_str(pacman_pkg_getinfo(package, typeInfo))


    # ------------------------------------
    #   Configuration
    # ------------------------------------

    def __set_option (self, option, value):
        """
        Set a library option

        :return: 0 on success, -1 on error
        """

        lib = cdll.LoadLibrary("libpacman.so")

        lib.pacman_set_option.argtypes = [c_int, c_char_p]
        lib.pacman_set_option.restype = c_int

        return lib.pacman_set_option(option, value)


    @BusMethod (BUSNAME, in_signature='a{ss}s')
    def configuration_update (self, path, hashmode="SHA1"):
        """
        Change pacman-g2 config

        :param dict(str=str) path: Generated files and them hash sums
        :param str hashmode: Hash mode to check file differences
        """

        error = False

        for data in path:

            if basename(data) == "pacman-g2.conf":
                dest = CONFIGURATION_PATH
            else:
                dest = "%s/%s" % (REPOSITORIES_PATH, basename(data))

            if exists(data):

                # ------------------------------------
                #   Hash
                # ------------------------------------

                pipe = open(data, 'r')

                sha1 = hash_new(hashmode)
                sha1.update(pipe.read())

                pipe.close()

                if path[data] == sha1.hexdigest():

                    # ------------------------------------
                    #   Update files
                    # ------------------------------------

                    # Save copy file
                    if exists(dest):
                        copyfile(dest, "%s.pacsave" % dest)

                        # Update config file
                        move(data, dest)

                    else:
                        move(data, dirname(dest))

                else:
                    self.__log.error("The sha1sums of %s file is not the "
                        "same as sended" % data)

                    error = True

        if not error:
            self.config = PacmanConfig(CONFIGURATION_PATH)


    @BusMethod (BUSNAME, in_signature='s', out_signature='s')
    def configuration_value (self, option):
        """
        Return the value for a specific option in pacman-g2.conf

        :param str option: Option name

        :return: str or None
        """

        data = self.config.option(option)

        if data is None:
            if option in self.__default_paths:
                return self.__default_paths[option]

            else:
                return None

        return data.value

    # ------------------------------------
    #   Repositories
    # ------------------------------------

    def __load_repositories (self):
        """
        Get the repositories from configuration file
        """

        self.__set_repositories(FW_LOCAL)

        for repository in self.config.repositories:

            if not repository.commented:
                self.__set_repositories(repository.name)


    def __convert_repository (self, repository):
        """
        Convert a repository to correct index

        :param int/str repository: Repository
        """

        if type(repository) == dbus_Int32:
            if repository >= 0 and repository < len(self.__repositories):
                return repository

        elif type(repository) == dbus_String:
            if str(repository) in self.__repositories:
                return self.__repositories.index(str(repository))

        return None


    def __set_repositories (self, repository):
        """
        Add repository into pacman instance
        """

        if not repository in self.__repositories:
            self.__repositories.append(repository)
            self.__databases.append(pacman_db_register(repository))


    @BusMethod (BUSNAME, in_signature='v', out_signature='v')
    def repositories (self, repository=None):
        """
        Get the repository list

        :param int/str repository: Repository

        :return: Repositories list
        :rtype: list (str repositoryName)
        """

        if repository is not None:
            return self.__convert_repository(repository)

        return self.__repositories

    # ------------------------------------
    #   Groups
    # ------------------------------------

    @BusMethod (BUSNAME, in_signature='v', out_signature='as')
    def groups (self, repository):
        """
        Get the groups list from a repository

        :param int/str repository: Repository

        :return: Repository groups list
        :rtype: list() or None
        """

        data = list()

        repository = self.__convert_repository(repository)

        if repository is not None:

            index = pacman_list_first(void_to_PM_LIST(
                pacman_db_getgrpcache(self.__databases[repository])))

            while index is not None:

                name = self.__char_to_str(pacman_grp_getinfo(void_to_PM_GRP(
                    pacman_list_getdata(void_to_PM_LIST(index))), GRP_NAME))

                if not name in data:
                    data.append(name)

                index = pacman_list_next(void_to_PM_LIST(index))

        return data


    # ------------------------------------
    #   Packages
    # ------------------------------------

    def __load_installed_packages (self):
        """
        Get the list of installed packages

        :param str groupName: Group name

        :return: Installed packages list
        :rtype: list (str packageName, str packageVersion)
        """

        if len(self.__installed) == 0:
            path = self.configuration_value("DBPath")

            for package in listdir("%s/%s" % (path, FW_LOCAL)):
                self.__installed.append(re_findall(INSTALLED, package)[0])


    @BusMethod (BUSNAME, in_signature='vs', out_signature='as')
    def packages (self, repository, name):
        """
        Get the packages list from a group and a repository

        :param str/int repository: Repository
        :param str name: Group name like apps-extra

        :return: Group packages list
        :rtype: list (str packageName)
        """

        data = list()

        repository = self.__convert_repository(repository)
        if repository is not None:

            group = pacman_db_readgrp(self.__databases[repository], str(name))
            if group is not None:

                # Get first package from group
                index = pacman_list_first(void_to_PM_LIST(
                    pacman_grp_getinfo(group, GRP_PKGNAMES)))

                while index is not None:
                    package = self.__char_to_str(
                        pacman_list_getdata(void_to_PM_LIST(index)))

                    if not package in data:
                        data.append(package)

                    # Get next package from group
                    index = pacman_list_next(void_to_PM_LIST(index))

        return data


    @BusMethod (BUSNAME, out_signature='a(ss)')
    def packages_installed (self):
        """
        Get the list of installed packages

        :return: Installed packages list
        :rtype: list (str, str)
        """

        return self.__installed


    @BusMethod (BUSNAME, in_signature='vs', out_signature='as')
    def packages_search (self, repository, pattern):
        """
        Search pattern string into the repository

        :param str/int repository: Repository
        :param str pattern: Expression to search into pacman-g2 databases

        :return: Found packages list
        :rtype: list (str repositoryName, str packageName)
        """

        data = list()

        repository = self.__convert_repository(repository)
        if repository is not None:
            self.__set_option(OPT_NEEDLES, pattern)

            packages = pacman_db_search(self.__databases[repository])
            if packages is not None:

                index = pacman_list_first(void_to_PM_LIST(packages))
                while index is not None:

                    name = self.__char_to_str(pacman_pkg_getinfo(
                        void_to_PM_PKG(pacman_list_getdata(
                        void_to_PM_LIST(index))), PKG_NAME))

                    if not name in data:
                        data.append(name)

                    index = pacman_list_next(void_to_PM_LIST(index))

            # Check nobuild packages
            if exists(REPOMAN_BIN):

                repositoryName = self.__repositories[repository]
                if repositoryName in self.__nobuild:

                    for package in self.__nobuild[repositoryName]:
                        result = re_findall(pattern, package[1])

                        if len(result) > 0:
                            data.append(package[1])

        return data


    @BusMethod (BUSNAME, out_signature='a(ss)')
    def packages_upgrade (self):
        """
        Get available upgrade for the user system

        :return: Updated packages list
        :rtype: list (str packageName, str packageVersion)
        """

        data = list()

        if pacman_trans_init(TRANS_TYPE_SYNC, TRANS_FLAGS,
            None, None, None) == -1:
            self.__log.error("Failed to initialize pacman_trans_init")
            return data

        if pacman_trans_sysupgrade() == -1:
            self.__log.error("Failed to initialize pacman_trans_sysupgrade")
            return data

        packages = pacman_trans_getinfo(TRANS_PACKAGES)
        if packages is not None:

            index = pacman_list_first(void_to_PM_LIST(packages))
            while index is not None:

                package = void_to_PM_PKG(pacman_sync_getinfo(
                    void_to_PM_SYNCPKG(pacman_list_getdata(
                    void_to_PM_LIST(index))), SYNC_PKG))

                data.append([self.__char_to_str(pacman_pkg_getinfo(package,
                    PKG_NAME)), self.__char_to_str(pacman_pkg_getinfo(package,
                    PKG_VERSION))])

                index = pacman_list_next(void_to_PM_LIST(index))

        pacman_trans_release()

        return data


    # ------------------------------------
    #   Package informations
    # ------------------------------------

    @BusMethod (BUSNAME, in_signature='ss', out_signature='a{sv}')
    def package (self, repository, name):
        """
        Get informations about the package

        :param str repository: Repository name
        :param str name: Package name

        :return: Package informations from the database
        :rtype: dict
        """

        # Create a new Package object
        package = dict()

        if repository in self.__repositories:
            repository = self.__repositories.index(repository)

        else:
            return package

        # Avoid to check unregistred repository
        if repository > (len(self.__repositories) - 1):
            return package

        # Get informations for package name
        data = pacman_db_readpkg(self.__databases[repository], str(name))

        # Check if current package has some informations
        version = pacman_pkg_getinfo(data, PKG_VERSION)
        if version == None:
            return package

        version = self.__char_to_str(version)

        # Check package status to get some specific informations
        installed = self.package_status(str(name), version)
        if repository == 0 and not installed:
            return package

        # Set package informations
        package["name"] = str(name)
        package["version"] = version
        package["description"] = self.__unicode(data, PKG_DESC)

        package["groups"] = self.__list(data, PKG_GROUPS)
        package["depends"] = self.__list(data, PKG_DEPENDS)
        package["provides"] = self.__list(data, PKG_PROVIDES)
        package["replaces"] = self.__list(data, PKG_REPLACES)
        package["conflicts"] = self.__list(data, PKG_CONFLICTS)
        package["required_by"] = self.__list(data, PKG_REQUIREDBY)

        package["installed"] = bool(installed)

        if installed:
            data = pacman_db_readpkg(self.__databases[0], str(name))

            package["url"] = self.__unicode(data, PKG_URL)
            package["install_date"] = self.__unicode(data, PKG_INSTALLDATE)
            package["packager"] = self.__unicode(data, PKG_PACKAGER)

            package["size"] = self.__long(data, PKG_SIZE)

        else:
            package["compress_size"] = self.__long(data, PKG_SIZE)
            package["uncompress_size"] = self.__long(data, PKG_USIZE)

        # This test check if the description can be send with dbus
        try:
            package["description"].decode("utf-8")
        except:
            package["description"] = self.__unicode(data, PKG_DESC, "replace")

        return package


    @BusMethod (BUSNAME, in_signature='ss', out_signature='s')
    def package_sha1sums (self, repository, name):
        """
        Get the correct SHA1SUMS from frugalware/repositories

        :param str repository: Repository name
        :param str name: Package name

        :return: Package sha1sums
        :rtype: string
        """

        if repository in self.__repositories:
            repository = self.__repositories.index(repository)

        return self.__unicode(pacman_db_readpkg(self.__databases[repository],
            str(name)), PKG_SHA1SUM)


    @BusMethod (BUSNAME, in_signature='s', out_signature='as')
    def package_files (self, name):
        """
        Get the files list of the package

        :param str name: Package name

        :return: Package files array
        :rtype: list (str filePath)
        """

        data = list()

        package = pacman_db_readpkg(self.__databases[0], str(name))

        index = pacman_list_first(void_to_PM_LIST(
            pacman_pkg_getinfo(package, PKG_FILES)))
        while index is not None:

            package = self.__char_to_str(
                pacman_list_getdata(void_to_PM_LIST(index)))

            if not package in data:
                data.append(package)

            index = pacman_list_next(void_to_PM_LIST(index))

        return data


    @BusMethod (BUSNAME, in_signature='ss', out_signature='b')
    def package_status (self, name, version):
        """
        Get install status for a specific package

        :param str name: Package name
        :param str version: Package version

        :return: Package status
        :rtype: bool
        """

        path = self.configuration_value("DBPath")

        if isdir("%s/%s/%s-%s" % (path, FW_LOCAL, name, version)):
            return True

        return False


    @BusMethod (BUSNAME, in_signature='sss', out_signature='b')
    def package_fpm (self, name, version, arch):
        """
        Look in packages cache if wanted fpm exist

        :param str name: Package name
        :param str version: Package version
        :param str arch: Package architecture

        :return: Package install status
        :rtype: bool
        """

        path = self.configuration_value("CacheDir")

        if isfile("%s/%s-%s-%s.fpm" % (path, name, version, arch)):
            return True

        return False


    # ------------------------------------
    #   No-build packages
    # ------------------------------------

    def __load_nobuild (self, repository):
        """
        Get the nobuild packages list

        :param str repository: Repository name
        """

        for folder in glob("%s/%s/source/*" % (
            FST_ROOT, self.nobuild_source_name(repository))):

            group = basename(folder)

            # Get packages list for current group
            packages = self.packages(
                self.__repositories.index(repository), group)

            # Get all the package from fstGroup
            for package in glob("%s/*" % folder):
                name = basename(package)
                if not name in packages:

                    frugalbuildPath = "%s/%s/FrugalBuild" % (folder, name)
                    if exists(frugalbuildPath):
                        pipe = open(frugalbuildPath, 'r')
                        pipe.seek(0)

                        for line in pipe.readlines():

                            if not line.find("nobuild") == -1:

                                # Create a new array if unavailable
                                if not group in self.__nobuild[repository]:
                                    self.__nobuild[repository][group] = list()

                                self.__nobuild[repository][group].append(name)
                                break

                        pipe.close()


    @BusMethod (BUSNAME, in_signature='s', out_signature='a{sv}')
    def nobuild (self, repository=None):
        """
        Get the nobuild packages for a specific groups list

        :param str repository: Repository name

        :return: Nobuild packages list
        :rtype: dict()
        """

        if not repository == None:
            if repository in self.__nobuild:
                return self.__nobuild.get(repository, dict())

            else:
                return dict()

        return self.__nobuild


    @BusMethod (BUSNAME, in_signature='s')
    def nobuild_reset (self, repository):
        """
        Reset the nobuild packages list to refill it correctly
        """

        if repository in self.__nobuild:
            self.__nobuild[repository] = dict()

            self.__load_nobuild(repository)


    @BusMethod (BUSNAME, out_signature='s')
    def nobuild_fst_path (self):
        """
        Return fst_root value from repoman.conf

        :return: Frugalware git path
        :rtype: str
        """

        return FST_ROOT


    @BusMethod (BUSNAME, in_signature='s', out_signature='s')
    def nobuild_source_name (self, value):
        """
        Get source name for a specific repository
        """

        return self.__sourceName.get(value, value)


# --------------------------------------------
#
#   ACTIONS
#
# --------------------------------------------

    @BusSignal (BUSNAME, signature='av')
    def signal_send (self, value):
        """
        Send a signal with dbus
        """

        pass


    @BusMethod (BUSNAME, in_signature='b')
    def system_upgrade (self, force=False):
        """
        Update pacman-g2 databases
        """

        mode = "update"

        repositories = len(self.__repositories) - 1

        # Check if nobuild repository is available
        if exists(REPOMAN_BIN):
            repositories += 1

        # Check if 32 bits chroot is available
        if exists("/usr/lib/fw32") and exists(FW32_UPDATE_BIN):
            repositories += 1

        # Start transaction
        self.signal_send(["start", mode, repositories])

        # Update pacman-g2 repositories
        pacman_logaction("synchronizing package lists")

        for repository in self.__repositories:

            # We don't use local repo for update
            if not repository == FW_LOCAL:
                self.signal_send(["event", mode, repository])

                index = self.__repositories.index(repository)
                result = pacman_db_update(int(force), self.__databases[index])

                # There is an error
                if result == -1:
                    pm_errno = pacman_geterror()

                    if pm_errno == ERR_DB_SYNC:
                        self.__log.error(
                            "failed to synchronize %s" % repository)
                    else:
                        self.__log.error(
                            "failed to update %s: %s" % (repository, pm_errno))

                self.signal_send(["progress", mode, repository, result])

        # Update 32 bits chroot
        if exists("/usr/lib/fw32") and exists(FW32_UPDATE_BIN):
            self.signal_send(["event", mode, "chroot"])
            result = call([FW32_UPDATE_BIN])

            self.signal_send(["progress", mode, "chroot", result])

        # Update nobuild repository
        if exists(REPOMAN_BIN):
            self.signal_send(["event", mode, "nobuild"])
            result = call([REPOMAN_BIN, "update"])

            self.signal_send(["progress", mode, "nobuild", result])
            self.nobuildListDone = False

        # Finish transaction
        self.signal_send(["end", mode])

        self.reload_daemon()


    @BusMethod (BUSNAME, in_signature='u')
    def system_cache (self, mode):
        """
        Clean pacman-g2 cache

        :param int mode: Clean all the packages from cache with mode=1 and
            clean only the old packages from cache with mode=0
        """

        if pacman_sync_cleancache(mode) == -1:
            self.__log.error("Failed to clean the cache with mode %d" % mode)


    @BusMethod (BUSNAME, in_signature='a(s)b')
    def system_sync (self, listPackages, download=False):
        """
        Install a packages list or download them only

        :param list(str) listPackages: Packages list
        :param bool download: Flag

        :return: transactionStatus
        :rtype: bool
        """

        mode = "install"

        try:

            self.signal_send(["start", mode, len(listPackages)])

            # Release transaction if necessary (useful to remove lock file)
            pacman_trans_release()

            # Create a translation list
            data = LISTp_new()

            # Define transaction flag
            transFlag = TRANS_FLAG_NOCONFLICTS
            if download:
                transFlag = TRANS_FLAG_DOWNLOADONLY


            # ------------------------------------------------
            #    Create a new transaction
            # ------------------------------------------------

            self.signal_send(["event", mode, "init"])

            if pacman_trans_init(TRANS_TYPE_SYNC, transFlag,
                self.__transaction_event, self.__transaction_conv,
                self.__transaction_progress) == -1:

                pm_errno = pacman_geterror()

                # The lock file still alive (♪)
                if pm_errno == ERR_HANDLE_LOCK:
                    self.__log.error("The pacman-g2 lock is active")

                    pacman_trans_release()

                else:
                    self.__log.error("Can't init transaction (%s)" % (
                        pacman_strerror(pm_errno)))

                self.signal_send(["error", mode, "init"])

                return False


            # ------------------------------------------------
            #    Add target
            # ------------------------------------------------

            self.signal_send(["event", mode, "add"])

            for package in listPackages:

                self.signal_send(["progress", mode, package])

                # Add package into transaction list
                if pacman_trans_addtarget(str(package)) == -1:

                    pm_errno = pacman_geterror()

                    # Just ignore duplicate targets
                    if pm_errno == ERR_TRANS_DUP_TARGET:
                        pass

                    elif not pm_errno == ERR_PKG_NOT_FOUND:
                        self.__log.error("Failed to add target %s (%s)" % \
                            (package, pacman_strerror(pm_errno)))

                        LISTp_free(data)

                        self.signal_send(["error", mode, "add"])

                        pacman_trans_release()
                        return False

                    # TODO: Check if this is a group or a regex

            # ------------------------------------------------
            #    Prepare the transaction based on targets and flags
            # ------------------------------------------------

            self.signal_send(["event", mode, "prepare"])

            if pacman_trans_prepare(data) == -1:

                pm_errno = pacman_geterror()

                if pm_errno == ERR_UNSATISFIED_DEPS:
                    self.__log.error("Unsatisfued deps")

                elif pm_errno == ERR_CONFLICTING_DEPS:
                    self.__log.error("Conflicting deps")

                elif pm_errno == ERR_DISK_FULL:
                    self.__log.error("Disk full")

                else:
                    self.__log.error("Failed to prepare transaction (%s)" % \
                        pacman_strerror(pm_errno))

                LISTp_free(data)

                self.signal_send(["error", mode, "prepare"])

                pacman_trans_release()
                return False


            # ------------------------------------------------
            #    Get transaction list
            # ------------------------------------------------

            packages = pacman_trans_getinfo(TRANS_PACKAGES)

            if not packages == None:

                # --------------------------------------------
                #    List targets and get confirmation
                # --------------------------------------------

                if not (pacman_trans_getinfo(TRANS_FLAGS) and \
                    TRANS_FLAG_PRINTURIS):

                    index = pacman_list_first(packages)

                    while not index == 0:

                        pointer = pacman_list_getdata(index)
                        pkg = pacman_sync_getinfo(pointer, SYNC_PKG)

                        if pacman_sync_getinfo(pointer, SYNC_TYPE) == \
                            SYNC_TYPE_REPLACE:

                            data = pacman_sync_getinfo(pointer, SYNC_DATA)

                        index = pacman_list_next(void_to_PM_LIST(index))

                # --------------------------------------------
                #    Perform the installation
                # --------------------------------------------

                self.signal_send(["event", mode, "commit"])

                if pacman_trans_commit(data) == -1:

                    pm_errno = pacman_geterror()

                    self.__log.error("Failed to commit transaction (%s)" % \
                        pacman_strerror(pm_errno))

                    LISTp_free(data)

                    self.signal_send(["error", mode, "commit"])

                    pacman_trans_release()
                    return False


            # ------------------------------------------------
            #    Clean up
            # ------------------------------------------------

            if pacman_trans_release() == -1:

                self.__log.error("Failed to release transaction (%s)" % \
                    pacman_strerror(pacman_geterror()))

                LISTp_free(data)

                self.signal_send(["error", mode, "release"])

                return False

            LISTp_free(data)

            self.signal_send(["end", mode])

            return True

        except Exception as err:

            self.__log.error("%s", str(err))

            self.signal_send(["error", mode, str(err)])

            pacman_trans_release()
            return False


    @BusMethod (BUSNAME, in_signature='su', out_signature='b')
    def system_remove (self, listPackages, removeDeps):

        # Remove a package

        pass


    @staticmethod
    def __transaction_event (event, data1, data2):

        # Get actual event

        self.signal_send(["event", "install", data1, data2])

        label = ""

        if event == TRANS_EVT_CHECKDEPS_START:
            label = "Checking dependencies"

        elif event == TRANS_EVT_FILECONFLICTS_START:
            label = "Checking for file conflicts"

        elif event == TRANS_EVT_RESOLVEDEPS_START:
            label = "Resolving dependencies"

        elif event == TRANS_EVT_INTERCONFLICTS_START:
            label = "Looking for inter-conflicts"

        elif event == TRANS_EVT_ADD_START:
            label = "Installing"

        elif event == TRANS_EVT_UPGRADE_START:
            label = "Upgrading"

        elif event == TRANS_EVT_REMOVE_START:
            label = "Removing"

        elif event == TRANS_EVT_INTEGRITY_START:
            label = "Checking package integrity"

        elif event == TRANS_EVT_SCRIPTLET_START:
            label = data1

        elif event == TRANS_EVT_RETRIEVE_START:
            label = "Retrieving packages"

        if len(label) > 0:
            print (label)


    @staticmethod
    def __transaction_conv (event, data1, data2, data3, response):

        # Requests from pacman-g2
        self.signal_send(["request", "install", data1, data2, data3])

        if event == TRANS_CONV_LOCAL_UPTODATE:
            self.__log.info("%s local version is up to date." % \
                void_to_char(pacman_pkg_getinfo(data1, PKG_NAME)))
            response[0] = 0

        if event == TRANS_CONV_LOCAL_NEWER:
            self.__log.info("%s local version is newer." % \
                void_to_char(pacman_pkg_getinfo(data1, PKG_NAME)))
            response[0] = 0

        if event == TRANS_CONV_CORRUPTED_PKG:
            self.__log.info("Archive is corrupted.")
            response[0] = 1


    @staticmethod
    def __transaction_progress (event, pkgName, percent, count, remaining):

        # Progress of package transaction
        self.signal_send(["progress", "install", pkgName, percent])

        if event == TRANS_PROGRESS_ADD_START:
            label = "Installing"

        elif event == TRANS_PROGRESS_UPGRADE_START:
            lavel = "Upgrading"

        elif event == TRANS_PROGRESS_REMOVE_START:
            label = "Removing"

        elif event == TRANS_PROGRESS_CONFLICTS_START:
            label = "Checking file conflicts"

        else:
            label = ""

        if len(label) > 0:
            print (label)

