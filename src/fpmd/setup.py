#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages

from os.path import join
from os.path import dirname

setup(
    name = "fpmd",
    version = "1.3rc2",
    author = "PacMiam",
    author_email = "PacMiam@gmx.fr",
    description = "Libpacman daemon",
    url = 'https://frugalware.org',
    license = "GPLv3",

    packages = ['fpmd', 'fpmd.tools'],

    entry_points = {
        'console_scripts': [
            'fpmd = fpmd.main:main',
        ],
    },

    classifiers = [
        "Programming Language :: Python :: 2.7",
        "Development Status :: Stable",
        "License :: GPL3",
        "Libraries :: Python Modules",
        "Operating System :: GNU/Linux",
        "Topic :: System",
    ],
)

