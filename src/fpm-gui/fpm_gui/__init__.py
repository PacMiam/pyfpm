# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os.path import expanduser

# ------------------------------------------------------------------
#   Informations
# ------------------------------------------------------------------

__version__ = "1.3rc2"
__website__ = "http://www.frugalware.org"
__copyright__ = "(C) 2012-2015 Frugalware Developer Team (GPLv3)"

# ------------------------------------------------------------------
#   Paths
# ------------------------------------------------------------------

LOGPATH = expanduser("~/fpm-gui.log")

# Files
CONFIGPATH = expanduser("~/.config/fpm-gui")

CONFIGFILE = "%s/fpm-gui.conf" % CONFIGPATH
LOCKFILE = "%s/fpm-gui.lock" % CONFIGPATH
GTKRCFILE = expanduser("~/.gtkrc-2.0")

# Screenshot website
WEBSITE = "https://screenshots.debian.net"
WEBSITE_FALLBACK = "http://screenshots.ubuntu.com"

# ------------------------------------------------------------------
#   Colors
# ------------------------------------------------------------------

RED = "#FF0039"
BLUE = "#3F8BE0"
GREEN = "#00AC00"
WHITE = "#FFFFFF"
BLACK = "#000000"
ORANGE = "#FF7D00"

# ------------------------------------------------------------------
#   Configurations
# ------------------------------------------------------------------

# Default gtkrc configuration
defaultGtkrc = """
gtk-theme-name="Frugalware"
gtk-icon-theme-name="Frugalware"
"""

# Default fpm-gui configuration
defaultValues = dict(
    application=dict(
        width="800",
        height="600",
        first_use="true",
        max_cache_size="0",
        save_window_size="true",
        show_notification="true",
        show_update_window="true",
        cache=expanduser("~/.local/share/fpm-gui/") ),
    packages=dict(
        install="",
        reinstall="",
        remove="",
        last_update="",
        check_dependencies="manually" ),
    network=dict(
        download="true",
        offline="false" ),
    editor=dict(
        colorscheme="classic",
        number_line="false" ),
    colors=dict(
        remove=RED,
        install=GREEN,
        upgrade=ORANGE,
        downgrade=BLUE,
        use_colors_statubar="false",
        use_bold="true" ),
    terminal=dict(
        font="Monospace 11",
        background=BLACK,
        foreground=WHITE,
        scrollbar="right",
        buffer="100" ),
    shortcuts=dict(
        focus_search_entry="<Control>F",
        open_preferences="<Control>P",
        open_repositories="<Control>R",
        open_logs="<Control>L",
        show_upgrade="<Control>U",
        mark_to_reinstall="<Control>M",
        apply_selection="<Control>Return",
        reset_selection="<Control>Escape",
        install_local_fpm="<Shift><Control>A",
        clean_packages_cache="<Shift><Control>E",
        update_databases="<Shift><Control>U",
        update_chroot="<Shift><Control>F",
        open_help="F1",
        open_files_manager="F2",
        open_changelog="F3",
        open_frugalbuild="F4",
        merge_package="F5",
        show_screenshot="F9",
        change_network_status="F12" ))

# ------------------------------------------------------------------
#   Regex
# ------------------------------------------------------------------

REGEX_LOCAL_PACKAGE = "([\w?\-?\.?\+?]+)-([\d+\.?|\d+~?|\w]+-\d+)+"
REGEX_MIRROR_INTEGRITY = \
    "^(?:(?:http:|https:|ftp:)//.+/frugalware\-[\w?\d?]+|file:///.+)$"

# ------------------------------------------------------------------
#   Icons
# ------------------------------------------------------------------

# Icons - Categories
ICON_CLEAR = "aptdaemon-cleanup"
ICON_CLEAR_FALLBACK = "edit-clear"
ICON_UPGRADE = "aptdaemon-upgrade"
ICON_UPGRADE_FALLBACK = "software-update-available"
ICON_SCREENSHOT = "image-x-generic"
ICON_LOGVIEWER = "edit-find"

# Icons - Misc
ICON_NETWORK_ONLINE = "network-transmit-receive"
ICON_NETWORK_OFFLINE = "network-offline"
ICON_NETWORK_RECEIVE = "network-receive"
ICON_HARDDRIVE = "drive-harddisk"
ICON_PYFPM_DELETE = "face-crying"

# Icons - Buttons
ICON_FOLDER = "folder"
ICON_FILE = "document"
ICON_FILES_MANAGER = "system-file-manager"
ICON_CHANGELOG = "document-open-recent"
ICON_FRUGALBUILD = "document-page-setup"
ICON_REPOMAN = "utilities-terminal"

# Icons - Packages
ICON_AVAILABLE = "package-x-generic"
ICON_TOINSTALL = "aptdaemon-add"
ICON_TOINSTALL_FALLBACK = "list-add"
ICON_PURGE = "aptdaemon-delete"
ICON_PURGE_FALLBACK = "list-remove"
ICON_UPGRADE = "aptdaemon-upgrade"
ICON_UPGRADE_URGENT = "software-update-urgent"
ICON_UPGRADE_FALLBACK = "software-update-available"
ICON_DOWNGRADE = "aptdaemon-download"
ICON_DOWNGRADE_FALLBACK = "go-down"
ICON_LOCKED = "emblem-readonly"

# Icons - Groups
ICON_GRP_APPLICATIONS = "applications-office"
ICON_GRP_CINNAMON = ICON_AVAILABLE
ICON_GRP_DEVELOPMENT = "applications-development"
ICON_GRP_DOCUMENTATION = "system-help"
ICON_GRP_ENLIGHTENMENT = ICON_AVAILABLE
ICON_GRP_FONTS = "preferences-desktop-font"
ICON_GRP_GAMES = "applications-games"
ICON_GRP_GNOME = ICON_AVAILABLE
ICON_GRP_KDE = ICON_AVAILABLE
ICON_GRP_LIBRARY = "applications-utilities"
ICON_GRP_LOCALES = "preferences-desktop-locale"
ICON_GRP_LXDE = ICON_AVAILABLE
ICON_GRP_MATE = ICON_AVAILABLE
ICON_GRP_MULTIMEDIA = "applications-multimedia"
ICON_GRP_NETWORK = "applications-internet"
ICON_GRP_SYSTEM = "utilities-system-monitor"
ICON_GRP_XFCE = ICON_AVAILABLE
ICON_GRP_XSERVER = "video-display"

# ------------------------------------------------------------------
#   Groups
# ------------------------------------------------------------------

GRP_APPLICATIONS = [ICON_GRP_APPLICATIONS, "(^[x]?apps.*)"]
GRP_DEVELOPMENT = [ICON_GRP_DEVELOPMENT, "(^devel.*)"]
GRP_DOCUMENTATION = [ICON_GRP_DOCUMENTATION, "(^docs.*)"]
GRP_ENLIGHTENMENT = [ICON_GRP_ENLIGHTENMENT, "(^e17.*)"]
GRP_FONTS = [ICON_GRP_FONTS, "(^.*fonts.*)"]
GRP_GAMES = [ICON_GRP_GAMES, "(^games.*)"]
GRP_GNOME = [ICON_GRP_GNOME, "(^gnome.*)"]
GRP_KDE = [ICON_GRP_KDE, "(^kde.*)"]
GRP_LIBRARY = [ICON_GRP_LIBRARY, "(^compat-.*|^[x]?lib.*|^qt.*-libs)"]
GRP_LOCALES = [ICON_GRP_LOCALES, "(^locale.*)"]
GRP_LXDE = [ICON_GRP_LXDE, "(^lxde.*)"]
GRP_MATE = [ICON_GRP_MATE, "(^mate.*)"]
GRP_MULTIMEDIA = [ICON_GRP_MULTIMEDIA, "(^[x]?multimedia.*)"]
GRP_NETWORK = [ICON_GRP_NETWORK, "(^network.*)"]
GRP_SYSTEM = [ICON_GRP_SYSTEM, "(^base.*|^core.*|^chroot.*)"]
GRP_XFCE = [ICON_GRP_XFCE, "(^xfce4.*)"]
GRP_XSERVER = [ICON_GRP_XSERVER, "(^x11.*|^xorg-(?!fonts).*)"]
