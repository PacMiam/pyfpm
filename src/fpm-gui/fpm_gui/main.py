# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import argv
from sys import version_info
from argparse import ArgumentParser

# Files
from os import F_OK
from os import W_OK
from os import read
from os import mkdir
from os import access
from os import getpid
from os import remove
from os.path import exists
from os.path import expanduser

# Log
from logging import getLogger

# Pyfpm
from fpm_gui import __version__
from fpm_gui import __copyright__
from fpm_gui import LOGPATH
from fpm_gui import LOCKFILE
from fpm_gui import GTKRCFILE
from fpm_gui import CONFIGPATH
from fpm_gui import CONFIGFILE
from fpm_gui import defaultGtkrc
from fpm_gui import defaultValues

from fpmd.tools.utils import check_xorg
from fpmd.tools.utils import file_handler
from fpmd.tools.utils import console_handler
from fpmd.tools.utils import getTranslation
from fpmd.tools.config import Config
from fpmd.tools.package import start_daemon

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()
LOG.addHandler(file_handler(LOG, LOGPATH))

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def main (*args):

    # ------------------------------------
    #   Arguments
    # ------------------------------------

    if len(argv) >= 1:

        parser = ArgumentParser(description="Graphical packages manager",
            epilog=__copyright__, conflict_handler="resolve")

        parser.add_argument("-v", "--version", action="version",
            version="fpm-gui %s (Inky) Licence GPLv3" % __version__,
            help="show the current version")
        parser.add_argument("-d", "--debug", action="store_true",
            help="use debug mode")

        group_ui = parser.add_mutually_exclusive_group()
        group_ui.add_argument("-a", "--add", action="store_true",
            help="install a local package")
        group_ui.add_argument("-l", "--log", action="store_true",
            help="show pacman-g2 logs")
        group_ui.add_argument("-r", "--repositories", action="store_true",
            help="show pacman-g2 repositories")
        group_ui.add_argument("-p", "--preferences", action="store_true",
            help="show fpm-gui preferences")

        args = parser.parse_args()

    # ------------------------------------
    #   Check debug mode
    # ------------------------------------

    LOG.addHandler(console_handler(LOG, args.debug))

    # ------------------------------------
    #   Graphical session
    # ------------------------------------

    if check_xorg():

        # ------------------------------------
        #   Generate user configuration
        # ------------------------------------

        if not exists(GTKRCFILE):
            with open(GTKRCFILE, 'w') as pipe:
                pipe.write(defaultGtkrc)

        # Create a configuration folder for new user
        if not exists(CONFIGPATH):
            try:
                mkdir(CONFIGPATH, 0o755)
                LOG.info(_("Create %s folder") % CONFIGPATH)

            except Exception as error:
                LOG.error(_("Cannot create %(path)s folder: %(error)s") % (
                    dict(path=CONFIGPATH, error=str(error))),
                    exc_info=args.debug)

                return False

        # ------------------------------------
        #   Check if another fpm-gui is running
        # ------------------------------------

        if access(LOCKFILE, F_OK):
            with open(LOCKFILE, 'r') as pipe:
                pid = pipe.read()

            if len(pid) == 0 or exists("/proc/%s" % pid):
                LOG.error(_("Another instance of fpm-gui is already running. "
                    "If this is not the case, please, remove %s file" %
                    LOCKFILE))

                return False

        # ------------------------------------
        #   Check FPMd connection
        # ------------------------------------

        if not start_daemon() is None:

            # ------------------------------------
            #   Generate fpm-gui configuration
            # ------------------------------------

            parser = Config(CONFIGFILE)
            parser.update_configuration(defaultValues)

            # ------------------------------------
            #   Screenshots cache
            # ------------------------------------

            cache = parser.get("application", "cache")
            default_cache = defaultValues["application"]["cache"]

            # Use default value
            if cache is None:
                cache = default_cache

                parser.set("application", "cache", default_cache)
                parser.write_configuration()

            # Check if folder is writable
            if exists(cache) and not access(cache, W_OK):
                LOG.warning(_("Cannot write into %s folder. Set default "
                    "folder instead") % cache)

                cache = default_cache

                parser.set("application", "cache", default_cache)
                parser.write_configuration()

            # Create a new folder
            if not exists(cache):
                try:
                    LOG.info(_("Create %s folder") % cache)
                    mkdir(cache, 0o755)

                except Exception as error:
                    cache = default_cache

                    LOG.error(_("Cannot create %(path)s folder: %(error)s") %
                        (dict(path=cache, error=str(error))),
                        exc_info=args.debug)

            # ------------------------------------
            #   Write current fpm-gui PID into lock
            # ------------------------------------

            with open(LOCKFILE, 'w') as pipe:
                pipe.write(str(getpid()))

            # ------------------------------------
            #   Run an interface
            # ------------------------------------

            try:
                if args.add:
                    from gtkui.window_sync import WindowSyncFpm

                    WindowSyncFpm()

                elif args.log:
                    from gtkui.window_logs import WindowLog

                    WindowLog()

                elif args.repositories:
                    from gtkui.window_repositories import WindowRepositories

                    WindowRepositories()

                elif args.preferences:
                    from gtkui.window_preferences import WindowPreferences

                    WindowPreferences()

                else:
                    from gtkui.interface import Interface

                    Interface(parser, args.debug)

            # ------------------------------------
            #   Show error dialog
            # ------------------------------------

            except Exception as error:
                LOG.error(error, exc_info=args.debug)
                show_error(args.debug)

            # ------------------------------------
            #   Remove fpm-gui lock
            # ------------------------------------

            if exists(LOCKFILE):
                with open(LOCKFILE, 'r') as pipe:
                    lockFile = pipe.read()

            if not lockFile is None and lockFile == str(getpid()):
                remove(LOCKFILE)

        else:
            LOG.fatal(_("Cannot connect to FPMd daemon"), exc_info=args.debug)
            show_error(args.debug)

    # ------------------------------------
    #   No graphical session
    # ------------------------------------

    else:
        LOG.warning(_("No display available, use pacman-g2 instead"))


def show_error (debug):

    try:
        from gtk import MESSAGE_ERROR
        from gtkui.window import WindowPopup

        WindowPopup(_("An error occur"), _("There is a problem and fpm-gui "
            "cannot be open.\nYou can found more informations in %s.\n\nSorry "
            "for the convenient." % (LOGPATH)), MESSAGE_ERROR)

    except ImportError as error:
        LOG.fatal(_("Cannot import gtk+ libraries"), exc_info=debug)


if __name__ == "__main__":
    main()
