# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from datetime import date

# Interface
import gtk

from modules import refresh
from modules import getIconFromTheme
from window import Window

# Log
from logging import getLogger

# Modules
from fpm_gui import *

from fpmd import LOGPATH_FILE

from fpmd.tools.config import Config
from fpmd.tools.pacmanconf import PacmanConfig
from fpmd.tools.utils import getTranslation

# Regex
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowLog (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Log viewer"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        if parent is not None:
            self.config = parent.config

            self.log_path = parent.pacman.configuration_value("LogFile")

            self.iconInstall = parent.icon_to_install
            self.iconRemove = parent.icon_to_remove
            self.iconUpgrade = parent.icon_upgrade

        else:
            self.config = Config(CONFIGFILE)
            self.config.update_configuration(defaultValues)

            config = PacmanConfig()
            self.log_path = config.option("LogFile").value

            self.iconInstall = getIconFromTheme(
                ICON_TOINSTALL, 24, ICON_TOINSTALL_FALLBACK)
            self.iconRemove = getIconFromTheme(
                ICON_PURGE, 24, ICON_PURGE_FALLBACK)
            self.iconUpgrade = getIconFromTheme(
                ICON_UPGRADE, 24, ICON_UPGRADE_FALLBACK)

        self.interface = parent

        # Get today date
        self.__today = date.today()

        # Arrays
        self.dayCache = list()
        self.dataStore = list()

        # Dictionnaries
        self.statusList = dict(
            installed=_("Installation"),
            upgraded=_("Upgrade"),
            removed=_("Remove") )

        self.statusBool = dict(
            installed=True,
            upgraded=True,
            removed=True,
            color=False )

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_main_icon(gtk.STOCK_FILE, gtk.ICON_SIZE_BUTTON)

        self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
        self.set_default_response(gtk.RESPONSE_CLOSE)

        # ------------------------------------
        #       Grid
        # ------------------------------------

        boxInterface = gtk.HPaned()

        boxCalendarFilters = gtk.VBox()
        boxFilters = gtk.VBox()

        viewFilters = gtk.Viewport()
        scrollFilters = gtk.ScrolledWindow()

        # Properties
        boxCalendarFilters.set_border_width(4)

        boxFilters.set_border_width(4)

        viewFilters.set_shadow_type(gtk.SHADOW_NONE)
        scrollFilters.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrollFilters.set_border_width(4)

        # ------------------------------------
        #       Calendar
        # ------------------------------------

        self.calendar = gtk.Calendar()

        # Properties
        self.calendar.mark_day(self.__today.day)
        self.calendar.select_day(self.__today.day)
        self.calendar.select_month(self.__today.month - 1, self.__today.year)
        self.calendar.set_size_request(220, -1)

        # ------------------------------------
        #       Filters
        # ------------------------------------

        frameFilters = gtk.Frame()
        frameFiltersLabel = gtk.Label("<b>%s</b>" % _("Selection filters"))

        self.filter_checkInstallation = gtk.CheckButton(_("Show Installation"))
        self.filter_checkRemove = gtk.CheckButton(_("Show Remove"))
        self.filter_checkUpgrade = gtk.CheckButton(_("Show Upgrade"))
        self.filter_checkColors = gtk.CheckButton(_("Use colors"))

        self.entryFilter = gtk.Entry()

        # Properties
        frameFilters.set_label_widget(frameFiltersLabel)
        frameFilters.set_shadow_type(gtk.SHADOW_NONE)

        frameFiltersLabel.set_use_markup(True)

        self.filter_checkInstallation.set_alignment(0, 0.5)
        self.filter_checkInstallation.set_active(True)

        self.filter_checkRemove.set_alignment(0, 0.5)
        self.filter_checkRemove.set_active(True)

        self.filter_checkUpgrade.set_alignment(0, 0.5)
        self.filter_checkUpgrade.set_active(True)

        self.filter_checkColors.set_alignment(0, 0.5)
        self.filter_checkColors.set_active(False)

        self.entryFilter.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        # ------------------------------------
        #       Treeview
        # ------------------------------------

        self.listLog = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str, str, bool)
        self.filterLog = self.listLog.filter_new()
        self.sortLog = gtk.TreeModelSort(self.filterLog)

        self.treeviewLog = gtk.TreeView(self.sortLog)

        columnHour = gtk.TreeViewColumn(_("Hour"))
        columnPackage = gtk.TreeViewColumn(_("Package"))

        cellHour = gtk.CellRendererText()
        cellAction = gtk.CellRendererPixbuf()
        cellPackage = gtk.CellRendererText()

        scrollTreeview = gtk.ScrolledWindow()

        # Properties
        self.treeviewLog.set_rules_hint(True)
        self.treeviewLog.set_search_column(2)
        self.treeviewLog.set_enable_search(True)
        self.treeviewLog.set_headers_clickable(True)

        self.sortLog.set_sort_column_id(0, gtk.SORT_ASCENDING)

        columnHour.set_sort_column_id(0)
        columnPackage.set_sort_column_id(2)

        columnHour.pack_start(cellHour, False)
        columnPackage.pack_start(cellAction, False)
        columnPackage.pack_start(cellPackage)

        columnHour.add_attribute(cellHour, "text", 0)
        columnPackage.add_attribute(cellAction, "pixbuf", 1)
        columnPackage.add_attribute(cellPackage, "text", 2)

        columnHour.set_attributes(cellHour, text=0)
        columnPackage.set_attributes(cellPackage, text=2,
            foreground=4, foreground_set=5)

        self.treeviewLog.append_column(columnHour)
        self.treeviewLog.append_column(columnPackage)

        scrollTreeview.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrollTreeview.set_border_width(4)

        self.treeview_fill(self.__get_short_date(False))
        self.filterLog.set_visible_func(self.filter_match)

        # ------------------------------------
        #   Add widgets into main interface
        # ------------------------------------

        # Filters
        boxFilters.pack_start(self.entryFilter, False, True)
        boxFilters.pack_start(gtk.HSeparator(), False, True, 6)
        boxFilters.pack_start(self.filter_checkInstallation, False, True)
        boxFilters.pack_start(self.filter_checkRemove, False, True)
        boxFilters.pack_start(self.filter_checkUpgrade, False, True)
        boxFilters.pack_start(self.filter_checkColors, False, True, 4)

        frameFilters.add(boxFilters)
        viewFilters.add(frameFilters)
        scrollFilters.add(viewFilters)

        # Calendar
        boxCalendarFilters.pack_start(self.calendar, False, True)
        boxCalendarFilters.pack_start(scrollFilters, True, True, 8)

        # Treeview
        scrollTreeview.add(self.treeviewLog)

        boxInterface.pack1(boxCalendarFilters, False, False)
        boxInterface.pack2(scrollTreeview, True, False)

        self.add_widget(boxInterface)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.filter_checkInstallation.connect("toggled",
            self.filter_check, "installed")
        self.filter_checkRemove.connect("toggled",
            self.filter_check, "removed")
        self.filter_checkUpgrade.connect("toggled",
            self.filter_check, "upgraded")

        self.filter_checkColors.connect("toggled", self.treeview_change_color)

        self.entryFilter.connect("icon-press", self.entry_clear)
        self.entryFilter.connect("changed", self.entry_refilter)

        self.calendar.connect("day-selected", self.__set_calendar_day)
        self.calendar.connect("month-changed", self.__set_calendar_day)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.show_all()

        self.__parse_log()

        self.__set_calendar_day(self.calendar)

        self.run()

        self.destroy()

    # ------------------------------------
    #       Interface functions
    # ------------------------------------

    def __parse_log (self):
        """
        Parse pacman log
        """

        currentDate = self.__get_short_date()

        with open(self.log_path, 'r') as file:

            for line in file:

                string = line.split(' ')
                if string[2] in self.statusList.keys():

                    pkgDate = ' '.join(string[0:2])[1:-1]
                    pkgStatus = string[2]
                    pkgName = string[3]
                    pkgVersion = ' '.join(string[4:])[1:-2]

                    pkgDateParse = self.__get_date_array(pkgDate)
                    pkgDateCheck = "%s/%s" % (pkgDateParse[0], pkgDateParse[2])

                    if currentDate == pkgDateCheck:
                        self.calendar.mark_day(int(pkgDateParse[1]))

                    self.dataStore.append(
                        [pkgDate, pkgStatus, pkgName, pkgVersion])


    def __get_short_date (self, withoutDay=True):
        """
        Get a short format for date to be easier to use
        """

        calYearIso, calMonth, calDay = self.calendar.get_date()

        # Month is zero-based
        calMonth += 1

        if withoutDay:
            return date(calYearIso, calMonth, calDay).strftime("%m/%y")

        else:
            return date(calYearIso, calMonth, calDay).strftime("%m/%d/%y")


    def __get_date_array (self, dateValue):
        """
        Get an array from a log date
        """

        # mm/dd/yy hh:mn
        pattern = "^(\d{2})/(\d{2})/(\d{2}) (\d{2}:\d{2})$"

        return findall(pattern, dateValue)[0]


    def __set_calendar_day (self, widget):
        """
        Event when user change month from calendar widget
        """

        self.dayCache = list()

        widget.clear_marks()

        self.treeview_fill(self.__get_short_date(False))


    def entry_clear (self, widget, pos, event):
        """
        Reset entry filter when icon was clicked
        """

        if len(self.entryFilter.get_text()) > 0:
            self.entryFilter.set_text("")
            self.filterLog.refilter()


    def entry_refilter (self, widget):
        """
        Filter results with entry text
        """

        self.filterLog.refilter()


    def treeview_fill (self, dateValue, useCache=False):
        """
        Fill liststore for a specific date
        """

        # Get user custom colors
        colorPurge = self.config.get("colors", "remove")
        colorInstall = self.config.get("colors", "install")
        colorUpgrade = self.config.get("colors", "upgrade")

        self.listLog.clear()

        if useCache:
            listStore = self.dayCache
        else:
            listStore = self.dataStore

        for element in listStore:

            pkgDate = element[0]
            pkgStatus = element[1]
            pkgName = element[2]
            pkgVersion = element[3].replace("->", "→")

            if pkgStatus == "installed":
                fgColor = colorInstall
                icon = self.iconInstall
                pkgText = "%s - %s" % (pkgName, pkgVersion)

            elif pkgStatus == "removed":
                fgColor = colorPurge
                icon = self.iconRemove
                pkgText = "%s - %s" % (pkgName, pkgVersion)

            elif pkgStatus == "upgraded":
                fgColor = colorUpgrade
                icon = self.iconUpgrade
                pkgText = "%s (%s)" % (pkgName, pkgVersion)

            useColor = self.filter_checkColors.get_active()

            pkgDateParse = self.__get_date_array(pkgDate)
            pkgDateCheck = '/'.join(pkgDateParse[0:3])

            if pkgDateCheck == dateValue:
                self.listLog.append([pkgDateParse[3], icon, pkgText,
                    pkgStatus, fgColor, useColor])

            if not useCache:
                self.dayCache.append([pkgDate, pkgStatus, pkgName, pkgVersion])

            pkgDateParse = self.__get_date_array(pkgDate)
            pkgDateCheck = "%s/%s" % (pkgDateParse[0], pkgDateParse[2])

            if pkgDateCheck == self.__get_short_date():
                self.calendar.mark_day(int(pkgDateParse[1]))

        self.filterLog.refilter()


    def treeview_change_color (self, widget):
        """
        Set color mode to rows
        """

        for row in self.listLog:
            row[5] = not row[5]


    def filter_match (self, model, row, data=None):
        """
        Update treeview filter
        """

        entry = self.entryFilter.get_buffer().get_text()

        name = model.get_value(row, 2)
        status = model.get_value(row, 3)

        if not name == None:
            name = name.split(' ')[0]

        if self.statusBool["installed"] and status == "installed" and \
            entry in name:
            return True
        elif self.statusBool["removed"] and status == "removed" and \
            entry in name:
            return True
        elif self.statusBool["upgraded"] and status == "upgraded" and \
            entry in name:
            return True

        return False


    def filter_check (self, widget, option):
        """
        Use the checked filters
        """

        self.statusBool[option] = widget.get_active()
        self.filterLog.refilter()
