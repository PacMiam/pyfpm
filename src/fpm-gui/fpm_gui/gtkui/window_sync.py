# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os.path import exists
from os.path import basename
from os.path import expanduser

# Interface
import gtk

from window import Window
from window import WindowPopup
from window_vte import WindowVte

# Log
from logging import getLogger

# Modules
from fpm_gui import *

from fpmd.tools.utils import getFolderSize
from fpmd.tools.utils import getTranslation
from fpmd.tools.config import Config
from fpmd.tools.package import Command
from fpmd.tools.package import Database

from modules import getIconFromTheme

# Regex
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowSyncFpm (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Local installation"),
            _("Install a local Frugalware package (*.fpm)"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        filename = None

        if parent is not None:
            database = parent.pacman

            icon = getIconFromTheme(ICON_AVAILABLE, 48,
                theme=parent.icons_theme)

        else:
            database = Database()

            icon = getIconFromTheme(ICON_AVAILABLE, 48)

            if type(widget) == str and exists(widget):
                filename = widget

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_default_size(-1, -1)

        self.set_main_icon(icon)

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_APPLY, gtk.RESPONSE_ACCEPT)

        self.set_default_response(gtk.RESPONSE_ACCEPT)

        # ------------------------------------
        #   Local package chooser
        # ------------------------------------

        filter_fpm = gtk.FileFilter()

        self.chooser_fpm = gtk.FileChooserButton(
            _("Select a Frugalware package file"), None)

        # Properties
        filter_fpm.set_name(_("Frugalware package"))
        filter_fpm.add_pattern("*.fpm")

        self.chooser_fpm.add_filter(filter_fpm)
        self.chooser_fpm.set_current_folder(expanduser('~'))

        if not filename == None:
            self.chooser_fpm.set_filename(filename)

        self.chooser_fpm.set_size_request(500, -1)

        # ------------------------------------
        #   Integrate widgets
        # ------------------------------------

        self.add_widget(self.chooser_fpm, False, False)

        if self.start() == gtk.RESPONSE_ACCEPT:

            filePath = self.chooser_fpm.get_filename()
            if not filePath == None:
                result = findall(REGEX_LOCAL_PACKAGE, filePath)[0]

                flags = dict(path=filePath)
                if database.package_status(result[0], result[1]):
                    flags["status"] = True

                action = Command()
                action.package()

                if action.generate(flags):
                    termVte = WindowVte(parent, _("Packages installer"),
                        _("Install <b>%s</b> local package") % (
                        basename(filePath)), action.command, True)

                    if termVte.success:
                        database.reload_daemon()

                        if parent is not None:
                            parent.reload_interface()
                            parent.upgrade_get(True)
                            parent.statusbar_update()

                else:
                    WindowPopup(_("An error occur for %s" % "WindowSyncFpm"),
                        _("Can't generate command with flags: %s" % str(flags)),
                        gtk.MESSAGE_ERROR)


class WindowUpdate (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Upgrade system"),
            _("The following updates are available"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__parent = parent

        self.__index = 0

        self.__checked_list = list()
        self.__packages_list = list()

        self.__update_list = parent.packages_data.get("upgrade")
        self.__installation_list = parent.packages_data.get("installation")

        # ------------------------------------
        #   Create update list
        # ------------------------------------

        for data in self.__update_list.keys():
            if not data in self.__installation_list:
                self.__packages_list.append([data, self.__update_list[data]])

        # ------------------------------------
        #   Interface
        # ------------------------------------

        if len(self.__packages_list) > 0:

            # Init widgets
            self.__init_widgets()

        else:
            WindowPopup(_("Update packages"), _("No package to update"))


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_default_size(400, 400)

        self.set_main_icon(self.__parent.category_upgrade)

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_ADD, gtk.RESPONSE_ACCEPT)

        self.set_default_response(gtk.RESPONSE_ACCEPT)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        scroll = gtk.ScrolledWindow()

        # Properties
        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Treeview
        # ------------------------------------

        self.store = gtk.ListStore(bool, str, str)

        treeview = gtk.TreeView()

        # Properties
        treeview.set_model(self.store)
        treeview.set_hover_selection(True)
        treeview.set_headers_visible(False)

        # ------------------------------------
        #   Cells
        # ------------------------------------

        self.cell_status = gtk.CellRendererToggle()
        cell_name = gtk.CellRendererText()
        cell_version = gtk.CellRendererText()

        # Properties
        self.cell_status.set_property("active", True)
        self.cell_status.set_property("activatable", True)

        # ------------------------------------
        #   Column
        # ------------------------------------

        column = gtk.TreeViewColumn()

        # Properties
        column.pack_start(self.cell_status, False)
        column.pack_start(cell_name, True)
        column.pack_start(cell_version, False)

        column.add_attribute(self.cell_status, "active", 0)
        column.add_attribute(cell_name, "text", 1)
        column.add_attribute(cell_version, "text", 2)

        # ------------------------------------
        #   Integrate widgets
        # ------------------------------------

        treeview.append_column(column)

        scroll.add(treeview)

        self.add_widget(scroll)

        # ------------------------------------
        #   Append packages
        # ------------------------------------

        self.__packages_list.sort()

        # Add updates in treeview
        for name, version in self.__packages_list:
            self.store.append([True, name, version])

        # ------------------------------------
        #   Connect signal
        # ------------------------------------

        self.cell_status.connect("toggled",
            self.__parent.treeview_checked_row, self.store)

        # ------------------------------------
        #   Start interface
        # ------------------------------------

        if self.start() == gtk.RESPONSE_ACCEPT:

            # Get checked packages
            for status, name, version in self.store:

                if status:
                    # Add the update to installation list
                    self.__installation_list.append(name)
                    self.__index += 1

                    # Get package for update data into treeview
                    self.__checked_list.append(name)

                    # Avoid to reselect this package again
                    del self.__update_list[name]

        # Update packages treeview if necessary
        if len(self.__checked_list) > 0:

            for repository in self.__parent.treeview_data:
                model = self.packages_data_widgets[repository].store

                for name in self.__parent.treeview_data[repository]:

                    if name in self.__checked_list:
                        row = self.__parent.treeview_data[repository][name]

                        if len(model.get_value(row, 4)) > 0:
                            model[row][1] = self.icon_to_install
                            model[row][6] = colorInstall

        if self.__index == 1:
            WindowPopup(_("Packages added succesfully"),
                _("One package has been add to your selection"))

        elif self.__index > 1:
            WindowPopup(_("Packages added succesfully"),
                _("%d packages has been add to your selection") % self.__index)

        self.__parent.statusbar_update()


class WindowBuild (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Building package"),
            _("Do you want to build %s ?") % (parent.selected["menu"]))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__repositoryName = parent.notebook_tab_name()

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_default_size(-1, -1)

        self.set_main_icon(parent.category_transaction)

        self.add_button(gtk.STOCK_NO, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_YES, gtk.RESPONSE_ACCEPT)

        self.set_default_response(gtk.RESPONSE_ACCEPT)

        self.set_resizable(False)
        self.set_has_separator(False)

        if self.start() == gtk.RESPONSE_ACCEPT:
            parent.statusbar_update()

            if len(parent.selected["menu"]) > 0 and \
                not parent.selected["menu"] == None:

                action = Command()
                action.repoman()

                if action.generate(dict(name=parent.selected["menu"])):

                    termVte = WindowVte(parent, _("Packages builder"),
                        _("Build <b>%s</b> package" % parent.selected["menu"]),
                        action.command, True)

                    if termVte.success:
                        parent.pacman.nobuild_reset(repositoryName)
                        parent.pacman.packages_installed(True)

                        parent.reload_interface()

                        self.upgrade_get()

                        WindowPopup(_("Building package"),
                            _("Finished to build %s") % (
                            parent.selected["menu"]))

                else:
                    WindowPopup(_("Error during command generation"),
                        _("Can't generate command for %s") % "WindowBuild",
                        gtk.MESSAGE_ERROR)

        parent.statusbar_update()


class WindowRepoman (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Packages builder installation"),
            _("Do you want to install packages builder ?"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        firstuse_status = False
        update_config = False

        self.__listPackages = list()

        self.__listNeeded = ["pacman-tools", "git", "bc", "readline5", "sudo",
            "autoconf", "bison", "ccache", "fakeroot", "kernel-headers", "m4",
            "make", "patch", "rsync"]

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_default_size(450, -1)

        self.set_main_icon(parent.category_transaction)

        self.add_button(gtk.STOCK_NO, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_YES, gtk.RESPONSE_ACCEPT)

        self.set_default_response(gtk.RESPONSE_ACCEPT)

        self.set_resizable(False)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        box = gtk.VBox()

        # Properties
        box.set_border_width(4)

        # ------------------------------------
        #   Informations area
        # ------------------------------------

        label_description = gtk.Label()
        label_packages = gtk.Label()

        check_firstuse = gtk.CheckButton()

        # Properties
        label_description.set_text(_("Packages builder help you "
            "to install not supported packages, like steam or skype."))
        label_description.set_line_wrap(True)
        label_description.set_size_request(400, -1)
        label_description.set_justify(gtk.JUSTIFY_FILL)

        label_packages.set_use_markup(True)

        check_firstuse.set_label(_("Not show this message again"))
        if parent.config.get("application", "first_use"):
            check_firstuse.set_active(False)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        box.pack_start(
            label_description, False, True, 8)

        # Check not installed packages
        for package in self.__listNeeded:
            if not parent.pacman.package_installed(package)[0]:
                self.__listPackages.append(package)

        # If there are some important packages not installed
        if len(self.__listPackages) > 0:
            label_packages.set_markup("%s: <b>%s</b>" % (
                _("This packages are going to be installed"),
                ', '.join(self.__listPackages)))

            box.pack_start(label_packages, False, True, 4)

        box.pack_start(check_firstuse, False, True, 4)

        self.add_widget(box)
        self.show_all()

        if self.run() == gtk.RESPONSE_ACCEPT:
            firstuse_status = check_firstuse.get_active()
            self.destroy()

            action = Command()
            action.sync()

            flags = dict(install_force=True, install_noconfirm=True,
                list_installation=self.__listPackages)

            if action.generate(flags):
                termVte = WindowVte(self, _("Packages builder"),
                    _("This packages are usefull to build your own packages"),
                    action.command, True)

                if termVte.success:
                    parent.reload_interface()

                    if parent.repoman:
                        parent.pacman.nobuild_reset()

                    update_config = True

        else:
            firstuse_status = check_firstuse.get_active()
            self.destroy()

            update_config = True

        if firstuse_status and update_config:
            parent.config.set("application", "first_use", not firstuse_status)
            parent.config.write_configuration()

        parent.statusbar_update()


class WindowCache (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Clear packages cache"),
            _("What did you want to do with packages cache ?"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        if parent is not None:
            database = parent.pacman

        else:
            database = Database()

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_default_size(-1, -1)

        self.set_main_icon(parent.category_cache)

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_APPLY, gtk.RESPONSE_ACCEPT)

        self.set_default_response(gtk.RESPONSE_ACCEPT)

        self.set_resizable(False)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        box = gtk.VBox()

        # Properties
        box.set_border_width(6)

        # ------------------------------------
        #   Mode
        # ------------------------------------

        radio_old = gtk.RadioButton(None,
            _("Remove only old packages from cache"), None)
        radio_all = gtk.RadioButton(radio_old,
            _("Remove all packages from cache"), None)

        # ------------------------------------
        #   Cache
        # ------------------------------------

        label_cache = gtk.Label()

        # Properties
        label_cache.set_alignment(0, 0.5)
        label_cache.set_text(_("Currently, the cache size is %0.1f MB") % (
            getFolderSize(database.configuration_value("CacheDir")) / (
            1024 * 1024.0)))

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        box.pack_start(radio_old)
        box.pack_start(radio_all)

        self.add_widget(box, False, True)
        self.add_widget(label_cache, False, True)

        if self.start() == gtk.RESPONSE_ACCEPT:
            action = Command()
            action.cache()

            flags = dict(mode=radio_all.get_active())

            if action.generate(flags):
                termVte = WindowVte(self, _("Clear packages cache"),
                    _("The packages cache going to be cleared."),
                    action.command, True)

                if termVte.success:
                    WindowPopup(_("Cache packages cleared"),
                        _("Finished clearing the cache packages"))

            else:
                WindowPopup(_("Error during command generation"),
                    _("Can't generate command for clean cache"),
                    gtk.MESSAGE_ERROR)

        parent.statusbar_update()
