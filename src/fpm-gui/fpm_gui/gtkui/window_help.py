# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Interface
import gtk

from window import Window

# Log
from logging import getLogger

# Modules
from fpm_gui import __version__
from fpm_gui import __website__
from fpm_gui import __copyright__

from fpmd.tools.utils import getTranslation

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowHelp (Window):

    def __init__ (self, widget, parent):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Software help"))

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_default_size(600, 600)

        self.set_main_icon(gtk.STOCK_HELP, gtk.ICON_SIZE_MENU)

        self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        self.set_default_response(gtk.RESPONSE_CLOSE)

        # ------------------------------------
        #   Box
        # ------------------------------------

        box = gtk.VBox()

        viewport = gtk.Viewport()
        scroll = gtk.ScrolledWindow()

        # Properties
        viewport.set_shadow_type(gtk.SHADOW_NONE)
        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Icons shortcuts
        # ------------------------------------

        boxIcons = gtk.Table()

        frameIcons = gtk.Frame()
        labelIcons = gtk.Label()

        # Properties
        boxIcons.set_border_width(4)

        frameIcons.set_label_widget(labelIcons)
        frameIcons.set_border_width(4)

        labelIcons.set_markup(" <b>%s</b> " % _("Icons"))
        labelIcons.set_alignment(0, 0.5)
        labelIcons.set_use_markup(True)

        # ------------------------------------
        #   Interface shortcuts
        # ------------------------------------

        boxInterface = gtk.Table()

        frameInterface = gtk.Frame()
        labelInterface = gtk.Label()

        # Properties
        boxInterface.set_border_width(4)

        frameInterface.set_label_widget(labelInterface)
        frameInterface.set_border_width(4)

        labelInterface.set_markup(" <b>%s</b> " % _("Interface shortcuts"))
        labelInterface.set_alignment(0, 0.5)
        labelInterface.set_use_markup(True)

        # ------------------------------------
        #   Packages shortcuts
        # ------------------------------------

        boxPackages = gtk.Table()

        framePackages = gtk.Frame()
        labelPackages = gtk.Label()

        # Properties
        boxPackages.set_border_width(4)

        framePackages.set_label_widget(labelPackages)
        framePackages.set_border_width(4)

        labelPackages.set_markup(" <b>%s</b> " % _("Packages shortcuts"))
        labelPackages.set_alignment(0, 0.5)
        labelPackages.set_use_markup(True)

        # ------------------------------------
        #   Add data into interface
        # ------------------------------------

        self.__create_grid(
            parent.helpShortcuts.get("icons"), boxIcons)
        self.__create_grid(
            parent.helpShortcuts.get("interfaces"), boxInterface)
        self.__create_grid(
            parent.helpShortcuts.get("packages"), boxPackages)

        # ------------------------------------
        #   Integrate widgets into interface
        # ------------------------------------

        frameIcons.add(boxIcons)
        frameInterface.add(boxInterface)
        framePackages.add(boxPackages)

        box.pack_start(frameIcons, False, True)
        box.pack_start(frameInterface, False, True, 4)
        box.pack_start(framePackages, False, True)

        viewport.add(box)
        scroll.add(viewport)

        self.add_widget(scroll)

        self.show_all()

        self.run()

        self.destroy()


    def __create_grid (self, shortcuts, grid):
        """
        Quick add shortcuts into selected grid
        """

        index = 0

        for shortcut in shortcuts:

            # Add widgets
            if len(shortcut) > 0:

                if len(shortcut) == 2 or shortcut[2]:

                    # Description
                    labelText = gtk.Label(shortcut[1])

                    # Show icon
                    if type(shortcut[0]) == gtk.gdk.Pixbuf:
                        imageShortcut = gtk.Image()
                        widgetShortcut = gtk.Alignment(0.5, 0.5, 0, 0)

                        # Properties
                        imageShortcut.set_from_pixbuf(shortcut[0])
                        widgetShortcut.add(imageShortcut)

                        labelText.set_alignment(0, 0.5)

                    # Show shortcut
                    else:
                        widgetShortcut = gtk.Label(shortcut[0])

                        # Properties
                        widgetShortcut.set_alignment(0, 0.5)

                        labelText.set_alignment(1, 0.5)

                    grid.attach(widgetShortcut, 0, 1, index, index + 1,
                        xoptions=gtk.FILL, yoptions=gtk.FILL,
                        xpadding=8, ypadding=4)
                    grid.attach(labelText, 1, 2, index, index + 1,
                        yoptions=gtk.FILL, xpadding=2, ypadding=4)

            # Add a separator
            else:
                grid.attach(gtk.HSeparator(), 0, 2, index, index + 1,
                    yoptions=gtk.FILL, xpadding=2, ypadding=4)

            index += 1


class WindowAbout (gtk.AboutDialog):

    def __init__ (self, widget, parent):
        """
        Constructor
        """

        gtk.AboutDialog.__init__(self)

        self.set_program_name("fpm-gui")
        self.set_version(__version__)
        self.set_logo(parent.icon_load("fpm-gui", 96))
        self.set_comments(_("Graphical packages manager for Frugalware"))
        self.set_website(__website__)
        self.set_copyright(__copyright__)

        # Authors
        self.set_authors(
            [_("Active developpers"),
            "\tAurélien Lubert (PacMiam)",
            "\n%s" % _("Former developpers"),
            "\tGaetan Gourdin (bouleetbil)",
            "\tAnthony Jorion (Pingax)",
            "\n%s" % _("Logo"),
            "\tTango Desktop Project (GPLv3)"])
        self.set_translator_credits("\n".join(
            ["fr_FR\t Anthony Jorion (Pingax)",
            "es_ES\t José Luis (Darknekros)"]))

        # License
        self.set_license("fpm-gui and fpmd are free software: you can "
            "redistribute it and/or modify it under the terms of the GNU "
            "General Public License as published by the Free Software "
            "Foundation, either version 3 of the License, or (at your option) "
            "any later version.\n\nfpm-gui and fpmd are distributed in the "
            "hope that it will be useful, but WITHOUT ANY WARRANTY; without "
            "even the implied warranty of MERCHANTABILITY or FITNESS FOR A "
            "PARTICULAR PURPOSE. See the GNU General Public License for more "
            "details.\n\nCheck /usr/share/doc/fpm-gui-%s/LICENSE for more "
            "informations.\n\nhttp://www.gnu.org/licenses/" % __version__)
        self.set_wrap_license(True)

        self.run()

        self.destroy()
