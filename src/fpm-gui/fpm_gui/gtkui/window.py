# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os.path import exists
from os.path import expanduser

# Log
from logging import getLogger

# Interface
import gtk

from modules import update_widget_size

# Pyfpm
from fpmd.tools.utils import getPixmap
from fpmd.tools.utils import getTranslation

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Window (gtk.Dialog):

    def __init__ (self, parent, title=None, subtitle=None):
        """
        Constructor
        """

        gtk.Dialog.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__showed = False
        self.__destroy = False

        self.__parent = parent

        self.__title = title
        self.__subtitle = subtitle

        self.__help_title = None
        self.__help_text = None

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_border_width(8)
        self.set_has_separator(True)
        self.set_default_size(800, 600)

        self.set_modal(gtk.DIALOG_MODAL)
        self.set_transient_for(self.__parent)
        self.set_position(gtk.WIN_POS_CENTER_ON_PARENT)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.HBox()
        box_title = gtk.VBox()

        # ------------------------------------
        #   Header
        # ------------------------------------

        self.imageHeader = gtk.Image()
        self.labelTitle = gtk.Label()
        self.labelSubtitle = gtk.Label()

        self.imageInfo = gtk.Image()
        self.buttonInfo = gtk.Button()

        # Properties
        self.labelTitle.set_alignment(0, 0.5)
        self.labelTitle.set_use_markup(True)

        self.labelSubtitle.set_alignment(0, 0.5)
        self.labelSubtitle.set_use_markup(True)

        self.imageInfo.set_from_stock(gtk.STOCK_INFO, gtk.ICON_SIZE_BUTTON)
        self.buttonInfo.set_use_stock(True)
        self.buttonInfo.set_no_show_all(True)
        self.buttonInfo.set_image(self.imageInfo)
        self.buttonInfo.set_relief(gtk.RELIEF_NONE)

        # ------------------------------------
        #   Add widgets into main window
        # ------------------------------------

        box_title.pack_start(self.labelTitle)

        if not self.__subtitle is None:
            box_title.pack_start(self.labelSubtitle)

        box.pack_start(self.imageHeader, False, False)
        box.pack_start(box_title, padding=12)
        box.pack_start(self.buttonInfo, False, False)

        self.vbox.pack_start(box, False, True, 4)
        self.vbox.pack_start(gtk.HSeparator(), False, True, 4)

        # ------------------------------------
        #   Special cases
        # ------------------------------------

        if not self.__title is None:
            self.set_header_title(self.__title)

        if not self.__subtitle is None:
            if not type(self.__subtitle) == list:
                self.labelSubtitle.set_markup(self.__subtitle)

            else:
                self.labelSubtitle.set_markup(self.__subtitle[0])

                for element in self.__subtitle[1:]:
                    label = gtk.Label(element)

                    label.set_alignment(0, 0.5)
                    label.set_use_markup(True)

                    box_title.pack_start(label, True, True, 2)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.buttonInfo.connect("clicked", self.__show_help)


    def start (self):
        """
        Run interface and destroy it correctly
        """

        self.show_all()

        response = self.run()
        self.destroy()

        return response

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    @property
    def icon (self):
        """
        Get main icon
        """

        imageType = self.imageHeader.get_storage_type()

        if imageType == gtk.IMAGE_PIXBUF:
            return self.imageHeader.get_pixbuf()

        elif imageType == gtk.IMAGE_PIXMAP:
            return self.imageHeader.get_pixmap()

        elif imageType == gtk.IMAGE_ICON_SET:
            return self.imageHeader.get_icon_set()

        elif imageType == gtk.IMAGE_ANIMATION:
            return self.imageHeader.get_animation()

        elif imageType == gtk.IMAGE_STOCK:
            return self.imageHeader.get_stock()

        else:
            return None


    def set_main_icon (self, icon, size=None):
        """
        Set main icon and header icon
        """

        if type(icon) == gtk.gdk.Pixbuf:
            self.imageHeader.set_from_pixbuf(icon)

        elif type(icon) == gtk.gdk.Pixmap:
            self.imageHeader.set_from_pixmap(icon)

        elif type(icon) == gtk.IconSet and size is not None:
            self.imageHeader.set_from_icon_set(icon, size)

        elif type(icon) == gtk.gdk.PixbufAnimation:
            self.imageHeader.set_from_animation(icon)

        elif type(icon) == str:
            if exists(expanduser(icon)):
                self.imageHeader.set_from_file(icon)

            elif size is not None:
                self.imageHeader.set_from_stock(icon, size)

        # Set also window icon
        if type(icon) == gtk.gdk.Pixbuf:
            self.set_icon(icon)


    @property
    def help_informations (self):
        """
        Get title and text for help window
        """

        return self.__help_title, self.__help_text


    def set_help_informations (self, title, text):
        """
        Set title and text for help window
        """

        self.__help_title = title
        self.__help_text = text

        self.buttonInfo.show()


    def set_header_title (self, title):
        """
        Set header title
        """

        self.set_title(title)

        self.labelTitle.set_markup("<span font='14'><b>%s</b></span>" % title)

        self.__title = title


    def set_header_subtitle (self, title):
        """
        Set header subtitle
        """

        self.labelSubtitle.set_markup(title)

    # ------------------------------------
    #   Methods
    # ------------------------------------

    def __show_help (self, widget):
        """
        Show help window
        """

        if self.__help_title is None:
            self.__help_title = self.__title

        if not self.__help_text is None:
            WindowPopup(self.__help_title, self.__help_text)


    def set_sensitive (self, response, status=None):
        """
        Set sensitive status for a specific button
        """

        if status is None:
            status = not self.get_response_sensitive()

        self.set_response_sensitive(response, status)


    def add_widget (self, widget, expand=True, fill=True, padding=4):
        """
        Add a widget into main grid
        """

        self.vbox.pack_start(widget, expand, fill, padding)


class WindowPopup (gtk.MessageDialog):

    def __init__(self, title=str(), text=str(), icon=gtk.MESSAGE_INFO):
        """
        Constructor
        """

        gtk.MessageDialog.__init__(self, message_format=title)

        if type(icon) == gtk._gtk.MessageType:
            self.set_property("message-type", icon)
        else:
            self.set_image(icon)

        self.set_title(title)
        self.set_destroy_with_parent(True)
        self.set_modal(gtk.DIALOG_MODAL)

        self.format_secondary_markup(text)

        self.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)
        self.set_default_response(gtk.RESPONSE_OK)

        self.get_message_area().set_spacing(2)

        label = self.get_message_area().children()[1]
        label.set_line_wrap(True)
        label.connect("size-allocate", update_widget_size)

        self.run()
        self.destroy()


class WindowQuestion (gtk.MessageDialog):

    def __init__ (self, title=str(), text=str()):
        """
        Constructor
        """

        gtk.MessageDialog.__init__(self)

        self.set_title(title)
        self.set_destroy_with_parent(True)
        self.set_modal(gtk.DIALOG_MODAL)

        self.set_markup("<span font='12'><b>%s</b></span>" % title)
        self.format_secondary_markup(text)

        self.set_property("message-type", gtk.MESSAGE_QUESTION)

        self.add_button(gtk.STOCK_NO, gtk.RESPONSE_NO)
        self.add_button(gtk.STOCK_YES, gtk.RESPONSE_YES)
        self.set_default_response(gtk.RESPONSE_NO)

        self.get_message_area().set_spacing(2)


    def start (self):

        response = self.run()
        self.destroy()

        if response == gtk.RESPONSE_YES:
            return True

        return False
