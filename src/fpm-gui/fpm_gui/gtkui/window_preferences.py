# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from subprocess import PIPE
from subprocess import Popen

# Files
from glob import glob
from copy import deepcopy

from os import W_OK
from os import mkdir
from os import access
from os.path import exists
from os.path import dirname
from os.path import basename

# Interface
import gtk

from vte import Terminal
from modules import refresh
from window import Window

# Log
from logging import getLogger

# Modules
from fpm_gui import RED
from fpm_gui import BLUE
from fpm_gui import GREEN
from fpm_gui import ORANGE
from fpm_gui import CONFIGFILE
from fpm_gui import defaultValues
from fpm_gui import REGEX_MIRROR_INTEGRITY

from fpmd import DBPATH_FILE
from fpmd import LOGPATH_FILE
from fpmd import CACHEDIR_FILE

from fpmd.tools.utils import *
from fpmd.tools.config import Config
from fpmd.tools.network import Network
from fpmd.tools.package import Database
from fpmd.tools.pacmanconf import PacmanConfig
from fpmd.tools.pacmanconf import PacmanRepository

# Regex
from re import search
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowPreferences (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Preferences"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.interface = parent

        if self.interface is not None:
            self.config = self.interface.config
            self.pacman = self.interface.pacman

            self.colorschemes = self.interface.colorschemes
            self.cachePath = self.interface.cachePath
            self.__debug = self.interface.debug

        else:
            self.config = Config(CONFIGFILE)
            self.config.update_configuration(defaultValues)

            self.pacman = Database()

            self.colorschemes = list()
            self.cachePath = self.config.get("application", "cache")
            self.__debug = False

        # Default frame shadow
        self.frame_type = gtk.SHADOW_OUT

        self.widgets = dict()
        self.widgets_pacman = dict()

        self.config_pacman_general = dict()
        self.config_pacman_path = dict()

        # ------------------------------------
        #   Pacman-g2 configuration
        # ------------------------------------

        self.pacman_config = PacmanConfig()

        self.pacmang2Update = False
        self.pacmang2Modification = False

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Notebook tabs
        # ------------------------------------

        self.__tab_general = TabGeneral(self)
        self.__tab_editor = TabEditor(self)
        self.__tab_terminal = TabTerminal(self)
        self.__tab_packages = TabPackages(self)

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_main_icon(gtk.STOCK_PREFERENCES, gtk.ICON_SIZE_MENU)

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_SAVE, gtk.RESPONSE_APPLY)
        self.set_default_response(gtk.RESPONSE_APPLY)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        notebook = gtk.Notebook()

        labelGeneral = gtk.Label(_("General"))
        labelInterface = gtk.Label(_("Visual"))
        labelTerminal = gtk.Label(_("Terminal"))
        labelPackages = gtk.Label(_("Packages"))

        # Properties
        notebook.set_border_width(4)
        notebook.set_tab_pos(gtk.POS_TOP)

        # Add scrollview into notbook
        notebook.append_page(self.__tab_general, labelGeneral)
        notebook.append_page(self.__tab_editor, labelInterface)
        notebook.append_page(self.__tab_terminal, labelTerminal)
        notebook.append_page(self.__tab_packages, labelPackages)

        self.add_widget(notebook)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.show_all()

        self.__get_preferences_values()

        reload_interface = False

        if self.run() == gtk.RESPONSE_APPLY:
            reload_interface = self.__save_configuration()

        self.destroy()

        if self.interface is not None:
            if reload_interface:
                self.interface.reload_interface()
                self.interface.upgrade_get(True)

            self.interface.reload_check_server()
            self.interface.statusbar_update()


    def __save_configuration (self):
        """
        Save user preferences
        """

        reload_interface = False

        # ------------------------------------
        #   Update fpm-gui config
        # ------------------------------------

        for widget in self.widgets.keys():
            option, value = self.widgets[widget]

            if type(widget) == gtk.CheckButton:
                result = str(widget.get_active()).lower()

            elif type(widget) == gtk.ComboBox:
                result = str(self.colorschemes[widget.get_active()])

            elif type(widget) == gtk.SpinButton:
                result = str(widget.get_value_as_int())

            elif type(widget) == gtk.ColorButton:
                result = str(widget.get_color())

            elif type(widget) == gtk.Entry:
                result = str(widget.get_text())

            elif type(widget) == gtk.FontButton:
                result = str(widget.get_font_name())

            self.config.set(option, value, result)

        # VTE scrollbar mode
        if self.__tab_terminal.radioLeftScrolling.get_active():
            self.config.set("terminal", "scrollbar", "left")
        elif self.__tab_terminal.radioNoScrolling.get_active():
            self.config.set("terminal", "scrollbar", "none")
        else:
            self.config.set("terminal", "scrollbar", "right")

        # Packages dependencies
        self.config.set("packages", "check_dependencies",
            self.__tab_general.dependenciesModes[1][
            self.__tab_general.comboDependencies.get_active()])

        # Screenshots cache
        path = str(self.__tab_general.entryScreenshotsCache.get_text())

        # Add a / to the end of path if not exist
        if len(path) > 0 and not path[-1] == '/':
            path += '/'

        # Change cache only if this is not the same cache
        if not self.cachePath == path:

            if exists(path):

                if not access(path, W_OK):
                    LOG.error(_("Cannot write into %s folder. Set default "
                        "folder instead") % self.cachePath)
                    self.config.set("application", "cache", self.cachePath)

                else:
                    self.config.set("application", "cache", path)
                    self.interface.cachePath = path

            else:
                try:
                    LOG.info(_("Create %s folder") % screen_cache)
                    mkdir(screen_cache, 0o755)

                    self.config.set("application", "cache", path)
                    self.interface.cachePath = path

                except OSError as err:
                    LOG.error(_("Cannot write into %s folder. Set default "
                        "folder instead") % self.cachePath)
                    self.config.set("application", "cache", self.cachePath)

        else:
            self.config.set("application", "cache", self.cachePath)

        self.config.write_configuration()

        refresh()

        # ------------------------------------
        #   Update pacman-g2 config
        # ------------------------------------

        # Update pacman-g2 configuration if a modification occur
        if self.pacmang2Modification:

            for widget in self.widgets_pacman.keys():
                option, value, commented = self.widgets_pacman[widget]

                # Get checkbutton status for widget
                if type(commented) == gtk.CheckButton:
                    commented = not commented.get_active()

                # Get data from widgets
                if type(widget) == gtk.Entry:
                    result = widget.get_text()

                elif type(widget) == gtk.CheckButton:
                    result = widget.get_active()

                elif type(widget) == gtk.SpinButton:
                    result = str(widget.get_value_as_int())

                # CheckButton case
                if commented is None:
                    result = result[0]

                if self.pacman_config.option(value) is not None:
                    self.pacman_config.option(value).value = result
                    self.pacman_config.option(value).commented = commented

            # Get modified files path
            self.pacman_config.generate_configuration()

            # Send paths to FPMd and update config
            self.pacman.configuration_update(self.pacman_config.hash)

            # Reset pacman databases
            self.pacman.reload_daemon()

            # Update pacman-g2 repositories
            if self.pacmang2Update and self.interface is not None:
                self.interface.databases_update()
            else:
                reload_interface = True

        return reload_interface


    def __get_preferences_values (self):
        """
        Update preferences
        """

        self.__tab_general.get_preferences_values()
        self.__tab_editor.get_preferences_values()
        self.__tab_terminal.get_preferences_values()

        # Avoid to ask root password if there are no user modifications
        self.pacmang2Modification = False

        # ------------------------------------
        #   PyFPM
        # ------------------------------------

        for widget in self.widgets.keys():
            option, value = self.widgets[widget]

            if type(widget) == gtk.CheckButton:
                widget.set_active(self.config.get(option, value))

            elif type(widget) == gtk.Entry:
                widget.set_text(self.config.get(option, value))

            elif type(widget) == gtk.SpinButton:
                widget.set_value(self.config.get(option, value))

            elif type(widget) == gtk.ColorButton:
                widget.set_color(gtk.gdk.Color(self.config.get(option, value)))

        # ------------------------------------
        #   Pacman-g2 - General
        # ------------------------------------

        for option in self.config_pacman_general.keys():

            data = self.pacman_config.option(option)
            if data is None:
                data = self.config_pacman_general[option]["default"]
            else:
                data = data.value

            widget = self.config_pacman_general[option]["widget"]

            if type(widget) == gtk.Entry:
                widget.set_text(data)

            elif type(widget) == gtk.SpinButton:
                widget.set_value(float(data))

            elif type(widget) == gtk.CheckButton:
                widget.set_active(data)

        # ------------------------------------
        #   Pacman-g2 - Path
        # ------------------------------------

        for option in self.config_pacman_path.keys():

            data = self.pacman_config.option(option)
            if data is None:
                data = self.config_pacman_path[option][-1]

            # Check the case where data is a string
            if type(data) is not str:
                text = data.value

                commented = data.commented
                if commented is None:
                    commented = True

            else:
                text = data
                commented = True

            # Add default text in entry widget
            self.config_pacman_path[option][0].set_text(text)

            for widget in self.config_pacman_path[option][:-1]:

                if type(widget) == gtk.CheckButton:
                    widget.set_active(not commented)

                else:
                    widget.set_sensitive(not commented)

    # ------------------------------------
    #   Preferences window functions
    # ------------------------------------

    def open_file (self, widget, entry, fileMode=False):
        """
        Open a dialog to choose a folder/file
        """

        if fileMode:
            title = _("Choose a file")
            action = gtk.FILE_CHOOSER_ACTION_OPEN
        else:
            title = _("Choose a folder")
            action = gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER

        fileWindow = gtk.FileChooserDialog(title, None,
            action, (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
            gtk.STOCK_OK, gtk.RESPONSE_OK))

        fileWindow.set_local_only(True)
        fileWindow.set_select_multiple(False)

        if len(entry.get_text()) > 0:
            if fileMode:
                fileWindow.set_current_folder(dirname(entry.get_text()))
            else:
                fileWindow.set_current_folder(entry.get_text())

        response = fileWindow.run()

        if response == gtk.RESPONSE_OK:
            entry.set_text(fileWindow.get_filename())

        fileWindow.destroy()


    def entry_activate (self, widget, pos, event, function=None):
        """
        Erase entry text
        """

        if pos == 0 and function is not None:
            function()

        elif pos == 1:
            widget.set_text(str())


    def update_sensitive (self, widget, *args):
        """
        Change sensitive mode for all the associate widgets of checked option
        """

        status = bool(widget.get_active())

        for arg in args:
            arg.set_sensitive(status)

            if type(arg) == gtk.Entry and arg.get_icon_stock(1) is not None:
                arg.set_icon_sensitive(1, status)


class TabGeneral (gtk.ScrolledWindow):

    def __init__ (self, parent):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__parent = parent

        # Check dependencies modes
        self.dependenciesModes = [
            [_("Manually"), _("Automatic"), _("None")],
            ["manually", "automatic", "none"]]

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_border_width(4)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        boxGeneral = gtk.VBox()

        boxGeneralInterface = gtk.VBox()
        boxGeneralDependencies = gtk.HBox()
        boxGeneralScreenshots = gtk.VBox()

        boxGeneralScreenshotsCache = gtk.HBox()
        boxGeneralScreenshotsMaxCache = gtk.HBox()
        boxGeneralScreenshotsDriveSize = gtk.HBox()

        viewGeneral = gtk.Viewport()

        # Properties
        boxGeneralInterface.set_border_width(8)
        boxGeneralDependencies.set_border_width(8)
        boxGeneralScreenshots.set_border_width(8)

        viewGeneral.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Frame interface
        # ------------------------------------

        frameInterface = gtk.Frame()
        frameInterfaceLabel = gtk.Label()

        # Properties
        frameInterface.set_border_width(4)
        frameInterface.set_label_widget(frameInterfaceLabel)
        frameInterface.set_shadow_type(self.__parent.frame_type)

        frameInterfaceLabel.set_padding(4, -1)
        frameInterfaceLabel.set_use_markup(True)
        frameInterfaceLabel.set_markup("<b>%s</b>" % _("General"))

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.labelInterfaceExit = gtk.CheckButton()
        self.labelInterfaceUpdate = gtk.CheckButton()
        self.labelInterfaceNotify = gtk.CheckButton()

        # Properties
        self.labelInterfaceExit.set_alignment(0, 0.5)
        self.labelInterfaceExit.set_label(
            _("Save window size on exit"))

        self.labelInterfaceUpdate.set_alignment(0, 0.5)
        self.labelInterfaceUpdate.set_label(
            _("Show updatable packages windows when pyFPM start"))

        self.labelInterfaceNotify.set_alignment(0, 0.5)
        self.labelInterfaceNotify.set_label(
            _("Show notification"))

        # ------------------------------------
        #   Frame dependencies
        # ------------------------------------

        frameDependencies = gtk.Frame()
        frameDependenciesLabel = gtk.Label()

        # Properties
        frameDependencies.set_label_widget(frameDependenciesLabel)
        frameDependencies.set_shadow_type(self.__parent.frame_type)
        frameDependencies.set_border_width(4)

        frameDependenciesLabel.set_padding(4, -1)
        frameDependenciesLabel.set_use_markup(True)
        frameDependenciesLabel.set_markup(
            "<b>%s</b>" % _("Packages dependencies"))

        # ------------------------------------
        #   Packages dependencies
        # ------------------------------------

        labelDependencies = gtk.Label()

        listDependencies = gtk.ListStore(str)
        self.comboDependencies = gtk.ComboBox(listDependencies)

        cellDependencies = gtk.CellRendererText()

        # Properties
        labelDependencies.set_alignment(0, 0.5)
        labelDependencies.set_text(
            "%s:" % _("Missing dependencies dectection mode"))

        self.comboDependencies.pack_start(cellDependencies, True)
        self.comboDependencies.add_attribute(cellDependencies, "text", 0)

        # ------------------------------------
        #   Frame screenshots
        # ------------------------------------

        frameScreenshots = gtk.Frame()
        frameScreenshotsLabel = gtk.Label()

        # Properties
        frameScreenshots.set_label_widget(frameScreenshotsLabel)
        frameScreenshots.set_shadow_type(self.__parent.frame_type)
        frameScreenshots.set_border_width(4)

        frameScreenshotsLabel.set_padding(4, -1)
        frameScreenshotsLabel.set_use_markup(True)
        frameScreenshotsLabel.set_markup("<b>%s</b>" % _("Screenshots"))

        # ------------------------------------
        #   Screenshots
        # ------------------------------------

        self.labelScreenshotsDownload = gtk.CheckButton()

        labelScreenshotsCache = gtk.Label()

        self.entryScreenshotsCache = gtk.Entry()
        self.buttonScreenshotsCache = gtk.Button()
        imageScreenshotsCache = gtk.Image()

        labelScreenshotMaxCache = gtk.Label()
        adjustementScreenshotsMaxCache = gtk.Adjustment(1, 0, 256, 1, 10)
        self.spinScreenshotsMaxCache = gtk.SpinButton()
        labelScreenshotMaxCacheEnd = gtk.Label()

        self.labelScreenshotsDriveSize = gtk.Label()
        self.buttonScreenshotsDriveSize = gtk.Button()
        imageScreenshotsDriveSize = gtk.Image()

        # Properties
        self.labelScreenshotsDownload.set_alignment(0, 0.5)
        self.labelScreenshotsDownload.set_label(_("Download screenshots"))

        labelScreenshotsCache.set_alignment(0, 0.5)
        labelScreenshotsCache.set_text("%s: " % _("Screenshots cache"))

        self.entryScreenshotsCache.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        imageScreenshotsCache.set_from_stock(
            gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_SMALL_TOOLBAR)

        self.buttonScreenshotsCache.set_alignment(0, 0)
        self.buttonScreenshotsCache.set_image(imageScreenshotsCache)

        labelScreenshotMaxCache.set_alignment(0, 0.5)
        labelScreenshotMaxCache.set_text(
            "%s: " % _("Maximum size of screenshots cache"))

        self.spinScreenshotsMaxCache.set_adjustment(
            adjustementScreenshotsMaxCache)
        self.spinScreenshotsMaxCache.set_tooltip_text(
            _("If you write 0, the cache can take any space he want."))

        labelScreenshotMaxCacheEnd.set_alignment(0, 0.5)
        labelScreenshotMaxCacheEnd.set_text(_("Mb"))

        self.labelScreenshotsDriveSize.set_alignment(0, 0.5)

        imageScreenshotsDriveSize.set_from_stock(
            gtk.STOCK_CLEAR, gtk.ICON_SIZE_SMALL_TOOLBAR)

        self.buttonScreenshotsDriveSize.set_label("%s " % _("Clean cache"))
        self.buttonScreenshotsDriveSize.set_image_position(gtk.POS_RIGHT)
        self.buttonScreenshotsDriveSize.set_image(imageScreenshotsDriveSize)
        self.buttonScreenshotsDriveSize.set_tooltip_text(
            _("Clean screenshots cache"))

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        # Interface general
        boxGeneralInterface.pack_start(self.labelInterfaceNotify, False, True)
        boxGeneralInterface.pack_start(self.labelInterfaceUpdate, False, True)
        boxGeneralInterface.pack_start(self.labelInterfaceExit, False, True)

        # Interface screenshots
        boxGeneralScreenshotsCache.pack_start(labelScreenshotsCache,
            False, True, 4)
        boxGeneralScreenshotsCache.pack_start(self.entryScreenshotsCache)
        boxGeneralScreenshotsCache.pack_start(self.buttonScreenshotsCache,
            False, True)

        boxGeneralScreenshotsMaxCache.pack_start(labelScreenshotMaxCache,
            True, True, 4)
        boxGeneralScreenshotsMaxCache.pack_start(self.spinScreenshotsMaxCache,
            False, True)
        boxGeneralScreenshotsMaxCache.pack_start(labelScreenshotMaxCacheEnd,
            False, True, 4)

        boxGeneralScreenshotsDriveSize.pack_start(
            self.labelScreenshotsDriveSize, True, True, 4)
        boxGeneralScreenshotsDriveSize.pack_start(
            self.buttonScreenshotsDriveSize, False, False)

        boxGeneralScreenshots.pack_start(self.labelScreenshotsDownload,
            False, True)
        boxGeneralScreenshots.pack_start(gtk.HSeparator(),
            False, True, 4)
        boxGeneralScreenshots.pack_start(boxGeneralScreenshotsCache,
            False, True)
        boxGeneralScreenshots.pack_start(gtk.HSeparator(),
            False, True, 4)
        boxGeneralScreenshots.pack_start(boxGeneralScreenshotsMaxCache,
            False, True)
        boxGeneralScreenshots.pack_start(boxGeneralScreenshotsDriveSize,
            False, True, 2)

        # Packages dependencies
        boxGeneralDependencies.pack_start(labelDependencies)
        boxGeneralDependencies.pack_start(self.comboDependencies,
            False, True, 4)

        # Add box into frames
        frameInterface.add(boxGeneralInterface)
        frameScreenshots.add(boxGeneralScreenshots)
        frameDependencies.add(boxGeneralDependencies)

        # Add frame into main box
        boxGeneral.pack_start(frameInterface, False)
        boxGeneral.pack_start(frameScreenshots, False)
        boxGeneral.pack_start(frameDependencies, False)

        # Add main box into scrollview
        viewGeneral.add(boxGeneral)

        self.add(viewGeneral)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        # Screenshots
        self.entryScreenshotsCache.connect("icon-press",
            self.__parent.entry_activate)
        self.buttonScreenshotsCache.connect("clicked",
            self.__parent.open_file, self.entryScreenshotsCache)

        self.buttonScreenshotsDriveSize.connect('clicked', self.cache_reset)


    def __start_interface (self):
        """
        Load all interface elements
        """

        data = self.__parent.widgets

        data[self.labelInterfaceExit] = ["application", "save_window_size"]
        data[self.labelInterfaceNotify] = ["application", "show_notification"]
        data[self.labelInterfaceUpdate] = ["application", "show_update_window"]
        data[self.entryScreenshotsCache] = ["application", "cache"]
        data[self.spinScreenshotsMaxCache] = ["application", "max_cache_size"]
        data[self.labelScreenshotsDownload] = ["network", "download"]

        self.show_all()


    def get_preferences_values (self):
        """
        Update preferences
        """

        # Get screenshots cache size
        self.cache_size()

        # Get check dependencies modes
        model = self.comboDependencies.get_model()
        for mode in self.dependenciesModes[0]:
            model.append([mode])

        try:
            self.comboDependencies.set_active(self.dependenciesModes[1].index(
                self.__parent.config.get("packages", "check_dependencies")))

        except:
            self.comboDependencies.set_active(
                self.dependenciesModes[1].index("manually"))


    def cache_reset (self, widget):
        """
        Erase the cache content
        """

        cleanFolder(self.__parent.cachePath, "pkg_*")

        if self.__parent.interface is not None:
            self.__parent.interface.screenshots = list()

        self.cache_size()


    def cache_size (self):
        """
        Get actual labelCache size
        """

        self.labelScreenshotsDriveSize.set_label(
            _("Currently, the cache size is %0.1f MB") % (
            getFolderSize(self.__parent.cachePath)/(1024*1024.0)))


class TabEditor (gtk.ScrolledWindow):

    def __init__ (self, parent):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__parent = parent

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_border_width(4)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        boxInterface = gtk.VBox()
        boxInterfaceColors = gtk.VBox()
        boxInterfaceEditor = gtk.Table()

        boxInterfaceColorsInstall = gtk.HBox()
        boxInterfaceColorsPurge = gtk.HBox()
        boxInterfaceColorsUpgrade = gtk.HBox()
        boxInterfaceColorsDowngrade = gtk.HBox()

        viewInterface = gtk.Viewport()

        # Properties
        boxInterface.set_border_width(4)

        boxInterfaceColors.set_border_width(8)
        boxInterfaceEditor.set_border_width(8)
        boxInterfaceEditor.set_col_spacings(8)

        viewInterface.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Colors
        # ------------------------------------

        frameColors = gtk.Frame()
        frameColorsLabel = gtk.Label("<b>%s</b>" % _("Colors"))

        self.pickColorsInstall = gtk.ColorButton()
        self.buttonColorsResetInstall = gtk.Button()
        alignmentColorsInstall = gtk.Alignment(0.5, 0.5, 0, 0)
        imageColorsResetInstall = gtk.Image()
        labelColorsInstall = gtk.Label(
            _("Color used to mark package to install"))

        self.pickColorsPurge = gtk.ColorButton()
        self.buttonColorsResetPurge = gtk.Button()
        alignmentColorsPurge = gtk.Alignment(0.5, 0.5, 0, 0)
        imageColorsResetPurge = gtk.Image()
        labelColorsPurge = gtk.Label(
            _("Color used to mark package to remove"))

        self.pickColorsUpgrade = gtk.ColorButton()
        self.buttonColorsResetUpgrade = gtk.Button()
        alignmentColorsUpgrade = gtk.Alignment(0.5, 0.5, 0, 0)
        imageColorsResetUpgrade = gtk.Image()
        labelColorsUpgrade = gtk.Label(
            _("Color used for a package with an available upgrade"))

        self.pickColorsDowngrade = gtk.ColorButton()
        self.buttonColorsResetDowngrade = gtk.Button()
        alignmentColorsDowngrade = gtk.Alignment(0.5, 0.5, 0, 0)
        imageColorsResetDowngrade = gtk.Image()
        labelColorsDowngrade = gtk.Label(
            _("Color used for a package with a newer version"))

        self.labelColorsTreeview = gtk.CheckButton(
            _("Use bold weight in packages list"))
        self.labelColorsStatubar = gtk.CheckButton(
            _("Use colors in statusbar"))

        # Properties
        frameColors.set_label_widget(frameColorsLabel)
        frameColors.set_shadow_type(self.__parent.frame_type)
        frameColors.set_border_width(4)

        frameColorsLabel.set_padding(4, -1)
        frameColorsLabel.set_use_markup(True)

        labelColorsInstall.set_alignment(0, 0.5)
        labelColorsPurge.set_alignment(0, 0.5)
        labelColorsUpgrade.set_alignment(0, 0.5)
        labelColorsDowngrade.set_alignment(0, 0.5)

        # Install color
        self.buttonColorsResetInstall.set_image(imageColorsResetInstall)
        alignmentColorsInstall.add(self.buttonColorsResetInstall)
        imageColorsResetInstall.set_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_SMALL_TOOLBAR)
        self.buttonColorsResetInstall.set_tooltip_text(
            _("Reset to default color"))

        # Purge color
        self.buttonColorsResetPurge.set_image(imageColorsResetPurge)
        alignmentColorsPurge.add(self.buttonColorsResetPurge)
        imageColorsResetPurge.set_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_SMALL_TOOLBAR)
        self.buttonColorsResetPurge.set_tooltip_text(
            _("Reset to default color"))

        # Upgrade color
        self.buttonColorsResetUpgrade.set_image(imageColorsResetUpgrade)
        alignmentColorsUpgrade.add(self.buttonColorsResetUpgrade)
        imageColorsResetUpgrade.set_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_SMALL_TOOLBAR)
        self.buttonColorsResetUpgrade.set_tooltip_text(
            _("Reset to default color"))

        # Downgrade color
        self.buttonColorsResetDowngrade.set_image(imageColorsResetDowngrade)
        alignmentColorsDowngrade.add(self.buttonColorsResetDowngrade)
        imageColorsResetDowngrade.set_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_SMALL_TOOLBAR)
        self.buttonColorsResetDowngrade.set_tooltip_text(
            _("Reset to default color"))

        # ------------------------------------
        #   Editor
        # ------------------------------------

        frameEditor = gtk.Frame()
        frameEditorLabel = gtk.Label("<b>%s</b>" % _("Editor"))

        labelEditorColorschemes = gtk.Label(
            _("Select a colorscheme"))

        listEditorColorschemes = gtk.ListStore(str)

        cellEditorColorschemes = gtk.CellRendererText()

        self.comboEditorColorschemes = gtk.ComboBox(listEditorColorschemes)

        self.labelEditorLines = gtk.CheckButton(
            _("Show number line"))

        # Properties
        frameEditor.set_label_widget(frameEditorLabel)
        frameEditor.set_shadow_type(self.__parent.frame_type)
        frameEditor.set_border_width(4)

        frameEditorLabel.set_padding(4, -1)
        frameEditorLabel.set_use_markup(True)

        labelEditorColorschemes.set_alignment(0, 0.5)

        self.comboEditorColorschemes.pack_start(cellEditorColorschemes, True)
        self.comboEditorColorschemes.add_attribute(
            cellEditorColorschemes, "text", 0)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        # Interface colors
        boxInterfaceColorsInstall.pack_start(self.pickColorsInstall, False)
        boxInterfaceColorsInstall.pack_start(alignmentColorsInstall,
            False, True, 6)
        boxInterfaceColorsInstall.pack_start(labelColorsInstall,
            True, True)
        boxInterfaceColorsPurge.pack_start(self.pickColorsPurge, False)
        boxInterfaceColorsPurge.pack_start(alignmentColorsPurge,
            False, True, 6)
        boxInterfaceColorsPurge.pack_start(labelColorsPurge,
            True, True)
        boxInterfaceColorsUpgrade.pack_start(self.pickColorsUpgrade, False)
        boxInterfaceColorsUpgrade.pack_start(alignmentColorsUpgrade,
            False, True, 6)
        boxInterfaceColorsUpgrade.pack_start(labelColorsUpgrade,
            True, True)
        boxInterfaceColorsDowngrade.pack_start(self.pickColorsDowngrade, False)
        boxInterfaceColorsDowngrade.pack_start(alignmentColorsDowngrade,
            False, True, 6)
        boxInterfaceColorsDowngrade.pack_start(labelColorsDowngrade,
            True, True)

        boxInterfaceColors.pack_start(boxInterfaceColorsInstall,
            False, False)
        boxInterfaceColors.pack_start(boxInterfaceColorsPurge,
            False, False)
        boxInterfaceColors.pack_start(boxInterfaceColorsUpgrade,
            False, False)
        boxInterfaceColors.pack_start(boxInterfaceColorsDowngrade,
            False, False)
        boxInterfaceColors.pack_start(gtk.HSeparator(),
            False, True, 6)
        boxInterfaceColors.pack_start(self.labelColorsTreeview,
            False, True)
        boxInterfaceColors.pack_start(self.labelColorsStatubar,
            False, True)

        # Interface editor
        boxInterfaceEditor.attach(labelEditorColorschemes, 0, 1, 0, 1)
        boxInterfaceEditor.attach(self.comboEditorColorschemes, 1, 2, 0, 1)
        boxInterfaceEditor.attach(self.labelEditorLines, 0, 2, 1, 2,
            xoptions=gtk.FILL, ypadding=4)

        # Add box into frames
        frameColors.add(boxInterfaceColors)
        frameEditor.add(boxInterfaceEditor)

        # Add frame into main box
        boxInterface.pack_start(frameColors, False)
        boxInterface.pack_start(frameEditor, False)

        # Add main box into scrollview
        viewInterface.add(boxInterface)

        self.add(viewInterface)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.buttonColorsResetInstall.connect("clicked",
            self.reset_color, GREEN)
        self.buttonColorsResetPurge.connect("clicked",
            self.reset_color, RED)
        self.buttonColorsResetUpgrade.connect("clicked",
            self.reset_color, ORANGE)
        self.buttonColorsResetDowngrade.connect("clicked",
            self.reset_color, BLUE)


    def __start_interface (self):
        """
        Load all interface elements
        """

        data = self.__parent.widgets

        data[self.pickColorsPurge] = ["colors", "remove"]
        data[self.pickColorsInstall] = ["colors", "install"]
        data[self.pickColorsUpgrade] = ["colors", "upgrade"]
        data[self.pickColorsDowngrade] = ["colors", "downgrade"]
        data[self.labelColorsTreeview] = ["colors", "use_bold"]
        data[self.labelColorsStatubar] = ["colors", "use_colors_statubar"]
        data[self.comboEditorColorschemes] = ["editor", "colorscheme"]
        data[self.labelEditorLines] = ["editor", "number_line"]

        self.show_all()


    def get_preferences_values (self):
        """
        Update preferences
        """

        # Get gtk+2 coloschemes list
        model = self.comboEditorColorschemes.get_model()

        # Fill colorschemes cache
        if len(self.__parent.colorschemes) == 0:
            for data in glob("/usr/share/gtksourceview-2.0/styles/*.xml"):
                model.append([basename(data)[:-4]])
                self.__parent.colorschemes.append(basename(data)[:-4])

        # Use colorscheme cache
        else:
            for colorscheme in self.__parent.colorschemes:
                model.append([str(colorscheme)])

        self.comboEditorColorschemes.set_model(model)

        try:
            self.comboEditorColorschemes.set_active(
                self.__parent.colorschemes.index(
                self.__parent.config.get("editor", "colorscheme")))

        except:
            self.comboEditorColorschemes.set_active(
                self.__parent.colorschemes.index("classic"))


    def reset_color (self, widget, color):
        """
        Reset the current color to default
        """

        if widget == self.buttonColorsResetInstall:
            self.pickColorsInstall.set_color(gtk.gdk.Color(color))

        elif widget == self.buttonColorsResetPurge:
            self.pickColorsPurge.set_color(gtk.gdk.Color(color))

        elif widget == self.buttonColorsResetUpgrade:
            self.pickColorsUpgrade.set_color(gtk.gdk.Color(color))

        elif widget == self.buttonColorsResetDowngrade:
            self.pickColorsDowngrade.set_color(gtk.gdk.Color(color))


class TabTerminal (gtk.ScrolledWindow):

    def __init__ (self, parent):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__parent = parent

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_border_width(4)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.VBox()

        boxAppearance = gtk.VBox()
        boxFont = gtk.HBox()
        boxColors = gtk.VBox()
        boxBackground = gtk.HBox()
        boxForeground = gtk.HBox()

        boxScrolling = gtk.VBox()
        boxScrollbar = gtk.VBox()
        boxBuffer = gtk.HBox()

        view = gtk.Viewport()

        # Properties
        box.set_border_width(4)

        boxAppearance.set_border_width(8)
        boxFont.set_spacing(8)
        boxColors.set_spacing(8)
        boxBackground.set_spacing(8)
        boxForeground.set_spacing(8)

        boxScrolling.set_border_width(8)
        boxScrollbar.set_spacing(8)
        boxBuffer.set_spacing(8)

        view.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        notebook = gtk.Notebook()

        labelAppearance = gtk.Label()
        labelScrolling = gtk.Label()

        # Properties
        notebook.set_border_width(4)
        notebook.set_tab_pos(gtk.POS_TOP)

        labelAppearance.set_text(_("Appearance"))
        labelScrolling.set_text(_("Scrolling"))

        # ------------------------------------
        #   Appearance
        # ------------------------------------

        labelFont = gtk.Label()
        self.buttonFont = gtk.FontButton()

        labelColors = gtk.Label()

        self.pickBackground = gtk.ColorButton()
        labelBackground = gtk.Label()

        self.pickForeground = gtk.ColorButton()
        labelForeground = gtk.Label()

        # Properties
        labelFont.set_alignment(0, 0.5)
        labelFont.set_text(_("Terminal font"))

        self.buttonFont.set_size_request(400, -1)

        labelColors.set_alignment(0, 0.5)
        labelColors.set_text(_("Terminal colors"))

        labelBackground.set_alignment(0, 0.5)
        labelBackground.set_text(_("Background color"))

        labelForeground.set_alignment(0, 0.5)
        labelForeground.set_text(_("Text color"))

        # ------------------------------------
        #   Scrolling
        # ------------------------------------

        labelScrollbar = gtk.Label()

        self.radioLeftScrolling = gtk.RadioButton()
        self.radioRightScrolling = gtk.RadioButton()
        self.radioNoScrolling = gtk.RadioButton()

        labelBuffer = gtk.Label()
        self.spinBuffer = gtk.SpinButton()
        labelBufferEnd = gtk.Label()

        # Properties
        labelScrollbar.set_alignment(0, 0.5)
        labelScrollbar.set_text(_("Scrollbar mode"))

        self.radioLeftScrolling.set_label(_("Left scrollbar"))

        self.radioRightScrolling.set_group(self.radioLeftScrolling)
        self.radioRightScrolling.set_label(_("Right scrollbar"))

        self.radioNoScrolling.set_group(self.radioLeftScrolling)
        self.radioNoScrolling.set_label(_("No scrollbar"))

        labelBuffer.set_alignment(0, 0.5)
        labelBuffer.set_text(_("Scrollbar buffer size"))

        self.spinBuffer.set_range(-1, 5000)

        labelBufferEnd.set_alignment(0, 0.5)
        labelBufferEnd.set_text(_("lines"))

        # ------------------------------------
        #   VTE
        # ------------------------------------

        framePreview = gtk.Frame()
        labelPreview = gtk.Label()

        self.boxPreview = gtk.HBox()
        self.scrollPreview = gtk.VScrollbar()
        self.vtePreview = Terminal()

        # Properties
        framePreview.set_label_widget(labelPreview)
        framePreview.set_shadow_type(self.__parent.frame_type)
        framePreview.set_border_width(4)

        labelPreview.set_padding(4, -1)
        labelPreview.set_use_markup(True)
        labelPreview.set_markup("<b>%s</b>" % _("Preview"))

        self.boxPreview.set_border_width(8)

        self.scrollPreview.set_adjustment(self.vtePreview.get_adjustment())

        self.vtePreview.set_size_request(400, 120)

        self.vtePreview.feed("The quick brown fox jumps over the lazy dog")

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        notebook.append_page(boxAppearance, labelAppearance)
        notebook.append_page(boxScrolling, labelScrolling)

        # Appearance
        boxFont.pack_start(labelFont, True)
        boxFont.pack_start(self.buttonFont, False)
        boxBackground.pack_start(self.pickBackground, False)
        boxBackground.pack_start(labelBackground, True)
        boxForeground.pack_start(self.pickForeground, False)
        boxForeground.pack_start(labelForeground, True)

        boxColors.pack_start(labelColors, True)
        boxColors.pack_start(boxBackground, True)
        boxColors.pack_start(boxForeground, True)

        boxAppearance.pack_start(boxFont, False)
        boxAppearance.pack_start(gtk.HSeparator(), False, False, 8)
        boxAppearance.pack_start(boxColors, False)

        # Scrolling
        boxScrollbar.pack_start(labelScrollbar, True)
        boxScrollbar.pack_start(self.radioLeftScrolling, True)
        boxScrollbar.pack_start(self.radioRightScrolling, True)
        boxScrollbar.pack_start(self.radioNoScrolling, True)

        boxBuffer.pack_start(labelBuffer, True)
        boxBuffer.pack_start(self.spinBuffer, False)
        boxBuffer.pack_start(labelBufferEnd, False)

        boxScrolling.pack_start(boxScrollbar, False)
        boxScrolling.pack_start(gtk.HSeparator(), False, False, 8)
        boxScrolling.pack_start(boxBuffer, False)

        # Preview vte
        self.boxPreview.pack_start(self.vtePreview)
        self.boxPreview.pack_start(self.scrollPreview, False)

        framePreview.add(self.boxPreview)

        # Add box into main box
        box.pack_start(notebook, True)
        box.pack_start(framePreview, False)

        # Add main box into scrollview
        view.add(box)

        self.add(view)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.buttonFont.connect("font-set", self.update_font)

        self.pickBackground.connect("color-set", self.update_colors)
        self.pickForeground.connect("color-set", self.update_colors)

        self.radioLeftScrolling.connect("toggled", self.update_scrolling)
        self.radioRightScrolling.connect("toggled", self.update_scrolling)
        self.radioNoScrolling.connect("toggled", self.update_scrolling)


    def __start_interface (self):
        """
        Load all interface elements
        """

        data = self.__parent.widgets

        data[self.buttonFont] = ["terminal", "font"]
        data[self.pickForeground] = ["terminal", "foreground"]
        data[self.pickBackground] = ["terminal", "background"]
        data[self.spinBuffer] = ["terminal", "buffer"]

        self.scrollPreview.set_no_show_all(True)

        self.show_all()


    def get_preferences_values (self):
        """
        Update preferences
        """

        self.buttonFont.set_font_name(
            self.__parent.config.get("terminal", "font"))

        self.vtePreview.set_font_from_string(self.buttonFont.get_font_name())

        self.pickForeground.set_color(gtk.gdk.Color(
            self.__parent.config.get("terminal", "foreground")))
        self.pickBackground.set_color(gtk.gdk.Color(
            self.__parent.config.get("terminal", "background")))

        self.vtePreview.set_colors(
            self.pickForeground.get_color(),
            self.pickBackground.get_color(),
            [gtk.gdk.color_parse("white")] * 16)

        mode = self.__parent.config.get("terminal", "scrollbar")

        if mode == "left":
            self.radioLeftScrolling.set_active(True)

        elif mode == "none":
            self.radioNoScrolling.set_active(True)

        else:
            self.radioRightScrolling.set_active(True)

        self.spinBuffer.set_value(
            self.__parent.config.get("terminal", "buffer"))


    def update_font (self, widget):
        """
        Update VTE font
        """

        self.vtePreview.set_font_from_string(self.buttonFont.get_font_name())


    def update_colors (self, widget):
        """
        Update VTE colors
        """

        self.vtePreview.set_colors(
            self.pickForeground.get_color(),
            self.pickBackground.get_color(),
            [gtk.gdk.color_parse("white")] * 16)


    def update_scrolling (self, widget):
        """
        Update VTE scrollbar position
        """

        if widget == self.radioLeftScrolling and \
            self.radioLeftScrolling.get_active():
            self.boxPreview.reorder_child(self.scrollPreview, 0)
            self.scrollPreview.show()

        elif widget == self.radioNoScrolling and \
            self.radioNoScrolling.get_active():
            self.scrollPreview.hide()

        elif widget == self.radioRightScrolling and \
            self.radioRightScrolling.get_active():
            self.boxPreview.reorder_child(self.scrollPreview, 1)
            self.scrollPreview.show()


class TabPackages (gtk.ScrolledWindow):

    def __init__ (self, parent):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__parent = parent

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_border_width(4)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        boxConfiguration = gtk.VBox()
        boxSync = gtk.Table()
        boxSystem = gtk.VBox()
        boxUpgrade = gtk.HBox()
        boxNetwork = gtk.HBox()
        boxDownload = gtk.HBox()

        boxFiles = gtk.VBox()
        boxException = gtk.Table()
        boxPaths = gtk.Table()

        view = gtk.Viewport()

        # Properties
        boxConfiguration.set_border_width(4)
        boxSync.set_border_width(8)
        boxSync.set_col_spacings(4)
        boxSync.set_row_spacings(4)
        boxSystem.set_border_width(8)
        boxSystem.set_spacing(8)
        boxUpgrade.set_spacing(8)
        boxNetwork.set_spacing(8)
        boxDownload.set_spacing(8)

        boxFiles.set_border_width(4)
        boxException.set_border_width(8)
        boxException.set_col_spacings(4)
        boxException.set_row_spacings(4)
        boxPaths.set_border_width(8)
        boxPaths.set_col_spacings(4)
        boxPaths.set_row_spacings(4)

        view.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        notebook = gtk.Notebook()

        labelConfiguration = gtk.Label()
        labelFiles = gtk.Label()

        # Properties
        notebook.set_border_width(4)
        notebook.set_tab_pos(gtk.POS_TOP)

        labelConfiguration.set_text(_("Configuration"))
        labelFiles.set_text(_("Files"))

        # ------------------------------------
        #   Configuration - Sync
        # ------------------------------------

        frameSync = gtk.Frame()
        labelSync = gtk.Label()

        labelConfirm = gtk.Label()
        self.entryConfirm = gtk.Entry()

        labelIgnore = gtk.Label()
        self.entryIgnore = gtk.Entry()

        # Properties
        frameSync.set_border_width(4)
        frameSync.set_label_widget(labelSync)
        frameSync.set_shadow_type(self.__parent.frame_type)

        labelSync.set_padding(4, -1)
        labelSync.set_use_markup(True)
        labelSync.set_markup("<b>%s</b>" % _("Synchronize"))

        labelConfirm.set_alignment(0, 0.5)
        labelConfirm.set_text("%s: " % _("Ask a confirmation for"))

        self.entryConfirm.set_size_request(400, -1)
        self.entryConfirm.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryConfirm.set_tooltip_text(_("If a user tries to remove a "
            "package that's listed in HoldPkg, pacman-g2 will ask for "
            "confirmation before proceeding."))

        labelIgnore.set_alignment(0, 0.5)
        labelIgnore.set_text("%s: " % _("Ignore any upgrades for"))

        self.entryIgnore.set_size_request(400, -1)
        self.entryIgnore.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryIgnore.set_tooltip_text(_("Instructs pacman-g2 to ignore "
            "any upgrades for this package when performing a sysupgrade."))

        # ------------------------------------
        #   Configuration - System
        # ------------------------------------

        frameSystem = gtk.Frame()
        labelSystem = gtk.Label()

        labelUpgrade = gtk.Label()
        self.spinUpgrade = gtk.SpinButton()
        adjustmentUpgrade = gtk.Adjustment(1, 0, 50, 1, 5)

        labelDownload = gtk.Label()
        self.spinDownload = gtk.SpinButton()
        adjustmentDownload = gtk.Adjustment(1, 0, 100, 1, 5)

        self.checkProxy = gtk.CheckButton()
        self.entryProxy = gtk.Entry()
        self.labelProxy = gtk.Label()
        self.spinProxy = gtk.SpinButton()
        adjustmentProxy = gtk.Adjustment(1, 0, 65535, 1, 5)

        # Properties
        frameSystem.set_border_width(4)
        frameSystem.set_label_widget(labelSystem)
        frameSystem.set_shadow_type(self.__parent.frame_type)

        labelSystem.set_padding(4, -1)
        labelSystem.set_use_markup(True)
        labelSystem.set_markup("<b>%s</b>" % _("System"))

        labelUpgrade.set_alignment(0, 0.5)
        labelUpgrade.set_text("%s: " % _("Upgrade only the packages that are "
            "at least"))
        self.spinUpgrade.set_adjustment(adjustmentUpgrade)
        self.spinUpgrade.set_tooltip_text(_("Upgrade only the packages that "
            "are at least <x> days old when performing a sysupgrade. Write 0 "
            "to disable."))

        labelDownload.set_alignment(0, 0.5)
        labelDownload.set_text("%s: " % _("Try to download packages"))
        self.spinDownload.set_adjustment(adjustmentDownload)
        self.spinDownload.set_tooltip_text(_("Try to download packages <x> "
            "times. This is useful in case you have a bad internet connection "
            "and your packages often get corrupted during the download."))

        self.checkProxy.set_alignment(0, 0.5)
        self.checkProxy.set_label(_("Proxy"))
        self.entryProxy.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryProxy.set_tooltip_text(_("If set, pacman-g2 will use this "
            "proxy server for all ftp/http transfers."))
        self.labelProxy.set_label(':')
        self.spinProxy.set_adjustment(adjustmentProxy)
        self.spinProxy.set_tooltip_text(_("If set, pacman-g2 will use this "
            "proxy port for all ftp/http transfers. (Default: 80)."))

        # ------------------------------------
        #   Files - Exceptions
        # ------------------------------------

        frameException = gtk.Frame()
        labelException = gtk.Label()

        labelNoUpgrade = gtk.Label()
        self.entryNoUpgrade = gtk.Entry()

        labelNoExtract = gtk.Label()
        self.entryNoExtract = gtk.Entry()

        # Properties
        frameException.set_border_width(4)
        frameException.set_label_widget(labelException)
        frameException.set_shadow_type(self.__parent.frame_type)

        labelException.set_padding(4, -1)
        labelException.set_use_markup(True)
        labelException.set_markup("<b>%s</b>" % _("Files"))

        labelNoUpgrade.set_alignment(0, 0.5)
        labelNoUpgrade.set_text("%s: " % _("Never upgrade this file"))
        self.entryNoUpgrade.set_size_request(400, -1)
        self.entryNoUpgrade.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryNoUpgrade.set_tooltip_text(_("All files listed with a "
            "NoUpgrade directive will never be touched during a package "
            "install/upgrade."))

        labelNoExtract.set_alignment(0, 0.5)
        labelNoExtract.set_text("%s: " % _("Never extract this file"))
        self.entryNoExtract.set_size_request(400, -1)
        self.entryNoExtract.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryNoExtract.set_tooltip_text(_("All files listed with a "
            "NoExtract directive will never be extracted from a package into "
            "the filesystem. This can be useful when you don't want part of "
            "a package to be installed. For example, if your httpd root uses "
            "an index.php, then you would not want the index.html file to be "
            "extracted from the apache package."))

        # ------------------------------------
        #   Files - Paths
        # ------------------------------------

        framePaths = gtk.Frame()
        labelPaths = gtk.Label()

        self.labelDatabase = gtk.CheckButton()
        self.entryDatabase = gtk.Entry()
        self.buttonDatabase = gtk.Button()
        imageDatabase = gtk.Image()

        self.labelCache = gtk.CheckButton()
        self.entryCache = gtk.Entry()
        self.buttonCache = gtk.Button()
        imageCache = gtk.Image()

        self.labelLog = gtk.CheckButton()
        self.entryLog = gtk.Entry()
        self.buttonLog = gtk.Button()
        imageLog = gtk.Image()

        # Properties
        framePaths.set_border_width(4)
        framePaths.set_label_widget(labelPaths)
        framePaths.set_shadow_type(self.__parent.frame_type)

        labelPaths.set_padding(4, -1)
        labelPaths.set_use_markup(True)
        labelPaths.set_markup("<b>%s</b>" % _("Path"))

        self.labelDatabase.set_alignment(0, 0.5)
        self.labelDatabase.set_label("%s: " % _("Database"))
        self.entryDatabase.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryDatabase.set_tooltip_text(_("Overrides the default location "
            "of the toplevel database directory. The default is "
            "/var/lib/pacman-g2."))
        self.buttonDatabase.set_image(imageDatabase)
        imageDatabase.set_from_stock(
            gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_SMALL_TOOLBAR)

        self.labelCache.set_alignment(0, 0.5)
        self.labelCache.set_label("%s: " % _("Cache"))
        self.entryCache.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryCache.set_tooltip_text(_("Overrides the default location of "
            "the package cache directory. The default is "
            "/var/cache/pacman-g2."))
        self.buttonCache.set_image(imageCache)
        imageCache.set_from_stock(
            gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_SMALL_TOOLBAR)

        self.labelLog.set_alignment(0, 0.5)
        self.labelLog.set_label("%s: " % _("Log"))
        self.entryLog.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entryLog.set_tooltip_text(_("Log actions directly to a file, "
            "usually /var/log/pacman-g2.log."))
        self.buttonLog.set_image(imageLog)
        imageLog.set_from_stock(
            gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_SMALL_TOOLBAR)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        notebook.append_page(boxConfiguration, labelConfiguration)
        notebook.append_page(boxFiles, labelFiles)

        # Configuration
        boxSync.attach(labelConfirm, 0, 1, 0, 1, xoptions=gtk.FILL)
        boxSync.attach(self.entryConfirm, 1, 2, 0, 1)
        boxSync.attach(labelIgnore, 0, 1, 1, 2, xoptions=gtk.FILL)
        boxSync.attach(self.entryIgnore, 1, 2, 1, 2)

        boxUpgrade.pack_start(labelUpgrade, True)
        boxUpgrade.pack_start(self.spinUpgrade, False)

        boxDownload.pack_start(labelDownload, True)
        boxDownload.pack_start(self.spinDownload, False)

        boxNetwork.pack_start(self.checkProxy, False)
        boxNetwork.pack_start(self.entryProxy, True)
        boxNetwork.pack_start(self.labelProxy, False)
        boxNetwork.pack_start(self.spinProxy, False)

        boxSystem.pack_start(boxUpgrade)
        boxSystem.pack_start(gtk.HSeparator())
        boxSystem.pack_start(boxDownload)
        boxSystem.pack_start(boxNetwork)

        frameSync.add(boxSync)
        frameSystem.add(boxSystem)

        boxConfiguration.pack_start(frameSync, False)
        boxConfiguration.pack_start(frameSystem, False)

        # File system
        boxException.attach(labelNoUpgrade, 0, 1, 0, 1, xoptions=gtk.FILL)
        boxException.attach(self.entryNoUpgrade, 1, 2, 0, 1)
        boxException.attach(labelNoExtract, 0, 1, 1, 2, xoptions=gtk.FILL)
        boxException.attach(self.entryNoExtract, 1, 2, 1, 2)

        boxPaths.attach(self.labelDatabase, 0, 1, 0, 1, xoptions=gtk.FILL)
        boxPaths.attach(self.entryDatabase, 1, 2, 0, 1)
        boxPaths.attach(self.buttonDatabase, 2, 3, 0, 1, xoptions=gtk.FILL)
        boxPaths.attach(self.labelCache, 0, 1, 1, 2, xoptions=gtk.FILL)
        boxPaths.attach(self.entryCache, 1, 2, 1, 2)
        boxPaths.attach(self.buttonCache, 2, 3, 1, 2, xoptions=gtk.FILL)
        boxPaths.attach(self.labelLog, 0, 1, 2, 3, xoptions=gtk.FILL)
        boxPaths.attach(self.entryLog, 1, 2, 2, 3)
        boxPaths.attach(self.buttonLog, 2, 3, 2, 3, xoptions=gtk.FILL)

        frameException.add(boxException)
        framePaths.add(boxPaths)

        boxFiles.pack_start(frameException, False)
        boxFiles.pack_start(framePaths, False)

        # Add main box into scrollview
        view.add(notebook)

        self.add(view)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        # Libpacman general
        self.entryConfirm.connect("icon-press", self.__parent.entry_activate)
        self.entryIgnore.connect("icon-press", self.__parent.entry_activate)

        self.entryConfirm.connect("focus-out-event", self.check_changes,
            self.__parent.pacman_config.option("HoldPkg"))
        self.entryIgnore.connect("focus-out-event", self.check_changes,
            self.__parent.pacman_config.option("IgnorePkg"))

        # Libpacman files
        self.entryNoUpgrade.connect("icon-press", self.__parent.entry_activate)
        self.entryNoExtract.connect("icon-press", self.__parent.entry_activate)

        self.entryNoUpgrade.connect("focus-out-event", self.check_changes,
            self.__parent.pacman_config.option("NoUpgrade"))
        self.entryNoExtract.connect("focus-out-event", self.check_changes,
            self.__parent.pacman_config.option("NoExtract"))

        # Libpacman sync
        self.spinUpgrade.connect("value-changed", self.check_changes,
            self.__parent.pacman_config.option("UpgradeDelay"))
        self.spinDownload.connect("value-changed", self.check_changes,
            self.__parent.pacman_config.option("MaxTries"))

        # Libpacman network
        self.checkProxy.connect("toggled", self.__parent.update_sensitive,
            self.entryProxy, self.labelProxy, self.spinProxy)
        self.entryProxy.connect("icon-press", self.__parent.entry_activate)

        self.checkProxy.connect("toggled", self.check_changes,
            self.__parent.pacman_config.option("ProxyServer"))
        self.spinProxy.connect("value-changed", self.check_changes,
            self.__parent.pacman_config.option("ProxyPort"))

        # Libpacman paths
        self.labelDatabase.connect("toggled", self.__parent.update_sensitive,
            self.entryDatabase, self.buttonDatabase)
        self.labelCache.connect("toggled", self.__parent.update_sensitive,
            self.entryCache, self.buttonCache)
        self.labelLog.connect("toggled", self.__parent.update_sensitive,
            self.entryLog, self.buttonLog)

        self.entryDatabase.connect("icon-press", self.__parent.entry_activate)
        self.entryCache.connect("icon-press", self.__parent.entry_activate)
        self.entryLog.connect("icon-press", self.__parent.entry_activate)

        self.buttonDatabase.connect("clicked", self.__parent.open_file,
            self.entryDatabase)
        self.buttonCache.connect("clicked", self.__parent.open_file,
            self.entryCache)
        self.buttonLog.connect("clicked", self.__parent.open_file,
            self.entryLog, True)

        if self.__parent.pacman_config.option("DBPath") is not None:
            self.entryDatabase.connect("focus-out-event", self.check_changes,
                self.__parent.pacman_config.option("DBPath").value)
        else:
            self.entryDatabase.connect("focus-out-event", self.check_changes,
                DBPATH_FILE)

        if self.__parent.pacman_config.option("CacheDir") is not None:
            self.entryCache.connect("focus-out-event", self.check_changes,
                self.__parent.pacman_config.option("CacheDir").value)
        else:
            self.entryCache.connect("focus-out-event", self.check_changes,
                CACHEDIR_FILE)

        if self.__parent.pacman_config.option("LogFile") is not None:
            self.entryLog.connect("focus-out-event", self.check_changes,
                self.__parent.pacman_config.option("LogFile").value)
        else:
            self.entryLog.connect("focus-out-event", self.check_changes,
                LOGPATH_FILE)


    def __start_interface (self):
        """
        Load all interface elements
        """

        data = self.__parent.widgets_pacman

        data[self.entryConfirm] = \
            ["options", "HoldPkg", False]
        data[self.entryIgnore] = \
            ["options", "IgnorePkg", False]
        data[self.entryNoUpgrade] = \
            ["options", "NoUpgrade", False]
        data[self.entryNoExtract] = \
            ["options", "NoExtract", False]
        data[self.spinUpgrade] = \
            ["options", "UpgradeDelay", False]
        data[self.spinDownload] = \
            ["options", "MaxTries", False]
        data[self.entryProxy] = \
            ["options", "ProxyServer", self.checkProxy]
        data[self.spinProxy] = \
            ["options", "ProxyPort", self.checkProxy]
        data[self.entryDatabase] = \
            ["options", "DBPath", self.labelDatabase]
        data[self.entryCache] = \
            ["options", "CacheDir", self.labelCache]
        data[self.entryLog] = \
            ["options", "LogFile", self.labelLog]

        self.__parent.config_pacman_general = {
            "HoldPkg": dict(
                widget=self.entryConfirm,
                default="pacman-g2 glibc bash coreutils chkconfig"),
            "IgnorePkg": dict(
                widget=self.entryIgnore, default=str()),
            "NoUpgrade": dict(
                widget=self.entryNoUpgrade, default=str()),
            "NoExtract": dict(
                widget=self.entryNoExtract, default=str()),
            "UpgradeDelay": dict(
                widget=self.spinUpgrade, default=0),
            "MaxTries": dict(
                widget=self.spinDownload, default=1),
            "ProxyPort": dict(
                widget=self.spinProxy, default=80),
            }

        self.__parent.config_pacman_path = {
            "ProxyServer": [self.entryProxy, self.labelProxy,
                self.spinProxy, ""],
            "DBPath": [self.entryDatabase, self.labelDatabase,
                self.buttonDatabase, "/var/lib/pacman-g2"],
            "CacheDir": [self.entryCache, self.labelCache,
                self.buttonCache, "/var/cache/pacman-g2"],
            "LogFile": [self.entryLog, self.labelLog,
                self.buttonLog, "/var/log/pacman-g2.log"]
            }

        self.show_all()


    def check_changes (self, widget, *args):
        """
        Detect if a pacman-g2 option have been modified
        """

        # args = option
        if len(args) == 1:
            option = args[0]

        # args = event, option
        else:
            option = args[1]

        if option is not None:

            # option = [value, status]
            if type(option) == list:
                option = option.value[0]

            # Entry widget
            if type(widget) == gtk.Entry:
                if not widget.get_text() == option.value:
                    self.__parent.pacmang2Modification = True

            # SpinButton
            elif type(widget) == gtk.SpinButton:
                if not widget.get_value_as_int() == int(option.value):
                    self.__parent.pacmang2Modification = True

            # CheckButton
            elif type(widget) == gtk.CheckButton:
                if widget.get_active() == bool(option.commented):
                    self.__parent.pacmang2Modification = True

        # Option which isn't in pacman-g2 conf
        else:
            self.__parent.pacmang2Modification = True

