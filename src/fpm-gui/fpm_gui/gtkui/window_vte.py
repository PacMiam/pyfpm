# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os import kill as os_kill
from signal import SIGINT

# Interface
import gtk

from vte import Terminal
from window import Window
from modules import getIconFromTheme

# Log
from logging import getLogger

# Pyfpm
from fpm_gui import CONFIGFILE
from fpm_gui import ICON_REPOMAN
from fpm_gui import defaultValues

from fpmd.tools.utils import getTranslation
from fpmd.tools.config import Config

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowVte (Window):
    """
    Show a terminal window
    """

    def __init__ (self, parent, title, description, command, root=False):
        """
        Constructor
        """

        Window.__init__(self, parent, title)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.end = False
        self.error = False

        self.root = root
        self.command = command

        # ------------------------------------
        #   Interface
        # ------------------------------------

        if parent is not None:
            self.config = parent.config

        else:
            self.config = Config(CONFIGFILE)
            self.config.update_configuration(defaultValues)

        self.set_main_icon(gtk.STOCK_EXECUTE, gtk.ICON_SIZE_MENU)

        self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        self.set_sensitive(gtk.RESPONSE_CLOSE, False)
        self.set_default_response(gtk.RESPONSE_CLOSE)

        # ------------------------------------
        #   VTE
        # ------------------------------------

        boxVte = gtk.HBox()

        self.vte = Terminal()
        scrollVte = gtk.VScrollbar()

        # Properties
        boxVte.set_border_width(8)

        self.vte.set_allow_bold(True)
        self.vte.set_font_from_string(self.config.get("terminal", "font"))
        self.vte.set_colors(
            gtk.gdk.Color(self.config.get("terminal", "foreground")),
            gtk.gdk.Color(self.config.get("terminal", "background")),
            [gtk.gdk.color_parse("white")] * 16)

        scrollVte.set_no_show_all(True)
        scrollVte.set_adjustment(self.vte.get_adjustment())

        # ------------------------------------
        #   Actions
        # ------------------------------------

        self.vte.connect("key-press-event", self.__key_pressed)
        self.vte.connect("child-exited", self.__close_command)

        # ------------------------------------
        #   Add widgets into main interface
        # ------------------------------------

        boxVte.pack_start(self.vte)
        boxVte.pack_start(scrollVte, False)

        self.add_widget(boxVte)

        self.__run_command(self.command)

        self.show_all()

        if self.config.get("terminal", "font") == "none":
            scrollVte.hide()

        elif self.config.get("terminal", "font") == "left":
            boxVte.reorder_child(scrollVte, 0)
            scrollVte.show()

        else:
            scrollVte.show()

        self.run()
        self.destroy()

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    def getError (self):
        return self.error

    @property
    def success (self):
        return not self.error

    # ------------------------------------
    #   Interface functions
    # ------------------------------------

    def __close_command (self, data=None):
        """
        Show the close button when program exit
        """

        self.end = True

        if not self.vte.get_child_exit_status() == 0:
            self.error = True

            self.vte.feed("\n%s" % _("An error occur during transaction"))

        else:
            self.vte.feed("\n%s" % _("Transaction is done"))

        self.set_sensitive(gtk.RESPONSE_CLOSE, True)


    def __key_pressed (self, widget, event):
        """
        Check pressed key into window
        """

        if not self.end:

            # Control + C
            if event.state & (gtk.gdk.SHIFT_MASK | gtk.gdk.CONTROL_MASK) and \
                gtk.gdk.keyval_name(event.keyval) == 'c':

                # The vte is hungry, we feed him
                self.vte.feed("%s" % _("You have stopped the processus"))

                # Send ctrl+c signal to current process
                os_kill(self.proc, SIGINT)

                self.error = True

                self.set_sensitive(gtk.RESPONSE_CLOSE, True)


    def __run_command (self, cmd):
        """
        Run a command into terminal
        """

        # Check if the command need root access
        if self.root:
            cmd = "gksu %s" % cmd

        # Split command to get an array
        cmd = cmd.split()
        # And get command from first element
        command = cmd[0]

        # Set arguments with splited command
        arguments = None
        if len(cmd) > 1:
            arguments = cmd[0:]

        self.proc = self.vte.fork_command(command, arguments)

