# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Interface
from gtk import events_pending
from gtk import main_iteration
from gtk import ICON_LOOKUP_FORCE_SVG
from gtk import icon_theme_get_default

from gtk import ListStore

from gtk.gdk import Pixbuf

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def refresh ():
    """
    Refresh the interface
    """

    while events_pending():
        main_iteration()


def getIconFromTheme (name, size=16, fallback="image-missing", theme=None):
    """
    Get icon pixbuf from default gtk icons theme
    """

    # Get default user icons theme
    if theme == None:
        theme = icon_theme_get_default()

    # Check if specific icon name is in icons theme
    if theme.has_icon(name):
        try:
            return theme.load_icon(name, size, ICON_LOOKUP_FORCE_SVG)

        except:
            if type(fallback) == Pixbuf:
                return fallback

            return theme.load_icon(fallback, size, ICON_LOOKUP_FORCE_SVG)

    # Return fallback icon (in the case where is a Pixbuf)
    if type(fallback) == Pixbuf:
        return fallback

    # Find fallback icon in icons theme
    if theme.has_icon(fallback):
        return theme.load_icon(fallback, size, ICON_LOOKUP_FORCE_SVG)

    # Instead, return default image
    return theme.load_icon("image-missing", size, ICON_LOOKUP_FORCE_SVG)


def createScript (path, action):
    """
    Create a script sh with specific action
    """

    filePath = "%s/installer.sh" % path

    with open(filePath, 'w') as pipe:
        pipe.write(str(action))

    return filePath


def update_widget_size (widget, allocation):
    """
    Update label size
    """

    widget.set_size_request(allocation.width -1, -1)


class ExtendedListStore (ListStore):

    def __init__ (self, *args):
        """
        Constructor
        """

        ListStore.__init__(self, *args)


    def iter_previous (self, treeiter):
        """
        Get previous iter in list store
        """

        path = self.get_path(treeiter)

        if not path[0] == 0:
            previousPath = list(path)[:-1]
            previousPath.append(path[-1] - 1)

            return self.get_iter(tuple(previousPath))

        return None
