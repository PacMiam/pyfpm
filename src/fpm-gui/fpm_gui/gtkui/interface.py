# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import exit as sys_exit
from stat import S_IMODE
from stat import ST_MODE
from copy import deepcopy
from gobject import idle_add
from gobject import timeout_add
from gobject import threads_init
from gobject import source_remove
from datetime import datetime
from subprocess import call

# Files
from os import stat
from os import W_OK
from os import remove
from os.path import exists
from os.path import expanduser

# Interface
import gtk

from mimetypes import guess_type
from mimetypes import init as mimetypes_init

from pango import Weight
from pango import WRAP_WORD
from pango import WEIGHT_BOLD
from pango import WEIGHT_NORMAL

from notify2 import Notification
from notify2 import init as notifyInit

from modules import *

from sourceview import Sourceview

from transaction import Transaction

from window import *
from window_vte import *
from window_logs import *
from window_help import *
from window_sync import *
from window_preferences import *
from window_repositories import *

# Log
from logging import getLogger

# Modules
from fpm_gui import *
from fpm_gui import __version__

from fpmd import DBPATH_FILE
from fpmd import REPOMAN_BIN
from fpmd import FW32_UPGRADE_BIN

from fpmd.tools.utils import *
from fpmd.tools.network import Network
from fpmd.tools.package import Command
from fpmd.tools.package import Database
from fpmd.tools.fbparser import Parser
from fpmd.tools.pacmanconf import PacmanConfig

# Regex
from re import match

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Interface (gtk.Window):

    def __init__ (self, configuration, debug):
        """
        Init main window
        """

        gtk.Window.__init__(self)

        threads_init()

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.debug = debug

        # Switch
        self.__fw32 = False
        self.repoman = False
        self.__website_status = False
        self.__website_checked = False
        self.__search_status = False
        self.__not_check_dependencies = True

        # Arrays
        self.colorschemes = list()
        self.screenshots = list()

        # Dictionnaries
        self.treeview_data = dict()
        self.__mimetypes = dict()
        self.__sub_groups = dict()
        self.__model_saved = dict()
        self.packages_data_widgets = dict()

        self.packages_data = dict(
            installation=list(),
            reinstallation=list(),
            remove=list(),
            upgrade=dict(),
            save=dict() )

        self.selected = dict(
            package_name=str(),
            package_version=str(),
            package_group=str(),
            repository=str(),
            group=str(),
            frugalbuild=str(),
            search=str(),
            menu=str() )

        self.__groups_data = {
            _("Applications"): GRP_APPLICATIONS,
            _("Development"): GRP_DEVELOPMENT,
            _("Documentation"): GRP_DOCUMENTATION,
            _("Enlightenment desktop"): GRP_ENLIGHTENMENT,
            _("Fonts"): GRP_FONTS,
            _("Games"): GRP_GAMES,
            _("GNOME desktop"): GRP_GNOME,
            _("KDE desktop"): GRP_KDE,
            _("Libraries"): GRP_LIBRARY,
            _("Locales"): GRP_LOCALES,
            _("LXDE desktop"): GRP_LXDE,
            _("Mate desktop"): GRP_MATE,
            _("Multimedia"): GRP_MULTIMEDIA,
            _("Network"): GRP_NETWORK,
            _("System"): GRP_SYSTEM,
            _("XFCE desktop"): GRP_XFCE,
            _("X server"): GRP_XSERVER }

        # String
        self.__label_notebook = str()

        # Value
        self.__length_packages = 0
        self.__thread_packages = 0
        self.__thread_thumbnail = 0
        self.__thread_screenshot = 0

        # Widgets
        self.__tooltip_packages = None
        self.__tooltip_files = None

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        # Create a configuration instance
        self.config = configuration

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Icons
        # ------------------------------------

        # Get user icon theme
        self.icons_theme = gtk.icon_theme_get_default()

        # Get mimeypes for files viewer
        mimetypes_init()

        # Packages icons
        self.icon_available = self.icon_load(ICON_AVAILABLE, 24)
        self.icon_installed = self.icon_load(ICON_AVAILABLE, 24)
        self.icon_to_install = self.icon_load(
            ICON_TOINSTALL, 24, ICON_TOINSTALL_FALLBACK)
        self.icon_to_remove = self.icon_load(
            ICON_PURGE, 24, ICON_PURGE_FALLBACK)
        self.icon_upgrade = self.icon_load(
            ICON_UPGRADE, 24, ICON_UPGRADE_FALLBACK)
        self.icon_upgrade_urgent = self.icon_load(ICON_UPGRADE_URGENT, 24)
        self.icon_downgrade = self.icon_load(
            ICON_DOWNGRADE, 24, ICON_DOWNGRADE_FALLBACK)
        self.icon_nobuild = self.icon_load(ICON_LOCKED, 24)

        # Files icons
        self.icon_folder = self.icon_load(ICON_FOLDER)
        self.icon_file = self.icon_load(ICON_FILE)

        # Network icons
        self.icon_network_online = self.icon_load(ICON_NETWORK_ONLINE, 22)
        self.icon_network_offline = self.icon_load(ICON_NETWORK_OFFLINE, 22)
        self.icon_network_receive = self.icon_load(ICON_NETWORK_RECEIVE, 22)

        # Default icon for package informations
        self.icon_package = self.icon_load(ICON_AVAILABLE, 22)
        self.icon_files_manager = self.icon_load(ICON_FILES_MANAGER, 24)
        self.icon_changelog = self.icon_load(ICON_CHANGELOG, 24)
        self.icon_frugalbuild = self.icon_load(ICON_FRUGALBUILD, 24)
        self.icon_repoman = self.icon_load(ICON_REPOMAN, 24)
        self.icon_cache = self.icon_load(ICON_HARDDRIVE, 24)

        # Group icon
        self.icon_group = self.icon_load(ICON_AVAILABLE, 32)

        # Windows icons
        self.category_transaction = self.icon_load(ICON_AVAILABLE, 48)
        self.category_cache = self.icon_load(
            ICON_CLEAR, 48, ICON_CLEAR_FALLBACK)
        self.category_upgrade = self.icon_load(
            ICON_UPGRADE, 48, ICON_UPGRADE_FALLBACK)
        self.category_file = self.icon_load(ICON_FILES_MANAGER, 48)
        self.category_changelog = self.icon_load(ICON_CHANGELOG, 48)
        self.category_frugalbuild = self.icon_load(ICON_FRUGALBUILD, 48)
        self.category_screenshot = self.icon_load(ICON_SCREENSHOT, 48)
        self.category_logs = self.icon_load(ICON_LOGVIEWER, 48)
        self.category_terminal = self.icon_load(ICON_REPOMAN, 48)

        # Face icon
        self.icon_interface_delete = gtk.image_new_from_icon_name(
            ICON_PYFPM_DELETE, gtk.ICON_SIZE_DIALOG)

        # Change icon render
        if not self.icons_theme.lookup_icon(ICON_AVAILABLE, 24, 0) == None:
            self.icon_available.saturate_and_pixelate(
                self.icon_available, 0.0, False)

        # Generate shortcuts array for help window
        self.__generate_shortcuts()

        # Set main clipboard
        self.clipboard = gtk.Clipboard(
            gtk.gdk.display_get_default(), "CLIPBOARD")

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.main_shortcuts = gtk.AccelGroup()

        # Properties
        self.set_title(_("Frugalware Packages Manager - %s" % __version__))
        self.set_wmclass("fpm-gui", "fpm-gui")
        self.set_icon_name("fpm-gui")

        self.set_resizable(True)
        self.set_position(gtk.WIN_POS_CENTER)

        self.set_geometry_hints(None, 800, 600)
        self.set_default_size(
            int(self.config.get("application", "width")),
            int(self.config.get("application", "height")))

        self.set_mnemonics_visible(True)
        self.add_accel_group(self.main_shortcuts)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.box = gtk.VBox()

        # Groups
        self.paned_groups = gtk.HPaned()
        self.box_repositories = gtk.HBox()
        self.box_groups = gtk.VBox()

        # Packages
        self.paned_packages = gtk.VPaned()
        self.box_notebook = gtk.VBox()
        self.box_informations = gtk.HBox()

        # Informations
        self.box_icone = gtk.HBox()
        self.box_buttons = gtk.HBox()
        self.box_screenshot = gtk.Table()

        box_button_files = gtk.HBox()
        box_button_changelog = gtk.HBox()
        box_button_frugalbuild = gtk.HBox()
        box_button_build = gtk.HBox()
        box_button_reinstall = gtk.HBox()

        # Properties
        self.box_repositories.set_spacing(10)
        self.box_notebook.set_spacing(10)
        self.box_buttons.set_spacing(4)
        self.box_icone.set_spacing(4)

        self.box_screenshot.set_row_spacings(4)

        box_button_files.set_spacing(4)
        box_button_changelog.set_spacing(4)
        box_button_frugalbuild.set_spacing(4)
        box_button_build.set_spacing(4)
        box_button_reinstall.set_spacing(4)

        self.box_groups.set_border_width(4)
        self.box_informations.set_border_width(4)

        self.paned_groups.set_position(250)
        self.paned_groups.set_border_width(4)

        # ------------------------------------
        #   Menu
        # ------------------------------------

        self.menu = gtk.MenuBar()

        self.item_system = gtk.MenuItem()
        self.item_edit = gtk.MenuItem()
        self.item_filters = gtk.MenuItem()
        self.item_selection = gtk.MenuItem()
        self.item_help = gtk.MenuItem()

        self.menu_system = gtk.Menu()
        self.menu_edit = gtk.Menu()
        self.menu_filters = gtk.Menu()
        self.menu_selection = gtk.Menu()
        self.menu_help = gtk.Menu()

        self.item_upgrade = gtk.ImageMenuItem()
        self.item_install = gtk.ImageMenuItem()
        self.item_cache = gtk.ImageMenuItem()
        self.item_update = gtk.ImageMenuItem()
        self.item_chroot = gtk.ImageMenuItem()
        self.item_logs = gtk.ImageMenuItem()
        self.item_quit = gtk.ImageMenuItem()

        self.item_get_screenshot = gtk.CheckMenuItem()
        self.item_preferences = gtk.ImageMenuItem()
        self.item_repositories = gtk.ImageMenuItem()

        self.filter_install = gtk.CheckMenuItem()
        self.filter_remove = gtk.CheckMenuItem()
        self.filter_upgrade = gtk.CheckMenuItem()
        self.filter_downgrade = gtk.CheckMenuItem()
        self.filter_nobuild = gtk.CheckMenuItem()

        self.item_apply = gtk.ImageMenuItem()
        self.item_reset = gtk.ImageMenuItem()

        self.item_shortcuts = gtk.ImageMenuItem()
        self.item_about = gtk.ImageMenuItem()

        # Properties
        self.item_system.set_use_underline(True)
        self.item_system.set_label(_("_System"))

        self.item_edit.set_use_underline(True)
        self.item_edit.set_label(_("Edi_t"))

        self.item_filters.set_use_underline(True)
        self.item_filters.set_label(_("_View"))

        self.item_selection.set_use_underline(True)
        self.item_selection.set_label(_("S_election"))

        self.item_help.set_use_underline(True)
        self.item_help.set_label(_("_Help"))

        self.item_apply.set_use_underline(True)
        self.item_apply.set_label(_("_Apply changes"))
        self.item_apply.set_image(gtk.image_new_from_stock(
            gtk.STOCK_APPLY, gtk.ICON_SIZE_MENU))

        self.item_reset.set_use_underline(True)
        self.item_reset.set_label(_("_Reset packages selection"))
        self.item_reset.set_image(gtk.image_new_from_stock(
            gtk.STOCK_CLEAR, gtk.ICON_SIZE_MENU))

        self.item_install.set_use_underline(True)
        self.item_install.set_label(_("_Install a local package"))
        self.item_install.set_image(gtk.image_new_from_stock(
            gtk.STOCK_ADD, gtk.ICON_SIZE_MENU))

        self.item_cache.set_use_underline(True)
        self.item_cache.set_label(_("_Clean packages cache"))
        self.item_cache.set_image(gtk.image_new_from_stock(
            gtk.STOCK_CLEAR, gtk.ICON_SIZE_MENU))

        self.item_update.set_use_underline(True)
        self.item_update.set_label(_("_Update pacman-g2 databases"))
        self.item_update.set_image(gtk.image_new_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_MENU))

        self.item_chroot.set_use_underline(True)
        self.item_chroot.set_label(_("Upgrade 32bits _environment"))
        self.item_chroot.set_image(gtk.image_new_from_stock(
            gtk.STOCK_HARDDISK, gtk.ICON_SIZE_MENU))

        self.item_upgrade.set_use_underline(True)
        self.item_upgrade.set_label(_("C_heck packages update"))
        self.item_upgrade.set_image(gtk.image_new_from_stock(
            gtk.STOCK_FIND, gtk.ICON_SIZE_MENU))

        self.item_logs.set_use_underline(True)
        self.item_logs.set_label(_("_Logs viewer"))
        self.item_logs.set_image(gtk.image_new_from_stock(
            gtk.STOCK_FILE, gtk.ICON_SIZE_MENU))

        self.item_quit.set_use_underline(True)
        self.item_quit.set_label(_("_Quit"))
        self.item_quit.set_image(gtk.image_new_from_stock(
            gtk.STOCK_QUIT, gtk.ICON_SIZE_MENU))

        self.item_get_screenshot.set_use_underline(True)
        self.item_get_screenshot.set_label(_("Download _screenshots"))
        self.item_get_screenshot.set_active(True)

        self.item_preferences.set_use_underline(True)
        self.item_preferences.set_label(_("_Preferences"))
        self.item_preferences.set_image(gtk.image_new_from_stock(
            gtk.STOCK_PREFERENCES, gtk.ICON_SIZE_MENU))

        self.item_repositories.set_use_underline(True)
        self.item_repositories.set_label(_("_Repositories"))
        self.item_repositories.set_image(gtk.image_new_from_stock(
            gtk.STOCK_CDROM, gtk.ICON_SIZE_MENU))

        self.item_about.set_use_underline(True)
        self.item_about.set_label(_("_About"))
        self.item_about.set_image(gtk.image_new_from_stock(
            gtk.STOCK_ABOUT, gtk.ICON_SIZE_MENU))

        self.item_shortcuts.set_use_underline(True)
        self.item_shortcuts.set_label(_("_Help"))
        self.item_shortcuts.set_image(gtk.image_new_from_stock(
            gtk.STOCK_HELP, gtk.ICON_SIZE_MENU))

        self.filter_install.set_use_underline(True)
        self.filter_install.set_label(_("_Installed packages"))
        self.filter_install.set_active(True)

        self.filter_remove.set_use_underline(True)
        self.filter_remove.set_label(_("_Not installed packages"))
        self.filter_remove.set_active(True)

        self.filter_upgrade.set_use_underline(True)
        self.filter_upgrade.set_label(_("_Available packages update"))
        self.filter_upgrade.set_active(True)

        self.filter_downgrade.set_use_underline(True)
        self.filter_downgrade.set_label(_("Ne_wer packages"))
        self.filter_downgrade.set_active(True)

        self.filter_nobuild.set_use_underline(True)
        self.filter_nobuild.set_label(_("No_build packages"))
        self.filter_nobuild.set_active(True)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.toolbar = gtk.Toolbar()

        self.tool_apply = gtk.ToolButton()
        self.tool_reset = gtk.ToolButton()
        self.tool_upgrade = gtk.ToolButton()
        self.tool_update = gtk.ToolButton()
        self.tool_preferences = gtk.ToolButton()
        self.tool_repositories = gtk.ToolButton()
        self.tool_quit = gtk.ToolButton()
        self.tool_request = gtk.ToolItem()

        self.tool_expand = gtk.SeparatorToolItem()

        self.entry_request = gtk.Entry()
        self.modelSearch = gtk.ListStore(str)
        self.completionSearch = gtk.EntryCompletion()

        # Properties
        self.toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)

        self.tool_apply.set_stock_id(gtk.STOCK_APPLY)
        self.tool_apply.set_tooltip_text(_("Apply changes"))

        self.tool_reset.set_label(_("Reset selection"))
        self.tool_reset.set_stock_id(gtk.STOCK_CLEAR)
        self.tool_reset.set_tooltip_text(_("Reset packages selection"))

        self.tool_upgrade.set_label(_("Show updates"))
        self.tool_upgrade.set_icon_name(ICON_UPGRADE_FALLBACK)
        self.tool_upgrade.set_tooltip_text(_("Check packages update"))

        self.tool_update.set_label(_("Upgrade databases"))
        self.tool_update.set_stock_id(gtk.STOCK_REFRESH)
        self.tool_update.set_tooltip_text(_("Update pacman-g2 databases"))

        self.tool_preferences.set_stock_id(gtk.STOCK_PREFERENCES)
        self.tool_preferences.set_tooltip_text(_("Preferences"))

        self.tool_repositories.set_stock_id(gtk.STOCK_CDROM)
        self.tool_repositories.set_tooltip_text(_("Repositories"))

        self.tool_quit.set_stock_id(gtk.STOCK_QUIT)
        self.tool_quit.set_tooltip_text(_("Quit"))

        self.tool_expand.set_expand(True)
        self.tool_expand.set_draw(False)

        self.tool_request.set_border_width(2)

        self.entry_request.set_size_request(250, -1)
        self.entry_request.set_completion(self.completionSearch)
        self.entry_request.set_icon_from_stock(0, gtk.STOCK_FIND)
        self.entry_request.set_icon_from_stock(1, gtk.STOCK_CLEAR)
        self.entry_request.set_tooltip_text(_("Search in database"))

        self.completionSearch.set_text_column(0)
        self.completionSearch.set_model(self.modelSearch)
        self.completionSearch.set_popup_completion(True)

        # ------------------------------------
        #   Repositories list
        # ------------------------------------

        label_repositories = gtk.Label()

        self.combo_repositories = gtk.combo_box_new_text()

        # Properties
        label_repositories.set_markup("<b>%s</b>: " % _("Repositories"))
        label_repositories.set_alignment(0, 0.5)
        label_repositories.set_use_markup(True)

        # ------------------------------------
        #   Groups list
        # ------------------------------------

        self.store_groups = gtk.TreeStore(gtk.gdk.Pixbuf, str)
        self.treeview_groups = gtk.TreeView(self.store_groups)

        column_groups = gtk.TreeViewColumn()

        cell_category = gtk.CellRendererPixbuf()
        cell_group = gtk.CellRendererText()

        scroll_groups = gtk.ScrolledWindow()

        # Properties
        self.store_groups.set_sort_column_id(1, gtk.SORT_ASCENDING)

        self.treeview_groups.set_headers_visible(False)
        self.treeview_groups.set_enable_search(False)
        self.treeview_groups.set_search_column(1)
        self.treeview_groups.set_row_separator_func(self.treeview_separator)

        column_groups.set_sort_column_id(1)
        column_groups.pack_start(cell_category, False)
        column_groups.pack_start(cell_group, True)
        column_groups.add_attribute(cell_category, "pixbuf", 0)
        column_groups.add_attribute(cell_group, "text", 1)

        self.treeview_groups.append_column(column_groups)

        scroll_groups.set_border_width(2)
        scroll_groups.set_resize_mode(gtk.RESIZE_PARENT)
        scroll_groups.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Packages list
        # ------------------------------------

        self.notebook_packages = gtk.Notebook()

        # Properties
        self.notebook_packages.remove_page(0)
        self.notebook_packages.set_border_width(4)
        self.notebook_packages.set_resize_mode(gtk.RESIZE_PARENT)

        # ------------------------------------
        #   Package menu
        # ------------------------------------

        self.menu_package = gtk.Menu()

        self.item_to_reinstall = gtk.ImageMenuItem()
        self.item_to_build = gtk.ImageMenuItem()
        self.item_separator = gtk.SeparatorMenuItem()
        self.item_to_install = gtk.ImageMenuItem()
        self.item_to_all = gtk.ImageMenuItem()
        self.item_to_remove = gtk.ImageMenuItem()

        # Properties
        self.item_to_reinstall.set_label(_("Reinstall this package"))
        self.item_to_reinstall.set_image(gtk.image_new_from_stock(
            gtk.STOCK_REDO, gtk.ICON_SIZE_MENU))

        self.item_to_build.set_label(_("Build this package"))
        self.item_to_build.set_image(gtk.image_new_from_stock(
            gtk.STOCK_EXECUTE, gtk.ICON_SIZE_MENU))

        self.item_to_install.set_label(
            _("Mark not installed packages to install"))
        self.item_to_install.set_image(gtk.image_new_from_stock(
            gtk.STOCK_ADD, gtk.ICON_SIZE_MENU))

        self.item_to_all.set_label(_("Mark all packages to install/reinstall"))
        self.item_to_all.set_image(gtk.image_new_from_stock(
            gtk.STOCK_ADD, gtk.ICON_SIZE_MENU))

        self.item_to_remove.set_label(_("Mark installed packages to remove"))
        self.item_to_remove.set_image(gtk.image_new_from_stock(
            gtk.STOCK_REMOVE, gtk.ICON_SIZE_MENU))

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        self.notebook_details = gtk.Notebook()

        label_general = gtk.Label()
        label_details = gtk.Label()

        # Properties
        label_general.set_label(_("General"))
        label_details.set_label(_("Details"))

        self.notebook_details.set_border_width(4)
        self.notebook_details.set_resize_mode(gtk.RESIZE_PARENT)

        # ------------------------------------
        #   Notebook - General
        # ------------------------------------

        self.label_name = gtk.Label()
        self.label_description = gtk.Label()
        self.label_website = gtk.Label()
        self.label_build = gtk.Label()
        self.label_reinstall = gtk.Label()

        self.image_package = gtk.Image()
        self.image_screenshot = gtk.Image()
        self.image_zoom = gtk.Image()
        image_files = gtk.Image()
        image_changelog = gtk.Image()
        image_frugalbuild = gtk.Image()
        image_build = gtk.Image()
        self.image_reinstall = gtk.Image()
        self.image_cache = gtk.Image()

        self.alignment_package = gtk.Alignment(0, 0.5, 0, 0)
        self.alignment_screen = gtk.Alignment(0, 0, 0, 0)
        self.alignment_zoom = gtk.Alignment(0, 0, 0, 0)
        self.alignment_cache = gtk.Alignment(1, 0.5, 0, 0)

        self.button_zoom = gtk.Button()
        self.button_files = gtk.Button()
        self.button_changelog = gtk.Button()
        self.button_frugalbuild = gtk.Button()
        self.button_build = gtk.Button()
        self.button_reinstall = gtk.Button()

        view_informations = gtk.Viewport()
        scroll_informations = gtk.ScrolledWindow()

        # Properties
        self.label_name.set_selectable(True)
        self.label_name.set_use_markup(True)
        self.label_name.set_alignment(0, 0.5)
        self.label_name.set_use_underline(False)

        self.label_description.set_line_wrap(True)
        self.label_description.set_selectable(True)
        self.label_description.set_use_markup(True)
        self.label_description.set_alignment(0, 0.5)
        self.label_description.set_use_underline(False)
        self.label_description.set_justify(gtk.JUSTIFY_FILL)

        self.label_website.set_use_markup(True)
        self.label_website.set_alignment(0, 0.5)
        self.label_website.set_use_underline(False)

        self.label_build.set_label(_("Merge this package"))

        self.image_zoom.set_from_stock(
            gtk.STOCK_FIND, gtk.ICON_SIZE_SMALL_TOOLBAR)
        image_files.set_from_pixbuf(self.icon_files_manager)
        image_changelog.set_from_pixbuf(self.icon_changelog)
        image_frugalbuild.set_from_pixbuf(self.icon_frugalbuild)
        image_build.set_from_pixbuf(self.icon_repoman)
        self.image_cache.set_alignment(1, 0.5)

        self.button_zoom.set_relief(gtk.RELIEF_NONE)
        self.button_zoom.set_image(self.image_zoom)

        self.button_files.set_sensitive(False)
        self.button_files.set_tooltip_text(_("See package files"))
        self.button_changelog.set_sensitive(False)
        self.button_changelog.set_tooltip_text(_("See package Changelog"))
        self.button_frugalbuild.set_sensitive(False)
        self.button_frugalbuild.set_tooltip_text(_("See package FrugalBuild"))
        self.button_build.set_sensitive(False)
        self.button_build.set_tooltip_text(_("Build this package"))
        self.button_reinstall.set_sensitive(False)

        view_informations.set_shadow_type(gtk.SHADOW_NONE)

        scroll_informations.set_border_width(4)
        scroll_informations.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Notebook - Details
        # ------------------------------------

        self.store_details = gtk.TreeStore(str, str, str, bool)
        self.treeview_details = gtk.TreeView(self.store_details)

        column_details = gtk.TreeViewColumn()

        cell_title = gtk.CellRendererText()
        cell_value = gtk.CellRendererText()

        scroll_details = gtk.ScrolledWindow()

        # Properties
        self.treeview_details.set_headers_visible(False)
        self.treeview_details.set_hover_selection(False)
        self.treeview_details.set_show_expanders(False)

        cell_title.set_property("weight", WEIGHT_BOLD)
        cell_value.set_property("wrap-mode", WRAP_WORD)
        cell_value.set_property("editable", True)

        column_details.pack_start(cell_title, False)
        column_details.pack_start(cell_value, True)
        column_details.add_attribute(cell_value, "text", 1)
        column_details.add_attribute(cell_title, "text", 0)
        column_details.set_attributes(cell_value,
            text=1, foreground=2, foreground_set=3)

        self.treeview_details.append_column(column_details)

        scroll_details.set_border_width(4)
        scroll_details.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Statusbar
        # ------------------------------------

        self.statusbar = gtk.Statusbar()

        self.tool_offline = gtk.ToggleButton()
        self.image_offline = gtk.Image()

        # Properties
        self.statusbar.set_has_resize_grip(False)

        box_statusbar = self.statusbar.get_message_area()

        self.label_status = box_statusbar.get_children()[0]
        self.label_status.set_use_markup(True)

        self.tool_offline.set_relief(gtk.RELIEF_NONE)
        self.tool_offline.set_image(self.image_offline)

        if self.config.get("network", "offline"):
            self.tool_offline.set_active(True)
            self.image_offline.set_from_pixbuf(self.icon_network_offline)

            self.item_update.set_sensitive(False)
            self.tool_update.set_sensitive(False)

            LOG.debug(_("Offline mode has been choosen"))

        else:
            self.tool_offline.set_active(False)
            self.image_offline.set_from_pixbuf(self.icon_network_online)

            self.item_update.set_sensitive(True)
            self.tool_update.set_sensitive(True)

            LOG.debug(_("Online mode has been choosen"))

        # ------------------------------------
        #   Add widgets into main interface
        # ------------------------------------

        self.add(self.box)

        # Main table
        self.box.pack_start(self.menu, False)
        self.box.pack_start(self.toolbar, False)
        self.box.pack_start(self.paned_groups)
        self.box.pack_start(self.statusbar, False)

        # Merge Repositories/Groups and Packages area
        self.paned_groups.pack1(self.box_groups, False, False)
        self.paned_groups.pack2(self.paned_packages, True, False)

        # Menu
        self.menu.add(self.item_system)
        self.menu.add(self.item_edit)
        self.menu.add(self.item_filters)
        self.menu.add(self.item_selection)
        self.menu.add(self.item_help)

        self.item_system.set_submenu(self.menu_system)
        self.item_edit.set_submenu(self.menu_edit)
        self.item_filters.set_submenu(self.menu_filters)
        self.item_selection.set_submenu(self.menu_selection)
        self.item_help.set_submenu(self.menu_help)

        self.menu_system.add(self.item_upgrade)
        self.menu_system.add(gtk.SeparatorMenuItem())
        self.menu_system.add(self.item_install)
        self.menu_system.add(self.item_cache)
        self.menu_system.add(self.item_update)
        self.menu_system.add(self.item_chroot)
        self.menu_system.add(gtk.SeparatorMenuItem())
        self.menu_system.add(self.item_logs)
        self.menu_system.add(gtk.SeparatorMenuItem())
        self.menu_system.add(self.item_quit)

        # self.menu_edit.add(self.item_get_screenshot)
        # self.menu_edit.add(gtk.SeparatorMenuItem())
        self.menu_edit.add(self.item_repositories)
        self.menu_edit.add(self.item_preferences)

        self.menu_filters.add(self.filter_install)
        self.menu_filters.add(self.filter_remove)
        self.menu_filters.add(self.filter_upgrade)
        self.menu_filters.add(self.filter_downgrade)
        self.menu_filters.add(self.filter_nobuild)

        self.menu_selection.add(self.item_apply)
        self.menu_selection.add(self.item_reset)

        self.menu_help.add(self.item_shortcuts)
        self.menu_help.add(gtk.SeparatorMenuItem())
        self.menu_help.add(self.item_about)

        # Toolbar
        self.toolbar.insert(self.tool_apply, -1)
        self.toolbar.insert(self.tool_reset, -1)
        self.toolbar.insert(self.tool_upgrade, -1)
        self.toolbar.insert(gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.tool_update, -1)
        self.toolbar.insert(gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.tool_quit, -1)
        self.toolbar.insert(self.tool_expand, -1)
        self.toolbar.insert(self.tool_request, -1)

        self.tool_request.add(self.entry_request)

        # Repositories
        self.box_repositories.pack_start(label_repositories, False)
        self.box_repositories.pack_start(self.combo_repositories)

        # Groups
        scroll_groups.add(self.treeview_groups)

        self.box_groups.pack_start(self.box_repositories, False)
        self.box_groups.pack_start(scroll_groups)

        # Packages menu
        self.menu_package.append(self.item_to_reinstall)
        self.menu_package.append(self.item_to_build)
        self.menu_package.append(self.item_separator)
        self.menu_package.append(self.item_to_install)
        self.menu_package.append(self.item_to_all)
        self.menu_package.append(self.item_to_remove)

        # Notebook
        self.paned_packages.pack1(self.notebook_packages, True, False)
        self.paned_packages.pack2(self.notebook_details, False, False)

        # Notebook general
        self.box_notebook.pack_start(self.box_icone, False, False)
        self.box_notebook.pack_start(self.label_description, False, False)
        self.box_notebook.pack_start(self.label_website, False, False)
        self.box_notebook.pack_end(self.box_buttons, False, False)

        self.box_icone.pack_start(self.alignment_package, False, False)
        self.box_icone.pack_start(self.label_name, False, False)

        self.alignment_package.add(self.image_package)
        self.alignment_screen.add(self.image_screenshot)
        self.alignment_zoom.add(self.button_zoom)
        self.alignment_cache.add(self.image_cache)

        self.button_files.add(box_button_files)
        self.button_changelog.add(box_button_changelog)
        self.button_frugalbuild.add(box_button_frugalbuild)
        self.button_build.add(box_button_build)
        self.button_reinstall.add(box_button_reinstall)

        box_button_files.pack_start(image_files, False, False)
        box_button_changelog.pack_start(image_changelog, False, False)
        box_button_frugalbuild.pack_start(image_frugalbuild, False, False)
        box_button_build.pack_start(image_build, False, False)
        box_button_build.pack_start(self.label_build, False, False)
        box_button_reinstall.pack_start(self.image_reinstall, False, False)
        box_button_reinstall.pack_start(self.label_reinstall, False, False)

        self.box_buttons.pack_start(self.button_files, False, False)
        self.box_buttons.pack_start(self.button_changelog, False, False)
        self.box_buttons.pack_start(self.button_frugalbuild, False, False)
        self.box_buttons.pack_start(self.button_build, False, False)
        self.box_buttons.pack_start(self.button_reinstall, False, False)
        self.box_buttons.pack_end(self.alignment_cache)

        self.box_screenshot.attach(self.alignment_screen, 0, 1, 0, 1,
            xoptions=gtk.FILL, yoptions=gtk.FILL)
        self.box_screenshot.attach(self.alignment_zoom, 0, 1, 1, 2,
            xoptions=gtk.SHRINK)

        self.box_informations.pack_start(self.box_notebook)
        self.box_informations.pack_start(self.box_screenshot, False, False)

        view_informations.add(self.box_informations)
        scroll_informations.add(view_informations)

        # Notebook - Details
        scroll_details.add(self.treeview_details)

        self.notebook_details.append_page(scroll_informations, label_general)
        self.notebook_details.append_page(scroll_details, label_details)

        # Statusbar
        box_statusbar.pack_start(self.tool_offline, False, False)
        box_statusbar.pack_start(gtk.VSeparator(), False, False, 2)
        box_statusbar.reorder_child(self.label_status, -1)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        # Main window
        self.connect("destroy", self.__stop_interface)
        self.connect("check-resize", self.__refresh_interface)

        # Menu
        self.item_install.connect("activate", WindowSyncFpm, self)
        self.item_cache.connect("activate", WindowCache, self)
        self.item_update.connect("activate", self.databases_update)
        self.item_chroot.connect("activate", self.chroot_update)

        self.item_upgrade.connect("activate", WindowUpdate, self)
        self.item_quit.connect("activate", self.__stop_interface)

        self.item_apply.connect("activate", self.window_sync_packages)
        self.item_reset.connect("activate", self.selection_reset)

        self.item_logs.connect("activate", WindowLog, self)
        self.item_preferences.connect("activate", WindowPreferences, self)
        self.item_repositories.connect("activate", WindowRepositories, self)

        self.filter_install.connect("activate", self.filters_update)
        self.filter_remove.connect("activate", self.filters_update)
        self.filter_upgrade.connect("activate", self.filters_update)
        self.filter_downgrade.connect("activate", self.filters_update)
        self.filter_nobuild.connect("activate", self.filters_update)

        self.item_about.connect("activate", WindowAbout, self)
        self.item_shortcuts.connect("activate", WindowHelp, self)

        # Toolbar
        self.tool_apply.connect("clicked", self.window_sync_packages)
        self.tool_reset.connect("clicked", self.selection_reset)
        self.tool_upgrade.connect("clicked", WindowUpdate, self)
        self.tool_update.connect("clicked", self.databases_update)
        self.tool_preferences.connect("clicked", WindowPreferences, self)
        self.tool_repositories.connect("clicked", WindowRepositories, self)
        self.tool_quit.connect("clicked", self.__stop_interface)

        self.entry_request.connect("activate", self.packages_search,
            gtk.RESPONSE_OK)
        self.entry_request.connect("icon-press", self.search_activate)

        # Repositories combobox
        self.combo_repositories.connect("changed",
            self.repositories_select, self)

        # Groups treeview
        self.treeview_groups.connect("button-press-event", self.groups_select)
        self.treeview_groups.connect("key-release-event", self.groups_select)

        # Packages notebook
        self.notebook_packages.connect("switch-page", self.notebook_update)

        # Packages menu
        self.item_to_reinstall.connect("activate", self.menu_mark_to_reinstall)
        self.item_to_build.connect("activate", WindowBuild, self)
        self.item_to_install.connect("activate", self.menu_mark_packages, False)
        self.item_to_all.connect("activate", self.menu_mark_packages)
        self.item_to_remove.connect("activate", self.menu_mark_packages)

        # Package information buttons
        self.button_zoom.connect("clicked", self.screenshots_large_show)
        self.button_files.connect("clicked", self.window_files)
        self.button_changelog.connect("clicked", self.window_changelog)
        self.button_frugalbuild.connect("clicked", self.window_frugalbuild)
        self.button_build.connect("clicked", WindowBuild, self)
        self.button_reinstall.connect("clicked", self.menu_mark_to_reinstall)

        # Statusbar
        self.tool_offline.connect("toggled", self.network_update)

        # Shortcuts
        self.__generate_shortcuts_signals()


    def __start_interface (self):
        """
        Load all interface elements
        """

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        # Cache path
        self.cachePath = self.config.get("application", "cache")

        # Create a pacman-g2 configuration instance
        self.pacmanConfig = PacmanConfig()

        # ------------------------------------
        #   Splash
        # ------------------------------------

        self.splash = Splash([
            _("Connect to FPMd daemon"),
            _("Check screenshots websites"),
            _("Get available upgrade"),
            _("Prepare fpm-gui interface") ])

        refresh()

        # ------------------------------------
        #   Database
        # ------------------------------------

        self.splash.update()

        # Create a database instance to access to fpmd
        self.pacman = Database()
        self.pacman.debug = self.debug

        # Reload fpmd data
        self.pacman.reload_daemon()

        # ------------------------------------
        #   Screenshots website
        # ------------------------------------

        # Check screenshot website
        if not self.config.get("network", "offline") and \
            self.config.get("network", "download"):
            self.__website_checked = True

            timeout_add(10000, self.__check_servers, WEBSITE, WEBSITE_FALLBACK)

        # ------------------------------------
        #   Notification
        # ------------------------------------

        # Init notification module
        if self.config.get("application", "show_notification"):
            try:
                notifyInit("fpm-gui")

            except Exception as err:
                LOG.error(_("Cannot load notification daemon"),
                    exc_info=self.debug)

                self.config.set("application", "show_notification", "false")
                self.config.write_configuration()

        # ------------------------------------
        #   Check
        # ------------------------------------

        LOG.debug(_("%d packages are installed on your system") % (
            len(self.pacman.packages_installed())))

        if exists(FW32_UPGRADE_BIN):
            self.__fw32 = True

        if exists(REPOMAN_BIN):
            self.repoman = True

        # ------------------------------------
        #   Repositories
        # ------------------------------------

        self.splash.update()

        for repository in self.pacman.repositories()[1:]:
            self.packages_data_widgets[str(repository)] = \
                PackagesList(self, str(repository))

        self.repositories_add()

        self.selected["repository"] = self.combo_repositories.get_active_text()

        # ------------------------------------
        #   Upgrade
        # ------------------------------------

        self.splash.update()

        self.upgrade_get()

        # ------------------------------------
        #   Saved packages
        # ------------------------------------

        # We save update list to use them later
        self.packages_data["saved"] = deepcopy(self.packages_data["upgrade"])

        # Get packages to install
        packages = self.config.get("packages", "install")
        if packages is not None and len(packages) > 0:

            # Check if packages are always in update list or not installed
            for package in packages.split():
                if package in self.packages_data["saved"] or \
                    not self.pacman.package_installed(package)[0]:
                    self.packages_data["installation"].append(package)

            self.config.set("packages", "install", str())

        # Get packages to reinstall
        packages = self.config.get("packages", "reinstall")
        if packages is not None and len(packages) > 0:

            self.packages_data["reinstallation"] = packages.split()

            self.config.set("packages", "reinstall", str())

        # Get packages to remove
        packages = self.config.get("packages", "remove")
        if packages is not None and len(packages) > 0:

            # Check if packages are always install
            for package in packages.split():
                if self.pacman.package_installed(package)[0]:
                    self.packages_data["remove"].append(package)

            self.config.set("packages", "remove", str())

        # Remove saved list in configuration file
        self.config.write_configuration()

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.splash.update()

        self.show_all()
        self.menu_package.show_all()

        self.button_zoom.hide()

        for widget in self.box_buttons.get_children():
            widget.hide()

        # Hide repositories combobox when only one repository is available
        if len(self.combo_repositories.get_model()) == 1:
            for widget in self.box_repositories.get_children():
                widget.hide()

        else:
            self.box_groups.set_child_packing(self.box_repositories,
                False, True, 6, gtk.PACK_START)

        self.menu_hide_entries()

        # Destroy splash window
        self.splash.destroy()

        refresh()

        # Update notebook height
        self.paned_packages.set_position(
            (3 * self.paned_packages.allocation.height) / 5)

        # ------------------------------------
        #   Repoman installation
        # ------------------------------------

        # Show a window to help user to install repoman
        if not self.repoman and self.config.get("application", "first_use"):
            WindowRepoman(None, self)

        # ------------------------------------
        #   Databases last update
        # ------------------------------------

        # Check Frugalware repository last update
        if self.databases_check_lastupdate():
            question = WindowQuestion(_("Repositories update"),
                _("Frugalware repository hasn't been updated since a long "
                "time. Would you want to update the Frugalware repository ?"))

            if question.start():
                self.databases_update(None)

        if len(self.packages_data["saved"]) > 0 and \
            self.config.get("application", "show_update_window"):

            self.window_upgrade()

        # Focus in search entry widget
        self.entry_request.grab_focus()

        # Get informations into statusbar
        self.statusbar_update()

        gtk.main()


    def __stop_interface (self, widget):
        """
        Close interface
        """

        # Stop packages loading thread
        self.__stop_threads()

        # Stop screenshots threads
        if not self.__thread_thumbnail == 0:
            source_remove(self.__thread_thumbnail)
        if not self.__thread_screenshot == 0:
            source_remove(self.__thread_screenshot)

        # Save packages selection
        if len(self.packages_data["installation"]) > 0 or \
            len(self.packages_data["reinstallation"]) > 0 or \
            len(self.packages_data["remove"]) > 0:

            question = WindowQuestion(_("Packages selection"),
                _("Some packages has been selected. Do you want to save your "
                "selection ?"))

            if question.start():

                if len(self.packages_data["installation"]) > 0:
                    self.config.set("packages", "install",
                        ' '.join(self.packages_data["installation"]))

                if len(self.packages_data["reinstallation"]) > 0:
                    self.config.set("packages", "reinstall",
                        ' '.join(self.packages_data["reinstallation"]))

                if len(self.packages_data["remove"]) > 0:
                    self.config.set("packages", "remove",
                        ' '.join(self.packages_data["remove"]))

                self.config.write_configuration()

        # Save main window size
        if self.config.get("application", "save_window_size"):

            width = str(self.get_size()[0])
            height = str(self.get_size()[1])

            if not self.config.get("application", "width") == width \
                or not self.config.get("application", "height") == height:

                self.config.set("application", "width", width)
                self.config.set("application", "height", height)

                self.config.write_configuration()

        LOG.info(_("Bye bye"))

        gtk.main_quit() # PacMiam was here

    # ------------------------------------
    #   Shortcuts
    # ------------------------------------

    def __generate_shortcuts (self):
        """
        Generate shortcuts dictionnary data
        """

        self.helpShortcuts = dict(
            icons=[
                [self.icon_available,
                    _("Not installed package")],
                [self.icon_installed,
                    _("Installed package")],
                [self.icon_to_install,
                    _("Package marked to install/reinstall/update")],
                [self.icon_to_remove,
                    _("Package marked to remove")],
                [self.icon_upgrade,
                    _("Package with an available update")],
                [self.icon_upgrade_urgent,
                    _("Package with an available urgent update")],
                [self.icon_downgrade,
                    _("Package which had a version superior to available one")],
                [self.icon_nobuild,
                    _("Package which must be build by user")] ],
            interfaces=[
                [self.config.get("shortcuts", "open_help"),
                    _("Show this help")],
                [self.config.get("shortcuts", "change_network_status"),
                    _("Check/Uncheck online status")],
                list(),
                [self.config.get("shortcuts", "focus_search_entry"),
                    _("Focus into search entry")],
                [self.config.get("shortcuts", "open_preferences"),
                    _("Show preferences")],
                [self.config.get("shortcuts", "open_repositories"),
                    _("Show repositories")],
                [self.config.get("shortcuts", "open_logs"),
                    _("Show pacman-g2 logs")],
                [self.config.get("shortcuts", "show_upgrade"),
                    _("Show available updates")],
                ["<Control>Q", _("Quit")],
                list(),
                [self.config.get("shortcuts", "install_local_fpm"),
                    _("Install a local package")],
                [self.config.get("shortcuts", "clean_packages_cache"),
                    _("Clean pacman-g2 packages cache")],
                [self.config.get("shortcuts", "update_chroot"),
                    _("Upgrade 32bits environment"), self.__fw32],
                [self.config.get("shortcuts", "update_databases"),
                    _("Update pacman-g2 databases")] ],
            packages=[
                [self.config.get("shortcuts", "open_files_manager"),
                    _("Show package's files if available")],
                [self.config.get("shortcuts", "open_changelog"),
                    _("Show package's changelog if available")],
                [self.config.get("shortcuts", "open_frugalbuild"),
                    _("Show package's FrugalBuild if available"),
                    self.repoman],
                [self.config.get("shortcuts", "merge_package"),
                    _("Build package if available"), self.repoman],
                [self.config.get("shortcuts", "show_screenshot"),
                    _("Show larger package's screenshot")],
                list(),
                [self.config.get("shortcuts", "mark_to_reinstall"),
                    _("Mark a package to reinstall")],
                [self.config.get("shortcuts", "apply_selection"),
                    _("Apply packages selection")],
                [self.config.get("shortcuts", "reset_selection"),
                    _("Reset packages selection")] ])


    def __generate_shortcuts_signals (self):
        """
        Generate shortcuts signals from user configuration
        """

        shortcuts = {
            self.entry_request:
                self.config.get("shortcuts", "focus_search_entry"),
            self.item_logs:
                self.config.get("shortcuts", "open_logs"),
            self.item_quit: "<Control>Q",
            self.item_preferences:
                self.config.get("shortcuts", "open_preferences"),
            self.item_repositories:
                self.config.get("shortcuts", "open_repositories"),
            self.item_upgrade:
                self.config.get("shortcuts", "show_upgrade"),
            self.button_reinstall:
                self.config.get("shortcuts", "mark_to_reinstall"),
            self.item_apply:
                self.config.get("shortcuts", "apply_selection"),
            self.item_reset:
                self.config.get("shortcuts", "reset_selection"),
            self.item_install:
                self.config.get("shortcuts", "install_local_fpm"),
            self.item_update:
                self.config.get("shortcuts", "update_databases"),
            self.item_cache:
                self.config.get("shortcuts", "clean_packages_cache"),
            self.item_chroot:
                self.config.get("shortcuts", "update_chroot"),
            self.item_shortcuts:
                self.config.get("shortcuts", "open_help"),
            self.button_files:
                self.config.get("shortcuts", "open_files_manager"),
            self.button_changelog:
                self.config.get("shortcuts", "open_changelog"),
            self.button_frugalbuild:
                self.config.get("shortcuts", "open_frugalbuild"),
            self.button_build:
                self.config.get("shortcuts", "merge_package"),
            self.button_zoom:
                self.config.get("shortcuts", "show_screenshot"),
            self.tool_offline:
                self.config.get("shortcuts", "change_network_status")}

        for widget in shortcuts.keys():
            key, mod = gtk.accelerator_parse(shortcuts[widget])

            if gtk.accelerator_valid(key, mod):
                if type(widget) == gtk.Entry:
                    widget.add_accelerator("grab_focus",
                        self.main_shortcuts, key, mod, gtk.ACCEL_MASK)
                else:
                    widget.add_accelerator("activate",
                        self.main_shortcuts, key, mod, gtk.ACCEL_VISIBLE)


    def __stop_threads (self):
        """
        Close any packages threads instance
        """

        if not self.__thread_packages == 0:
            source_remove(self.__thread_packages)


    def __refresh_interface (self, interface):
        """
        Refresh widgets size
        """

        size = self.paned_packages.allocation.width - 250

        # Update description label size
        self.label_description.set_size_request(size, -1)

    # ------------------------------------
    #   Interface functions
    # ------------------------------------

    def menu_hide_entries (self):
        """
        Hide fw32 and repoman entries when users not have them
        """

        self.filter_nobuild.hide()
        if self.repoman:
            self.filter_nobuild.show()

        self.item_chroot.hide()
        if self.__fw32:
            self.item_chroot.show()


    def filters_update (self, widget):
        """
        Reload packages filter when user change filters from menu
        """

        for repository in self.packages_data_widgets.keys():
            self.packages_data_widgets[repository].filter_list.refilter()


    def filters_match (self, model, row, data=None):
        """
        Update treeview filter
        """

        status = model.get_value(row, 10)

        if (self.filter_install.get_active() and status == "install") or \
            (self.filter_remove.get_active() and status == "remove") or \
            (self.filter_upgrade.get_active() and status == "upgrade") or \
            (self.filter_upgrade.get_active() and status == "urgent") or \
            (self.filter_downgrade.get_active() and status == "downgrade") or \
            (self.filter_nobuild.get_active() and status == "nobuild"):
            return True

        return False


    def notebook_update (self, notebook, page, index):
        """
        Unselect treeview when user change notebook page
        """

        self.__label_notebook = notebook.get_tab_label_text(
            notebook.get_nth_page(index))

        # Unselect current selected package
        self.treeview_unselect_row(
            self.packages_data_widgets[self.__label_notebook].treeview)

        self.statusbar_update()


    @property
    def notebook_tab_name (self):
        """
        Return label text from selected notebook tab
        """

        return str(self.notebook_packages.get_tab_label_text(
            self.notebook_packages.get_nth_page(
            self.notebook_packages.get_current_page())))


    def statusbar_update (self):
        """
        Change the statusbar
        """

        statusbar = list()

        # Get color from config file
        if self.config.get("colors", "use_colors_statubar"):
            color_remove = self.config.get("colors", "remove")
            color_install = self.config.get("colors", "install")

        # ------------------------------------
        #   Groups
        # ------------------------------------

        number = int()

        # Get length of current packages treeview
        if len(self.__label_notebook) > 0:
            number = len(
                self.packages_data_widgets[self.__label_notebook].store)

        # Group packages length
        if number > 0 and len(self.selected["group"]) > 0:

            group = self.selected["group"]
            if group == _("Search"):
                group = _("Found in %s" % self.__label_notebook)

            if number == 1:
                statusbar.append(_("%s: 1 package") % group)

            else:
                statusbar.append(_("%(group)s: %(len)d packages") % dict(
                    group=group, len=number ))

        # ------------------------------------
        #   Available updates
        # ------------------------------------

        if len(self.packages_data["upgrade"]) > 1:
            statusbar.append("<b>%s</b>" % \
                _("%d updates available") % len(self.packages_data["upgrade"]))

        elif len(self.packages_data["upgrade"]) == 1:
            statusbar.append("<b>%s</b>" % _("1 update available"))

        # ------------------------------------
        #   Packages selection
        # ------------------------------------

        # Packages to install
        number = int()

        if len(self.packages_data["installation"]) > 0:
            number += len(self.packages_data["installation"])

        if len(self.packages_data["reinstallation"]) > 0:
            number += len(self.packages_data["reinstallation"])

        text = str()
        if number > 1:
            text = _("%d packages to install") % number
        elif number == 1:
            text = _("1 package to install")

        if len(text) > 0:
            if self.config.get("colors", "use_colors_statubar"):
                statusbar.append("<span foreground='%s'>%s</span>" % (
                    color_install, text))
            else:
                statusbar.append(text)

        # Packages to delete
        number = len(self.packages_data["remove"])

        text = str()
        if number > 1:
            text = _("%d packages to remove") % (number)
        elif number == 1:
            text = _("1 package to remove")

        if len(text) > 0:
            if self.config.get("colors", "use_colors_statubar"):
                statusbar.append("<span foreground='%s'>%s</span>" % (
                    color_remove, text))
            else:
                statusbar.append(text)

        # ------------------------------------
        #   Installed packages
        # ------------------------------------

        if len(self.pacman.packages_installed()) == 1:
            statusbar.append(_("1 package installed"))

        elif len(self.pacman.packages_installed()) > 1:
            statusbar.append(_("%d packages installed") % \
                len(self.pacman.packages_installed()))

        # ------------------------------------
        #   Screenshots server
        # ------------------------------------

        if self.__website_checked:
            statusbar.append(_("Check screenshots website ..."))

        # Show informations into statusbar
        if len(statusbar) > 0:
            self.label_status.set_markup(" | ".join(statusbar))
            refresh()


    def reload_check_server (self):
        """
        Check screenshots server
        """

        if not self.__website_checked and \
            not self.config.get("network", "offline") and \
            self.config.get("network", "download"):

            self.__website_checked = True
            timeout_add(10000, self.__check_servers, WEBSITE, WEBSITE_FALLBACK)


    def reload_interface (self):
        """
        Reset all the interface
        """

        # Set status for specific programs
        self.__fw32 = False
        if exists("/usr/sbin/fw32-upgrade"):
            self.__fw32 = True

        self.repoman = False
        if exists("/usr/bin/repoman"):
            self.repoman = True

        # Change shortcuts with new user configuration file
        self.__generate_shortcuts()
        self.__generate_shortcuts_signals()

        # Reset repositories model
        model = self.combo_repositories.get_model()
        model.clear()
        self.combo_repositories.set_model(model)

        # Reset groups store
        self.store_groups.clear()

        # Remove old notebook tabs
        while not self.notebook_packages.get_n_pages() == 0:
            self.notebook_packages.remove_page(0)

        # Only add new repositories
        for repository in self.pacman.repositories()[1:]:
            if not str(repository) in self.packages_data_widgets:
                self.packages_data_widgets[str(repository)] = \
                    PackagesList(self, str(repository))

        # Reset informations
        self.label_name.set_text(str())
        self.label_description.set_text(str())
        self.label_website.set_text(str())

        # Add some widgets
        for widget in self.box_buttons.get_children():
            widget.hide()
            widget.set_sensitive(False)

        self.button_zoom.hide()
        self.button_zoom.set_sensitive(False)

        # Clear package informations icons
        self.store_details.clear()
        self.image_package.clear()
        self.image_screenshot.clear()
        self.image_cache.clear()

        # Reset values
        self.selected["group"] = str()
        self.selected["search"] = str()

        self.__length_packages = int()

        self.colorschemes = list()

        # Reload pacman configuration instance
        self.pacmanConfig = PacmanConfig()

        # Check screenshots server
        self.reload_check_server()

        # Hide menu entries
        self.menu_hide_entries()

        # Reset packages list
        self.selection_reset()

        # Restore repositories
        self.repositories_add()

        # Show repositories combobox when there are 2 repositories or more
        if len(self.combo_repositories.get_model()) > 1:
            for widget in self.box_repositories.get_children():
                widget.show()

        self.statusbar_update()


    def selection_reset (self, *args):
        """
        Reset user selection and restore update list
        """

        if len(self.packages_data["installation"]) > 0 or \
            len(self.packages_data["remove"]) > 0 or \
            len(self.packages_data["reinstallation"]) > 0:

            # Restore a true copy of self.packages_data["saved"]
            self.packages_data["upgrade"] = deepcopy(self.packages_data["saved"])

            # Remove any modifications into packages treeview
            if len(self.__model_saved) > 0:

                # Get only visible repositories
                for repository in self.treeview_data:
                    self.treeview_reset_values(repository)

            self.packages_data["installation"] = list()
            self.packages_data["reinstallation"] = list()
            self.packages_data["remove"] = list()

            # Restore default value for Reinstall button
            self.label_reinstall.set_text(_("Reinstall this package"))
            self.button_reinstall.set_tooltip_text(_("Reinstall this package"))
            self.image_reinstall.set_from_pixbuf(self.icon_to_install)

            self.__not_check_dependencies = True

            self.statusbar_update()


    def treeview_reset_values (self, repository):
        """
        Restore default values for packages into treeview
        """

        # Get user custom colors
        color_upgrade = self.config.get("colors", "upgrade")
        color_downgrade = self.config.get("colors", "downgrade")

        model = self.packages_data_widgets[repository].store
        packages = self.treeview_data[repository]

        # Manual adding
        for name in self.__model_saved[repository]:

            if name in packages:
                row = model[packages[name]]

                # Installation/Deletion
                if not name in self.packages_data["reinstallation"]:

                    if row[10] == "downgrade":
                        row[0] = True
                        row[1] = self.icon_downgrade
                        row[6] = color_downgrade
                        row[7] = True

                        if self.config.get("colors", "use_bold"):
                            row[9] = WEIGHT_BOLD

                    else:
                        if row[10] == "install":
                            row[0] = True
                            row[1] = self.icon_installed

                        else:
                            row[0] = False
                            row[1] = self.icon_available

                        if name in self.packages_data["remove"]:
                            self.packages_data["remove"].remove(name)

                        elif name in self.packages_data["installation"]:
                            self.packages_data["installation"].remove(name)

                        row[7] = False
                        row[9] = WEIGHT_NORMAL

                # Reinstallation
                else:
                    if row[10] == "downgrade":
                        row[0] = True
                        row[1] = self.icon_downgrade
                        row[6] = color_downgrade
                        row[7] = True

                        if self.config.get("colors", "use_bold"):
                            row[9] = WEIGHT_BOLD

                    else:
                        row[0] = True
                        row[1] = self.icon_installed
                        row[7] = False
                        row[9] = WEIGHT_NORMAL

                    self.packages_data["reinstallation"].remove(name)

        # Update adding
        for package in self.packages_data["upgrade"].keys():

            if package in packages:
                row = model[packages[package]]

                row[0] = True
                row[6] = color_upgrade
                row[7] = True

                if row[10] == "upgrade":
                    row[1] = self.icon_upgrade
                elif row[10] == "urgent":
                    row[1] = self.icon_upgrade_urgent

                if self.config.get("colors", "use_bold"):
                    row[9] = WEIGHT_BOLD

        self.__model_saved[repository] = list()


    def packages_search (self, *args):
        """
        Search all the package corresponding to self.entry_request and show them
        in self.mainPackagesList
        """

        listSearch = dict()

        # Do a search if string is not null or not already done
        if len(self.entry_request.get_text()) > 0 and \
            self.entry_request.get_text() != self.selected["search"]:

            pkgSearch = self.entry_request.get_text()

            # Avoid to search in local repository (installed packages)
            for repository in self.pacman.repositories()[1:]:
                listSearch[str(repository)] = \
                    self.pacman.packages_search(str(repository), pkgSearch)

                # Remove useless repositories
                if len(listSearch[str(repository)]) == 0:
                    del listSearch[str(repository)]

            if len(listSearch) > 0:
                self.statusbar_update()

            self.__search_status = True
            self.selected["search"] = pkgSearch

            self.selected["group"] = _("Search")

            if len(listSearch) > 0:

                # Add current search to completion list
                if not pkgSearch in self.modelSearch:
                    self.modelSearch.append([pkgSearch])

                LOG.debug(_("%s string has been searched") % pkgSearch)

                # Fill the treeview during idle cycles
                self.__stop_threads()

                loader = self.packages_fill(listSearch)
                self.__thread_packages = idle_add(loader.next)

            else:
                WindowPopup(_("Packages search"), _("No package has been "
                    "found for %s expression") % pkgSearch)

            # Unselect current selected group
            self.treeview_unselect_row(self.treeview_groups)


    def search_completion (self, widget, model, row):
        """
        Select a string from completion popup
        """

        self.entry_request.set_text(model[row][0])


    def search_activate (self, widget, pos, event):
        """
        Erase search text
        """

        # Search package
        if pos == 0:
            self.packages_search()

        # Reset search entry
        elif pos == 1:
            self.entry_request.set_text("")


    def treeview_unselect_row (self, treeview):
        """
        Unselect the actual selection from specific treeview
        """

        if not treeview.get_selection().get_selected()[1] == None:
            treeview.get_selection().unselect_path(treeview.get_cursor()[0])


    def treeview_checked_row (self, widget, path, model):
        """
        Update selected package combobox in treeview
        """

        model[path][0] = not model[path][0]


    def tooltips_description (self, widget, x, y, keyboard, tooltip):
        """
        Show package description in a tooltip
        """

        try:
            position = widget.get_dest_row_at_pos(int(x), int(y))
            if not position == None:

                # Update tooltip position
                if self.__tooltip_packages is not None and \
                    not self.__tooltip_packages == position:
                    self.__tooltip_packages = position
                    return False

                model = widget.get_model()

                pkgName = model.get_value(model.get_iter(position[0]), 2)
                pkgVersion = model.get_value(model.get_iter(position[0]), 3)

                if len(pkgVersion) > 0:

                    # Get current repo index
                    pkgRepo = self.pacman.repositories(
                        self.combo_repositories.get_active_text())

                    # Get current repo index
                    if self.__search_status:
                        pkgRepo = self.pacman.repositories(
                            self.notebook_tab_name)

                    pkgStatus, pkgInfo, pkgGroup, noBuild = \
                        self.pacman.package(pkgRepo, pkgName)

                    if pkgInfo.get("description") is not None:
                        tooltip.set_icon(
                            self.icon_load(pkgName, 24, self.icon_installed))
                        tooltip.set_text(pkgInfo.get("description"))

                        self.__tooltip_packages = position

                        return True

            return False

        except:
            return False


    @staticmethod
    def refresh ():
        """
        Refresh the interface
        """

        while gtk.events_pending():
            gtk.main_iteration()


    # ------------------------------------
    #   Update databases
    # ------------------------------------

    def configuration_lastupdate (self):
        """
        Update last_update value from configuration file
        """

        self.config.set("packages", "last_update",
            datetime.today().strftime("%Y%m%d%H%M%S"))

        self.config.write_configuration()


    def databases_check_lastupdate (self):
        """
        Check if the last_update value is older than the OldDelay value
        """

        last_update = self.config.get("packages", "last_update")

        if last_update == None or \
            (type(last_update) == str and len(last_update) == 0):
            return True

        old_delay = self.pacmanConfig.option("OldDelay").value
        if old_delay == None:
            old_delay = 3

        today_time = int(datetime.today().strftime("%Y%m%d%H%M%S"))

        # Repositories need to be update
        if today_time - last_update > (old_delay * pow(10, 6)):
            return True

        return False


    def databases_update (self, widget=None):
        """
        Update pacman-g2 databases and if repoman is available, update
        repoman database
        """

        # Stop packages loading thread
        self.__stop_threads()

        action = Command()
        action.upgrade()

        flags = dict(repoman=self.repoman)

        if action.generate(flags):
            scriptFile = createScript(CONFIGPATH, action.command)

            terminal = WindowVte(self, _("Update pacman-g2 databases"),
                _("Using <b>%s</b> as updater script") % scriptFile,
                "sh %s" % scriptFile, True)

            remove(scriptFile)

            if terminal.success:
                self.configuration_lastupdate()
                self.pacman.reload_daemon()
                self.reload_interface()
                self.upgrade_get(True)

        self.statusbar_update()


    def chroot_update (self, widget=None):
        """
        Update Fw32 chroot
        """

        if self.__fw32:
            action = Command()
            action.chroot()

            if action.generate():
                termVte = WindowVte(_("Update 32bits environment"),
                    _("Update 32bits environment"), action.command, True)

                if termVte.succes:
                    WindowPopup(_("Update 32bits environment"),
                        _("The 32bits environment has been updated"))

            else:
                WindowPopup(_("Error during command generation"),
                    _("Can't generate command for %s") % "chroot_update",
                    gtk.MESSAGE_ERROR)

        self.statusbar_update()


    def upgrade_get (self, resetList=False):
        """
        Get available packages upgrade
        """

        if resetList:
            self.packages_data["upgrade"] = dict()
            self.packages_data["saved"] = dict()

        # Get update packages list
        listePaquetsMiseAJour = self.pacman.packages_upgrade()

        # Some packages are available
        if len(listePaquetsMiseAJour) > 0:

            # Add package to cache list
            for status, name in listePaquetsMiseAJour:
                if not str(status) in self.packages_data["installation"] and \
                    not str(status) in self.packages_data["reinstallation"]:
                    self.packages_data["upgrade"][str(status)] = str(name)

            listSize = len(self.packages_data["upgrade"])

            # More than one package
            if listSize > 1:
                LOG.info(_("%d update has been found") % listSize)

                self.notification_show(_("pyFPM"),
                    _("%d update has been found") % listSize)

                # Get a true copy of self.packages_data["upgrade"]
                self.packages_data["saved"] = deepcopy(
                    self.packages_data["upgrade"])

            # Only one package
            elif listSize == 1:
                LOG.info(_("One update has been found"))

                self.notification_show(_("pyFPM"),
                    _("One update has been found"))

                # Get a true copy of self.packages_data["upgrade"]
                self.packages_data["saved"] = deepcopy(
                    self.packages_data["upgrade"])

            else:
                LOG.info(_("No update available"))

        else:
            LOG.info(_("No update available"))


    # ------------------------------------
    #   Repositories
    # ------------------------------------

    def repositories_add (self):
        """
        Add pacman-g2 repositories into self.combo_repositories
        """

        # Get repositories list
        repositories = self.pacman.repositories()[1:]
        LOG.info(_("%d repos has been found") % len(repositories))

        LOG.debug(str(", ".join(repositories)))

        # Add repositories into combobox
        for element in repositories:
            self.__model_saved[str(element)] = list()
            self.combo_repositories.append_text(element)

        # Set system repository to default value
        self.combo_repositories.set_active(self.pacman.repositories_index() - 1)


    def repositories_select (self, *args):
        """
        When the user change actual repository, fpm-gui need to update groups
        list
        """

        self.store_groups.clear()

        self.groups_add(self.combo_repositories.get_active_text())

        self.statusbar_update()


    # ------------------------------------
    #   Groups
    # ------------------------------------

    def treeview_separator (self, model, treeiter, column=1):
        """
        Check if a row is a separator in treeview
        """

        if model.get_value(treeiter, column) == "---":
            return True

        return False


    def groups_add (self, repository):
        """
        Add repository groups into self.store_groups
        """

        self.__sub_groups = dict()

        if repository is not None:

            # Get pacman-g2 groups list
            pacmanGroups = self.pacman.groups(repository)

            # Add categories to groups treeview
            for group in self.__groups_data:
                icon = self.__groups_data[group][0]
                regex = self.__groups_data[group][1]

                row = self.store_groups.append(None,
                    [self.icon_load(icon, 32, self.icon_group), group])

                self.__sub_groups[group] = list()

                # Get remaining sub-groups
                currentGroups = deepcopy(pacmanGroups)

                groupsNumber = 0

                # Add sub-groups to current category
                for element in currentGroups:

                    # Check if sub-group is link to current category
                    if match(regex, element) is not None:

                        actualRow = self.store_groups.append(row,
                            [self.icon_package, element])

                        self.__sub_groups[group].append(str(element))

                        # Remove current sub-group
                        pacmanGroups.remove(element)

                        groupsNumber += 1

                # Remove category when there is not sub-groups
                if groupsNumber == 0:
                    self.store_groups.remove(row)
                # Remove sub-group when category has only one of them
                if groupsNumber == 1:
                    self.store_groups.remove(actualRow)

            # Add lastest groups to Misc category
            if len(pacmanGroups) > 0:
                row = self.store_groups.append(None,
                    [self.icon_group, _("Others")])

                self.__sub_groups[_("Others")] = list()

                # Not used sub-groups
                for element in pacmanGroups:
                    self.store_groups.append(row, [self.icon_package, element])

                    self.__sub_groups[_("Others")].append(str(element))

            LOG.info(_("%(length)d group(s) append for %(repo)s") % dict(
                length=len(self.store_groups), repo=repository ))


    def groups_select (self, treeview, event):
        """
        Action to do when user click on group
        """

        expandRow = False
        showPackages = False

        repoName = self.combo_repositories.get_active_text()

        # Keyboard
        if event.type == gtk.gdk.KEY_RELEASE:
            selection = treeview.get_selection().get_selected()
            groupIter = selection[1]

            if not groupIter == None:

                model = treeview.get_model()
                iterDepth = self.store_groups.iter_depth(groupIter)
                groupName = model.get_value(groupIter, 1)

                # Return - Load packages from selected group
                if gtk.gdk.keyval_name(event.keyval) == "Return":

                    # Avoid to load packages if already done
                    if not self.selected["group"] == groupName:
                        showPackages = True

                # Space - Show/hide sub-group | Check/uncheck sub-group
                # But why it's "space" and not "Space" oO
                elif gtk.gdk.keyval_name(event.keyval) == "space":
                    if iterDepth == 0:
                        path = model.get_path(groupIter)
                        expandRow = True

        # Mouse
        elif event.type == gtk.gdk.BUTTON_PRESS:

            # Left click - Load packages from selected group
            if event.button == 1:

                # Current selection
                selection = treeview.get_path_at_pos(
                    int(event.x), int(event.y))

                if not selection == None:

                    model = treeview.get_model()
                    path = selection[0]

                    groupIter = model.get_iter(path)
                    groupName = model.get_value(groupIter, 1)

                    treeview.set_cursor(path, None, False)

                    iterDepth = len(path) - 1

                    # Expand/collapse sub-groups when user double-click
                    if event.type == gtk.gdk._2BUTTON_PRESS:
                        if iterDepth == 0:
                            expandRow = True

                    # Avoid to load packages if already done
                    if not self.selected["repository"] == repoName:
                        showPackages = True
                    else:
                        if not self.selected["group"] == groupName:
                            showPackages = True


        # Expand/collapse sub-groups
        if expandRow:
            if not treeview.row_expanded(path):
                treeview.expand_row(path, True)
            else:
                treeview.collapse_row(path)

        # Show packages into packages treeview
        if showPackages:

            # Reset search mode
            self.__search_status = False
            self.selected["search"] = str()

            self.selected["repository"] = repoName
            self.selected["group"] = groupName

            # Category
            if iterDepth == 0:

                if groupName in self.__sub_groups.keys():
                    self.packages_get(
                        groupName, self.__sub_groups[groupName])

            # Sub-group
            else:
                self.packages_get(groupName)


    # ------------------------------------
    #   Packages
    # ------------------------------------

    def packages_get (self, groupName, groupList=None):
        """
        Get all the group packages and run packages fill function
        """

        packages_list = dict()

        if groupList is None:
            groupList = groupName

        repositoryName = self.combo_repositories.get_active_text()
        repositoryIndex = self.pacman.repositories(repositoryName)

        packages_list[repositoryName] = list()

        # Category
        if groupList is not None and type(groupList) == list:

            for group in groupList:

                # Get packages from sub-groups
                packages_list[repositoryName].extend(
                    self.pacman.packages(repositoryName, group))

                # Get nobuild from sub-group
                if exists(REPOMAN_BIN):
                    packages_list[repositoryName].extend(
                        self.pacman.nobuild(repositoryName).get(group, list()))

        # Sub-group
        else:
            # Get packages from sub-groups
            packages_list[repositoryName].extend(
                self.pacman.packages(repositoryName, groupName))

            # Get nobuild from sub-group
            if exists(REPOMAN_BIN):
                packages_list[repositoryName].extend(
                    self.pacman.nobuild(repositoryName).get(groupName, list()))

        packages_list[repositoryName].sort()

        # Fill the treeview during idle cycles
        if not self.__thread_packages == 0:
            source_remove(self.__thread_packages)

        loader = self.packages_fill(packages_list)
        self.__thread_packages = idle_add(loader.next)


    def packages_fill (self, packages_list):
        """
        Add the packages into self.mainPackagesList
        """

        refresh()

        # ------------------------------------
        #   Variables
        # ------------------------------------

        iteration = int()

        status = False

        notebook_pages = dict()
        treeview_models = dict()
        repositories_packages = dict()

        self.__label_notebook = str()

        # ------------------------------------
        #   Thread
        # ------------------------------------

        # Get current id of packages loading
        current_thread_id = self.__thread_packages

        # ------------------------------------
        #   Manage packages notebook
        # ------------------------------------

        # Remove notebook pages
        while not self.notebook_packages.get_n_pages() == 0:
            self.notebook_packages.remove_page(-1)

        # Show tabs
        if not self.debug and len(packages_list.keys()) <= 1:
            self.notebook_packages.set_show_tabs(False)
            self.notebook_packages.set_show_border(False)
        else:
            self.notebook_packages.set_show_tabs(True)
            self.notebook_packages.set_show_border(True)

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Colors
        color_remove = self.config.get("colors", "remove")
        color_install = self.config.get("colors", "install")
        color_upgrade = self.config.get("colors", "upgrade")
        color_downgrade = self.config.get("colors", "downgrade")

        # Change mouse cursor
        self.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH))

        # Update statubar informations
        self.statusbar_update()

        # ------------------------------------
        #   Load repositories
        # ------------------------------------

        for repository in packages_list.keys():
            repositories_packages[repository] = list()

            # Set active page to False
            notebook_pages[repository] = False

            widget = self.packages_data_widgets[repository]

            # Set treeview properties
            widget.treeview.set_search_column(2)
            widget.treeview.set_headers_visible(True)
            widget.treeview.set_enable_search(False)
            widget.treeview.show()

            # Treeview model
            widget.store.clear()
            treeview_models[repository] = widget.store

            # Set scrollbar properties
            widget.show()

            # Create a new pad for current repository
            label = gtk.Label(repository)

            # Oficial frugalware repository put in first tab
            if repository == self.pacman.repository_official():
                self.notebook_packages.insert_page(widget, label, 0)
                self.notebook_packages.set_current_page(0)
            else:
                self.notebook_packages.append_page(widget, label)

            # Update widget design
            if self.debug or len(packages_list.keys()) > 1:
                widget.set_border_width(4)
            else:
                widget.set_border_width(0)

            # TreeIter list
            self.treeview_data[repository] = dict()

            # ------------------------------------
            #   Refresh treeview
            # ------------------------------------

            widget.filter_list.refilter()

            refresh()
            widget.treeview.freeze_child_notify()

            yield True

            # ------------------------------------
            #   Add packages into interface
            # ------------------------------------

            # Remove duplicate entries (Come from sub-groups)
            sorted_packages_list = list(set(packages_list[repository]))
            sorted_packages_list.sort()

            for name in sorted_packages_list:

                # Another thread has been called by user, close this one
                if not current_thread_id == self.__thread_packages:
                    yield False

                # ------------------------------------
                #   Default variables
                # ------------------------------------

                fg_color = WHITE
                use_color = False
                sensitive = True

                # ------------------------------------
                #   Get informations
                # ------------------------------------

                data = self.pacman.package(repository, name)

                # Get package status and installed version (if available)
                status, version = self.pacman.package_installed(data.name)

                # Installed package
                if status:

                    # Available update
                    if data.name in self.packages_data["upgrade"].keys():
                        fg_color = color_upgrade
                        use_color = True

                        local = version
                        version = self.packages_data["upgrade"][data.name]

                        if "base" in data.groups:
                            image = self.icon_upgrade_urgent
                            description = "urgent"

                        else:
                            image = self.icon_upgrade
                            description = "upgrade"

                    # Higher version
                    elif data.version < version:
                        fg_color = color_downgrade
                        use_color = True

                        image = self.icon_downgrade
                        local = version
                        version = data.version
                        description = "downgrade"

                    # Normal package
                    else:
                        image = self.icon_installed
                        local = data.version
                        description = "install"

                # Not installed package
                else:
                    status = False
                    image = self.icon_available
                    local = str()
                    version = data.version
                    description = "remove"


                # Package is in self.packages_data["installation"]
                if data.name in self.packages_data["installation"]:
                    status = True

                    # Current package has an update available
                    if not self.packages_data["saved"].__contains__(
                        (data.name, data.version)):

                        image = self.icon_to_install
                        fg_color = color_install
                        use_color = True

                # Package is in self.packages_data["remove"]
                elif data.name in self.packages_data["remove"]:
                    status = False
                    image = self.icon_to_remove
                    fg_color = color_remove
                    use_color = True

                # Package is in self.packages_data["reinstallation"]
                elif data.name in self.packages_data["reinstallation"]:
                    status = True
                    image = self.icon_to_install
                    fg_color = color_install
                    use_color = True

                # Nobuild package not installed
                if data.nobuild and not data.installed:
                    sensitive = False
                    image = self.icon_nobuild

                    local = str()
                    version = data.version
                    description = "nobuild"

                # ------------------------------------
                #   Add package into treeview
                # ------------------------------------

                if data.name is not None and \
                    not data.name in repositories_packages[repository]:

                    # Set weight format for modified packages
                    bold = WEIGHT_NORMAL
                    if use_color and self.config.get("colors", "use_bold"):
                        bold = WEIGHT_BOLD

                    if data.groups is not None:
                        groups = ','.join(data.groups)

                    else:
                        LOG.warning(
                            _("No groups has been founded for %s") % data.name)
                        groups = str()

                    # Add current package into liststore
                    row = widget.store.append([status, image, data.name,
                        local, version, groups, fg_color, use_color, sensitive,
                        bold, description])

                    repositories_packages[repository].append(data.name)

                    # Get row position for each packages name
                    self.treeview_data[repository][data.name] = row

                iteration += 1
                if (iteration % 5 == 0):
                    self.statusbar_update()

                    widget.treeview.thaw_child_notify()
                    yield True
                    widget.treeview.freeze_child_notify()

            # Restore options for packages treeviews
            widget.treeview.set_enable_search(True)
            widget.treeview.thaw_child_notify()

        self.window.set_cursor(None)

        self.statusbar_update()

        # ------------------------------------
        #   Close thread
        # ------------------------------------

        self.__thread_packages = 0

        yield False


    def packages_select (self, treeview, event):
        """
        When the user select a package, fpm-gui need to get informations about
        this package
        """

        packageMenu = False
        changeStatus = False
        showInformations = False

        # Keyboard
        if event.type == gtk.gdk.KEY_RELEASE:
            selection = treeview.get_selection().get_selected()

            packageIter = selection[1]

            if packageIter is not None:

                model = treeview.get_model()

                path = model.get_path(packageIter)
                packageName = model.get_value(packageIter, 2)

                # Return - Load informations from selected package
                if gtk.gdk.keyval_name(event.keyval) == "Return":

                    # Avoid to load informations if already done
                    if not self.selected["package_name"] == packageName:
                        showInformations = True


                # Space - Check/uncheck package
                # But why it's "space" and not "Space" oO
                elif gtk.gdk.keyval_name(event.keyval) == "space":
                    changeStatus = True


                # Menu - Show package menu
                elif gtk.gdk.keyval_name(event.keyval) == "Menu":
                    button = event.keyval
                    packageMenu = True

        # Mouse
        elif event.type == gtk.gdk.BUTTON_PRESS:

            # Current selection
            selection = treeview.get_path_at_pos(
                int(event.x), int(event.y))

            if not selection == None:

                model = treeview.get_model()

                path = selection[0]
                packageIter = model.get_iter(path)
                packageName = model.get_value(packageIter, 2)

                # Left click - Load packages from selected group
                if event.button == 1:
                    treeview.set_cursor(path, None, False)

                    iterDepth = len(path) - 1

                    # Check/uncheck package when user double-click
                    if event.type == gtk.gdk._2BUTTON_PRESS:
                        changeStatus = True

                    # Avoid to load packages if already done
                    if not self.selected["package_name"] == packageName:
                        showInformations = True


                # Right click - Show a popup menu
                if event.button == 3:
                    button = event.button
                    packageMenu = True


        # Change checkbox status
        if changeStatus:
            self.window_missing_dependancies(None, None, treeview)

        # Show packages into packages treeview
        if showInformations:
            self.package(model, packageIter)

        # Show package menu
        if packageMenu:
            self.item_to_build.hide()
            self.item_to_reinstall.hide()
            self.item_separator.hide()

            packageStatus = model.get_value(packageIter, 0)
            packageName = model.get_value(packageIter, 2)
            packageVersion = model.get_value(packageIter, 3)

            nobuildStatus = False

            # Check if selected package is a nobuild
            if self.repoman:
                if packageName in self.nobuild_packages():
                    nobuildStatus = True

            # Nobuild package
            if nobuildStatus:
                self.item_to_build.show()
                self.item_separator.show()

            # Installed package
            if self.pacman.package_installed(packageName)[0]:
                if not nobuildStatus:
                    self.item_to_reinstall.show()
                    self.item_separator.show()

                elif self.pacman.package_fpm(packageName, packageVersion):
                    self.item_to_reinstall.show()
                    self.item_separator.show()

            # Resinstallation button
            if not packageName in self.packages_data["reinstallation"]:
                self.item_to_reinstall.set_label(_("Reinstall this package"))
            else:
                self.item_to_reinstall.set_label(_("Cancel reinstallation"))

            self.selected["menu"] = packageName

            self.menu_package.popup(None, None,
                self.packages_menu_position, button, event.time, event)


    def packages_menu_position (self, menu, event):
        """
        Set the package menu position correctly when it's called
        """

        if event.type == gtk.gdk.BUTTON_PRESS:
            return int(event.x_root), int(event.y_root), 1

        # Get repository name from noteboob tab label
        repositoryName = self.notebook_tab_name
        treeview = self.packages_data_widgets[repositoryName].treeview

        selection = treeview.get_selection().get_selected()
        packageIter = selection[1]

        if not packageIter == None:
            path = treeview.get_model().get_path(packageIter)

            (x, y) = self.get_position()

            area_treeview = treeview.get_allocation()
            aera_selection = treeview.get_background_area(
                path, treeview.get_column(2))

            x += area_treeview.x + 15
            y += area_treeview.y + aera_selection.y + (
                aera_selection.height * 2)

            return x, y, 1


    def menu_mark_packages (self, widget, allPackages=True):
        """
        Change treeview data for a specific menu entry
        """

        colorRemove = self.config.get("colors", "remove")
        colorInstall = self.config.get("colors", "install")
        colorUpgrade = self.config.get("colors", "upgrade")

        repository = self.notebook_tab_name

        saveModel = self.__model_saved[repository]
        itersList = self.treeview_data[repository]

        model = self.packages_data_widgets[repository].store

        for package in itersList:

            updatable = False

            data = model[itersList[package]]

            status = data[0]
            icon = data[1]
            name = data[2]
            installed_version = data[3]
            repository_version = data[4]

            if data[8]:

                # ------------------------------------
                #   To install packages
                # ------------------------------------

                if widget in [self.item_to_install, self.item_to_all]:

                    if not self.pacman.package_installed(package)[0]:

                        if not package in self.packages_data["installation"]:
                            self.packages_data["installation"].append(package)
                            updatable = True

                    elif allPackages:

                        if not package in self.packages_data["reinstallation"]:
                            self.packages_data["reinstallation"].append(package)
                            updatable = True

                    if updatable:
                        if package in self.packages_data["remove"]:
                            self.packages_data["remove"].remove(package)

                    status = True
                    icon = self.icon_to_install
                    color = colorInstall
                    use = True

                    bold = WEIGHT_NORMAL
                    if self.config.get("colors", "use_bold"):
                        bold = WEIGHT_BOLD

                # ------------------------------------
                #   To remove packages
                # ------------------------------------

                elif widget == self.item_to_remove:

                    if self.pacman.package_installed(package)[0]:

                        if not package in self.packages_data["remove"]:
                            self.packages_data["remove"].append(package)

                        if package in self.packages_data["installation"]:
                            self.packages_data["installation"].remove(package)
                        elif package in self.packages_data["reinstallation"]:
                            self.packages_data["reinstallation"].remove(package)

                        updatable = True

                        icon = self.icon_to_remove
                        color = colorRemove
                        use = True

                        bold = WEIGHT_NORMAL
                        if self.config.get("colors", "use_bold"):
                            bold = WEIGHT_BOLD

                    else:
                        updatable = True

                        color = data[6]
                        use = data[7]
                        bold = data[9]

                # ------------------------------------
                #   Update data in treeview
                # ------------------------------------

                if updatable:
                    if not package in saveModel:
                        saveModel.append(package)

                    self.treeview_update_from_data(dict(
                        status=status,
                        icon=icon,
                        name=name,
                        install=installed_version,
                        repository=repository_version,
                        group="",
                        color=color,
                        use=use,
                        bold=bold))

        self.__not_check_dependencies = False

        self.statusbar_update()


    def menu_mark_to_reinstall (self, widget):
        """
        Change package status to reinstall it or not
        """

        colorInstall = self.config.get("colors", "install")
        colorUpgrade = self.config.get("colors", "upgrade")

        pkgName = self.selected["menu"]

        # Get repository name from noteboob tab label
        repositoryName = self.notebook_tab_name

        saveModel = self.__model_saved[repositoryName]
        repositoryPackagesIter = self.treeview_data[repositoryName]

        model = self.packages_data_widgets[repositoryName].store

        index = repositoryPackagesIter[pkgName]

        pkgStatus = True
        pkgIcon = model[index][1]
        pkgName = model[index][2]
        pkgInstalledVersion = model[index][3]
        pkgRepositoryVersion = model[index][4]

        # Marked to reinstall
        if pkgName in self.packages_data["reinstallation"]:

            self.packages_data["reinstallation"].remove(pkgName)

            if pkgName == self.selected["package_name"]:
                self.label_reinstall.set_text(
                    _("Reinstall this package"))
                self.button_reinstall.set_tooltip_text(
                    _("Reinstall this package"))
                self.image_reinstall.set_from_pixbuf(
                    self.icon_to_install)

            if pkgName in saveModel:
                saveModel.remove(pkgName)

            if pkgName in self.packages_data["upgrade"].keys():
                pkgIcon = self.icon_upgrade
                fgColor = colorUpgrade
                useColor = True

                bold = WEIGHT_NORMAL
                if self.config.get("colors", "use_bold"):
                    bold = WEIGHT_BOLD

            else:
                pkgIcon = self.icon_installed
                fgColor = WHITE
                useColor = False
                bold = WEIGHT_NORMAL

        # Cancel reinstall
        else:

            if pkgName in self.packages_data["remove"]:
                self.packages_data["remove"].remove(pkgName)

            self.packages_data["reinstallation"].append(pkgName)

            if pkgName == self.selected["package_name"]:
                self.label_reinstall.set_text(
                    _("Cancel reinstallation"))
                self.button_reinstall.set_tooltip_text(
                    _("Cancel reinstallation"))
                self.image_reinstall.set_from_pixbuf(
                    self.icon_to_remove)

            if not pkgName in saveModel:
                saveModel.append(pkgName)

            pkgIcon = self.icon_to_install
            fgColor = colorInstall
            useColor = True

            bold = WEIGHT_NORMAL
            if self.config.get("colors", "use_bold"):
                bold = WEIGHT_BOLD

        # Create a temp dictionnary to store package data
        data = dict(status=pkgStatus, icon=pkgIcon, name=pkgName,
            install=pkgInstalledVersion, repository=pkgRepositoryVersion,
            group="", color=fgColor, use=useColor, bold=bold)

        # Get modified row index
        self.treeview_update_from_data(data)

        self.__not_check_dependencies = False

        self.statusbar_update()


    def treeview_update_rows (self, model, path):
        """
        Update the selected package checkbox
        """

        # Get user custom colors
        colorPurge = self.config.get("colors", "remove")
        colorInstall = self.config.get("colors", "install")
        colorUpgrade = self.config.get("colors", "upgrade")
        colorDowngrade = self.config.get("colors", "downgrade")

        # Default colors
        fgColor = WHITE
        useColor = False

        # Check if actual selection is not a nobuild package
        if model[path][8]:

            # Get repository name from noteboob tab label
            repository = self.notebook_tab_name

            saveModel = self.__model_saved[repository]

            pkgStatus = model[path][0]
            pkgIcon = model[path][1]
            pkgName = model[path][2]
            pkgInstalledVersion = model[path][3]
            pkgRepositoryVersion = model[path][4]
            pkgDescription = model[path][10]

            # Inverse checkbox value
            pkgStatus = not pkgStatus

            # If checkbox is True
            if pkgStatus:

                # Installed package so cancel deletion
                if self.pacman.package_installed(pkgName)[0]:

                    if pkgDescription == "upgrade":
                        useColor = True
                        fgColor = colorUpgrade
                        pkgIcon = self.icon_upgrade

                    elif pkgDescription == "urgent":
                        useColor = True
                        fgColor = colorUpgrade
                        pkgIcon = self.icon_upgrade_urgent

                    elif pkgDescription == "downgrade":
                        useColor = True
                        fgColor = colorDowngrade
                        pkgIcon = self.icon_downgrade

                    else:
                        pkgIcon = self.icon_installed

                    # Remove package from deletion list
                    if pkgName in self.packages_data["remove"]:
                        self.packages_data["remove"].remove(pkgName)

                    if pkgName in saveModel:
                        del saveModel[saveModel.index(pkgName)]

                # Not installed package so add it to installation list
                else:
                    pkgIcon = self.icon_to_install
                    fgColor = colorInstall
                    useColor = True

                    # Add package to installation list
                    if not pkgName in self.packages_data["installation"]:
                        self.packages_data["installation"].append(pkgName)

                    if not pkgName in saveModel:
                        saveModel.append(pkgName)

            # If Checkbox is False
            else:

                # Installed package so add it to deletion list
                if self.pacman.package_installed(pkgName)[0]:
                    pkgIcon = self.icon_to_remove
                    fgColor = colorPurge
                    useColor = True

                    if pkgName in self.packages_data["reinstallation"]:
                        self.packages_data["reinstallation"].remove(pkgName)

                        self.label_reinstall.set_text(
                            _("Reinstall this package"))
                        self.button_reinstall.set_tooltip_text(
                            _("Reinstall this package"))
                        self.image_reinstall.set_from_pixbuf(
                            self.icon_to_install)

                    # Add package to deletion list
                    if not pkgName in self.packages_data["remove"]:
                        self.packages_data["remove"].append(pkgName)

                    if not pkgName in saveModel:
                        saveModel.append(pkgName)

                # Not installed package so cancel installation
                else:
                    pkgIcon = self.icon_available

                    # Remove package from installation list
                    if pkgName in self.packages_data["installation"]:
                        self.packages_data["installation"].remove(pkgName)

                    if pkgName in saveModel:
                        del saveModel[saveModel.index(pkgName)]


            # Set bold weight
            bold = WEIGHT_NORMAL
            if useColor and self.config.get("colors", "use_bold"):
                bold = WEIGHT_BOLD

            # Create a temp dictionnary to store package data
            data = dict(status=pkgStatus, icon=pkgIcon, name=pkgName,
                color=fgColor, use=useColor, bold=bold)

            # Get modified row index
            self.treeview_update_from_data(data)

            self.statusbar_update()


    def treeview_update_from_data (self, data):
        """
        Update treeview rows when a change occur
        """

        pkgName = data.get("name")

        for repository in self.treeview_data:
            if pkgName in self.treeview_data[repository]:

                # Update models
                model = self.packages_data_widgets[repository].store
                path = self.treeview_data[repository][pkgName]

                model[path][0] = data.get("status")
                model[path][1] = data.get("icon")
                model[path][6] = data.get("color")
                model[path][7] = data.get("use")
                model[path][9] = data.get("bold")


    # ------------------------------------
    #   Package informations
    # ------------------------------------

    def package (self, model, packageIter):
        """
        Get all available informations about selected package
        """

        pkgName = model.get_value(packageIter, 2)
        pkgInstalledVersion = model.get_value(packageIter, 3)
        pkgRepositoryVersion = model.get_value(packageIter, 4)
        pkgDescription = model.get_value(packageIter, 10)

        color = WHITE
        useColor = True
        iconUrgent = False

        # Reset widgets
        self.label_name.set_markup(str())
        self.label_description.set_markup(str())
        self.label_website.set_markup(str())

        self.image_screenshot.clear()

        self.button_zoom.hide()
        self.button_zoom.set_sensitive(False)
        self.button_files.hide()
        self.button_files.set_sensitive(False)
        self.button_changelog.hide()
        self.button_changelog.set_sensitive(False)
        self.button_frugalbuild.hide()
        self.button_frugalbuild.set_sensitive(False)
        self.button_build.hide()
        self.button_build.set_sensitive(False)
        self.button_reinstall.hide()
        self.button_reinstall.set_sensitive(False)

        self.store_details.clear()

        # Check if this is a search and get informations
        if self.__search_status:
            pkgRepo = self.notebook_tab_name

        # Get current repo index
        else:
            pkgRepo = self.combo_repositories.get_active_text()

        # Get package version from treeview data
        pkgVersion = pkgRepositoryVersion
        if len(pkgInstalledVersion.rstrip()) > 0:
            pkgVersion = pkgInstalledVersion

        # Put value in cache to be use with Changelog and Files windows
        self.selected["package_name"] = pkgName
        self.selected["package_version"] = pkgVersion

        # Set selected package for package menu
        self.selected["menu"] = pkgName

        # Get repository and install package status
        data = self.pacman.package(pkgRepo, pkgName)

        # --------------------
        # Icon
        # --------------------

        self.image_package.set_from_pixbuf(
            self.icon_load(data.name, 22, self.icon_package))

        # --------------------
        # Name - version
        # --------------------

        self.label_name.set_markup(
            "<span font='18'><b>%s - %s</b></span>" % (
            data.name, data.version))

        # --------------------
        # Description
        # --------------------

        if data.description is not None:
            self.label_description.set_markup(data.description)

        # --------------------
        # Check if the FrugalBuild is available
        # --------------------

        self.selected["frugalbuild"] = data.frugalbuild

        if data.frugalbuild is not None:
            self.button_frugalbuild.show()
            self.button_frugalbuild.set_sensitive(True)

        # --------------------
        # Check if package is available in local cache
        # --------------------

        if self.pacman.package_fpm(data.name, data.version):
            self.image_cache.set_from_pixbuf(self.icon_cache)
            self.image_cache.set_tooltip_text(
                _("This package is present into the packages cache"))

        else:
            self.image_cache.clear()
            self.image_cache.set_tooltip_text("")

        # --------------------
        # Website link
        # --------------------

        if data.url is not None:
            self.label_website.set_markup(
                "<a href='%(url)s' title='%(url)s'>%(title)s</a>" %
                dict(url=str(data.url), title=_("Visit website")))

        # --------------------
        # Compiling time
        # --------------------

        if data.sbu is not None:
            self.store_details.append(None, ["%s:" % _("Compiling time"),
                "%s SBU" % str(data.sbu), WHITE, False])

        # --------------------
        # Maintainer
        # --------------------

        if data.packager is not None:

            if type(data.packager) == list:
                packagers = ", ".join(data.packager)
            else:
                packagers = data.packager

            self.store_details.append(None, ["%s:" % _("Packagers"),
                packagers, WHITE, False])


        # Get more informations for installed or not installed package
        if data.installed:

            if not data.nobuild:
                self.button_reinstall.show()
                self.button_reinstall.set_sensitive(True)

                if not data.name in self.packages_data["reinstallation"]:
                    icon = self.icon_to_install
                    text = _("Reinstall this package")
                else:
                    icon = self.icon_to_remove
                    text = _("Cancel reinstallation")

                self.label_reinstall.set_text(text)
                self.button_reinstall.set_tooltip_text(text)
                self.image_reinstall.set_from_pixbuf(icon)

            # --------------------
            # Installation date
            # --------------------

            if data.install_date is not None:
                self.store_details.append(None, ["%s:" % _("Install date"),
                    str(data.install_date), WHITE, False])

            # --------------------
            # Package size
            # --------------------

            if data.size is not None:
                self.store_details.append(None, ["%s:" % _("Size"),
                    "%s MB" % str(data.size), WHITE, False])

            # --------------------
            # Show tools button
            # --------------------

            if self.pacmanConfig.option("DBPath") is not None:
                databases_path = self.pacmanConfig.option("DBPath").value
            else:
                databases_path = DBPATH_FILE

            if exists("%s/local/%s-%s/files" % (
                databases_path, pkgName, pkgVersion)):
                self.button_files.show()
                self.button_files.set_sensitive(True)

            if exists("%s/local/%s-%s/changelog" % (
                databases_path, pkgName, pkgVersion)):
                self.button_changelog.show()
                self.button_changelog.set_sensitive(True)

        else:
            # --------------------
            # Package compressed size
            # --------------------

            if data.compress_size is not None and not data.nobuild:
                self.store_details.append(None, ["%s:" % _("Compress size"),
                    "%s MB" % str(data.compress_size), WHITE, False])

            # --------------------
            # Package uncompressed size
            # --------------------

            if data.uncompress_size is not None and not data.nobuild:
                self.store_details.append(None, ["%s:" % _("Uncompress size"),
                    "%s MB" % str(data.uncompress_size), WHITE, False])

        # --------------------
        # Merge button
        # --------------------

        if data.nobuild:
            self.button_build.show()
            self.button_build.set_sensitive(True)

        # --------------------
        # Groups list
        # --------------------

        if data.groups is not None and len(data.groups) > 0:

            if "base" in data.groups:
                iconUrgent = True

            groupsRoot = self.store_details.append(None,
                ["%s:" % _("Groups"), data.groups[0], WHITE, False])

            if len(data.groups) > 1:
                for group in data.groups[1:]:
                    self.store_details.append(groupsRoot,
                        ["", group, WHITE, False])

        # --------------------
        # Dependances list
        # --------------------

        dependenciesRegex = "^([A-Za-z0-9\-\+\_]*)([><=]+.*)$"

        if not data.depends is None and len(data.depends) > 0:
            data.depends.sort()

            splitPkg = splitRegex(dependenciesRegex, data.depends[0])

            text = data.depends[0]
            if len(splitPkg) > 1:
                text = "%s (%s)" % (splitPkg[0], splitPkg[1])

            color = self.config.get("colors", "remove")
            if self.pacman.package_installed(splitPkg[0])[0]:
                color = self.config.get("colors", "install")

            dependsRoot = self.store_details.append(None,
                ["%s:" % _("Depends"), text, color, True])

            if len(data.depends) > 1:
                for package in data.depends[1:]:

                    splitPkg = splitRegex(dependenciesRegex, package)

                    color = self.config.get("colors", "remove")
                    if self.pacman.package_installed(splitPkg[0])[0]:
                        color = self.config.get("colors", "install")

                    text = package
                    if len(splitPkg) > 1:
                        text = "%s (%s)" % (splitPkg[0], splitPkg[1])

                    self.store_details.append(dependsRoot,
                        ["", text, color, True])

        # --------------------
        # Provides list
        # --------------------

        if data.provides is not None and len(data.provides) > 0:
            data.provides.sort()

            providesRoot = self.store_details.append(None,
                ["%s:" % _("Provides"), data.provides[0], WHITE, False])

            if len(data.provides) > 1:
                for package in data.provides[1:]:
                    self.store_details.append(providesRoot,
                        ["", package, WHITE, False])

        # --------------------
        # Replaces list
        # --------------------

        if data.replaces is not None and len(data.replaces) > 0:
            data.replaces.sort()

            replacesRoot = self.store_details.append(None,
                ["%s:" % _("Replaces"), data.replaces[0], WHITE, False])

            if len(data.replaces) > 1:
                for package in data.replaces[1:]:
                    self.store_details.append(replacesRoot,
                        ["", package, WHITE, False])

        # --------------------
        # Required list
        # --------------------

        if data.required_by is not None and len(data.required_by) > 0:
            data.required_by.sort()

            requiredRoot = self.store_details.append(None,
                ["%s:" % _("Required_by"), data.required_by[0], WHITE, False])

            if len(data.required_by) > 1:
                for package in data.required_by[1:]:
                    self.store_details.append(requiredRoot,
                        ["", package, WHITE, False])

        # --------------------
        # Conflicts (de canard) list
        # --------------------

        if data.conflicts is not None and len(data.conflicts) > 0:
            data.conflicts.sort()

            conflictsRoot = self.store_details.append(None,
                ["%s:" % _("Conflits"), data.conflicts[0], WHITE, False])

            if len(data.conflicts) > 1:
                for package in data.conflicts[1:]:
                    self.store_details.append(conflictsRoot,
                        ["", package, WHITE, False])

        # --------------------
        # SHA1SUMS
        # --------------------

        # Get active repo
        if data.installed:
            pkgRepo = self.pacman.repositories_index()

        # Check sha1sums for normal package
        sha1sums = None
        if not data.nobuild:

            try:
                sha1sums = self.pacman.package_sha1sums(pkgRepo, pkgName)

            except:
                sha1sums = self.pacman.package_sha1sums(
                    self.notebook_tab_name, pkgName)

        # Strange case where None from sha1sums can be a string (╯°□°）╯︵ ┻━┻
        if sha1sums is not None and not sha1sums == "None":
            self.store_details.append(None,
                ["%s:" % _("SHA1SUMS"), str(sha1sums), WHITE, False])

        # --------------------
        # Package screenshot
        # --------------------

        if not self.config.get("network", "offline") and \
            self.config.get("network", "download"):

            if self.__website_status and not pkgName in self.screenshots:

                if not self.__thread_thumbnail == 0:
                    source_remove(self.__thread_thumbnail)

                loader = self.screenshots_download(pkgName)
                self.__thread_thumbnail = idle_add(loader.next)

        self.screenshots_thumbnail_show(pkgName)

        self.treeview_details.expand_all()

        self.statusbar_update()


    def screenshots_download (self, pkgName, mode="thumbnail"):
        """
        Get package screenshot from http://screenshots.ubuntu.com
        """

        sizePath = int(getFolderSize(self.cachePath)/(1024*1024.0))
        maxSize = int(self.config.get("application", "max_cache_size"))

        cacheFile = "%spkg_%s" % (self.cachePath, pkgName)
        if mode == "screenshot":
            cacheFile = "%spkg_%s_large" % (self.cachePath, pkgName)

        # If screenshot isn't already into cache folder
        if not exists(cacheFile):

            self.screenshots.append(pkgName)

            # Check if folder cache can contain more screenshots
            if sizePath < maxSize or maxSize == 0:

                # Change cursor
                self.window.set_cursor(gtk.gdk.Cursor(
                    gtk.gdk.WATCH))

                if not mode == "screenshot":

                    # Show a connect icon
                    self.image_screenshot.set_from_pixbuf(
                        self.icon_network_receive)

                yield True

                # Can check screenshots website
                website = Network("%s/%s/%s" % (WEBSITE, mode, pkgName))

                if website.checkUrl():
                    yield True

                    if website.downloadContent(cacheFile):

                        if mode == "thumbnail":
                            LOG.info(_("%s thumbnail was saved") % pkgName)

                    else:
                        self.__website_status = False

                        WindowPopup(_("Screenshots server problem"),
                            _("The screenshots server is too slow, disable "
                            "this function"))


                self.window.set_cursor(None)

                if mode == "thumbnail":
                    self.screenshots_thumbnail_show(pkgName)

        # Show window_screenshots if screenshot is available
        if mode == "screenshot":
            if exists("%spkg_%s_large" % (self.cachePath, pkgName)):
                self.window_screenshots()

        self.__thread_screenshot = 0
        self.__thread_thumbnail = 0

        yield False


    def __check_servers (self, url, fallback):
        """
        Check if the screenshot website is available
        """

        gtk.gdk.threads_enter()

        website = Network(url)
        self.__website_status = website.checkUrl(5)

        if not self.__website_status:
            website = Network(fallback)
            self.__website_status = website.checkUrl(5)

        self.__website_checked = False
        self.statusbar_update()

        LOG.info(_("Screenshots website has been checked"))

        gtk.gdk.threads_leave()


    def screenshots_thumbnail_show (self, pkgName):
        """
        Show thumbnail in the interface
        """

        fileCache = "%spkg_%s" % (self.cachePath, pkgName)

        if exists(fileCache):
            self.image_screenshot.set_from_file(fileCache)

            if not self.config.get("network", "offline") and \
                self.__website_status:
                self.button_zoom.show()
                self.button_zoom.set_sensitive(True)

            fileCacheLarge = "%spkg_%s_large" % (self.cachePath, pkgName)
            if exists(fileCacheLarge):
                self.button_zoom.show()
                self.button_zoom.set_sensitive(True)

        else:
            self.image_screenshot.clear()
            self.button_zoom.hide()


    def screenshots_large_show (self, widget):
        """
        Run window_screenshots into a thread
        """

        loader = self.screenshots_download(
            self.selected["package_name"], "screenshot")

        if not self.__thread_screenshot == 0:
            source_remove(self.__thread_screenshot)

        self.__thread_screenshot = idle_add(loader.next)


    def nobuild_packages (self, repository=None):
        """
        Get only packages name from nobuild list
        """

        if repository == None:
            repository = self.notebook_tab_name

        nobuildList = self.pacman.nobuild(repository)

        return [row[1] for row in nobuildList]


    # ------------------------------------
    #   Misc
    # ------------------------------------

    def notification_show (self, title, description, icon=""):
        """
        Send a notification
        """

        if self.config.get("application", "show_notification"):

            try:
                Notification(title, description, icon).show()

            except:
                pass


    def icon_load (self, name, size=16, fallback="image-missing"):
        """
        Get an icon from data folder
        """

        return getIconFromTheme(name, size, fallback, self.icons_theme)


    def network_update (self, widget, *event):
        """
        Change configuration with actual self.tool_offline status
        """

        status = self.tool_offline.get_active()

        if status:
            self.image_offline.set_from_pixbuf(self.icon_network_offline)
        else:
            self.image_offline.set_from_pixbuf(self.icon_network_online)

        self.tool_offline.set_active(status)

        self.item_update.set_sensitive(not status)
        self.tool_update.set_sensitive(not status)

        if len(self.label_name.get_text()) > 0 and \
            self.image_screenshot.get_pixbuf() is not None:

            if not status:
                self.button_zoom.show()
            else:
                self.button_zoom.hide()

            self.button_zoom.set_sensitive(not status)

        self.config.set("network", "offline", status)
        self.config.write_configuration()

    # ------------------------------------
    #   Screenshot window
    # ------------------------------------

    def window_screenshots (self):
        """
        Show a screenshot
        """

        window = Window(self, _("Screenshot viewer"),
            _("Show <b>%s</b> screenshot") % self.selected["package_name"])

        viewScreenshot = gtk.Viewport()
        scrollScreenshot = gtk.ScrolledWindow()

        # Properties
        window.set_main_icon(self.category_screenshot)
        window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
        window.set_default_response(gtk.RESPONSE_CLOSE)


        alignmentScreenshotImage = gtk.Alignment(0.5, 0.5, 0, 0)
        screenshotImage = gtk.Image()

        alignmentScreenshotImage.add(screenshotImage)

        fileCache = "%spkg_%s_large" % (
            self.cachePath, self.selected["package_name"])
        screenshotImage.set_from_file(fileCache)

        scrollScreenshot.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        viewScreenshot.set_shadow_type(gtk.SHADOW_NONE)

        viewScreenshot.add(alignmentScreenshotImage)
        scrollScreenshot.add(viewScreenshot)

        # Get image size to set window size correctly
        width, height = getSizeFromImage(fileCache)

        # 800x600 screen must set separatly
        if self.get_screen().get_width() <= 800:
            height = height + 150
            if height > 600:
                height = 600

            window.set_size_request(800, height)
        else:
            window.set_size_request(width + 50, height + 150)


        window.add_widget(scrollScreenshot)

        window.show_all()

        window.run()
        window.destroy()


    # ------------------------------------
    #   Files window
    # ------------------------------------

    def tooltips_permissions (self, treeview, event):
        """
        Show a tooltip on row which give file permissions informations
        """

        self.__tooltip_files = gtk.Tooltips()

        # Show tooltip only during the move and if no buttons are pressed
        if not event.state == gtk.gdk.KEY_PRESS:
            text = str()

            pos = treeview.get_dest_row_at_pos(int(event.x), int(event.y))
            if pos is not None:

                model = treeview.get_model()
                treepath = model.get_value(model.get_iter(pos[0]), 2)

                if exists("/%s" % treepath):

                    fileStatus = stat("/%s" % treepath)
                    filePerms = oct(S_IMODE(fileStatus[ST_MODE]))

                    text = "/%s\n%s" % (str(treepath),
                        str(getPermissionFromOctect(filePerms)))

            self.__tooltip_files.set_tip(treeview, text)


    def menu_show (self, treeview, event, menu):
        """
        Show a menu when user right-click in treeview
        """

        if event.button == 3:
            pathInfo = treeview.get_path_at_pos(int(event.x), int(event.y))

            if pathInfo is not None:
                path, col = pathInfo[0:2]
                treeview.grab_focus()
                treeview.set_cursor(path, col, 0)

                menu.popup(None, None, None, event.button, event.time)

            return True


    def menu_manage_files (self, menuItem, treeview, copy=False):
        """
        Make an action when user click on files menu item
        """

        # Current selection
        model, treeiter = treeview.get_selection().get_selected()
        if model is not None:
            path = "/%s" % model.get_value(treeiter, 2)

            # Open file manager
            if not copy:
                call(["xdg-open", path])

            # Copy selected path
            else:
                self.clipboard.set_text(path)


    def window_files (self, widget):
        """
        Show a window with a treeview content the package files list
        """

        path = list()
        specialCases = ['-x-ms', 'x-x']


        self.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH))

        refresh()

        packageFilesList = self.pacman.package_files(
            self.selected["package_name"])


        window = Window(self, _("Files viewer"),
            _("<b>%s</b> package files") % self.selected["package_name"])

        # Properties
        window.set_default_size(600, 600)
        window.set_main_icon(self.category_file)
        window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        menu = gtk.Menu()

        menuOpenFM = gtk.ImageMenuItem(_("Open in the files manager"))
        menuCopyPath = gtk.ImageMenuItem(_("Copy in the clipboard"))


        menuOpenFM.set_image(
            gtk.image_new_from_stock(gtk.STOCK_OPEN, gtk.ICON_SIZE_MENU))
        menuCopyPath.set_image(
            gtk.image_new_from_stock(gtk.STOCK_COPY, gtk.ICON_SIZE_MENU))

        menu.append(menuOpenFM)
        menu.append(menuCopyPath)


        filesList = gtk.TreeStore(gtk.gdk.Pixbuf, str, str)
        filesTreeview = gtk.TreeView(filesList)

        filesColumn = gtk.TreeViewColumn()
        filesColumnPath = gtk.TreeViewColumn()
        filesCellIcon = gtk.CellRendererPixbuf()
        filesCellLabel = gtk.CellRendererText()
        filesCellPath = gtk.CellRendererText()

        filesTreeview.set_headers_visible(False)
        filesTreeview.set_size_request(180,0)
        filesTreeview.set_search_column(1)
        filesTreeview.set_has_tooltip(True)

        filesColumn.set_sort_column_id(1)
        filesColumn.pack_start(filesCellIcon, False)
        filesColumn.pack_start(filesCellLabel, True)
        filesColumn.add_attribute(filesCellIcon, "pixbuf", 0)
        filesColumn.add_attribute(filesCellLabel, "text", 1)

        filesColumnPath.pack_start(filesCellPath, True)
        filesColumnPath.add_attribute(filesCellPath, "text", 2)
        filesColumnPath.set_visible(False)

        filesTreeview.append_column(filesColumn)
        filesTreeview.append_column(filesColumnPath)
        filesScroll = gtk.ScrolledWindow()

        filesScroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        filesScroll.add(filesTreeview)

        menuOpenFM.connect("activate", self.menu_manage_files,
            filesTreeview)
        menuCopyPath.connect("activate", self.menu_manage_files,
            filesTreeview, True)

        filesTreeview.connect("button_press_event", self.menu_show, menu)
        filesTreeview.connect("motion-notify-event", self.tooltips_permissions)


        # Set root
        root = filesList.append(None, [self.icon_folder, '/', ''])

        for element in packageFilesList:

            chaine = element.split('/')

            if len(chaine[-1]) == 0:

                # Folder
                if len(chaine) == 2:
                    # Root sub-folder
                    path.append([str(element), filesList.append(root,
                        [self.icon_folder, str(element), str(element)])])
                else:
                    # Other folders
                    found = root

                    for index in path:
                        if str(element[0:len(element) - len(chaine[-2]) - 1]) \
                            == index[0]:

                            # If parent folder exist
                            found = index[1]
                            break

                    path.append([str(element), filesList.append(found,
                        [self.icon_folder,
                        str(chaine[-2]) + '/', str(element)])])

            # File
            else:
                found = root

                for index in path:

                    # If parent folder exist
                    if element[0:len(element) - len(chaine[-1])] == index[0]:
                        found = index[1]

                mime, encoding = guess_type(str(element))

                icon = self.icon_file
                if mime is not None:
                    mime = mime.replace('/', '-')

                    for case in specialCases:
                        mime = mime.replace(case, '')

                    if not mime in self.__mimetypes.keys():
                        self.__mimetypes[mime] = self.icon_load(mime,
                            fallback=ICON_FILE)

                    icon = self.__mimetypes[mime]

                filesList.append(found, [icon, str(chaine[-1]), str(element)])

        filesTreeview.expand_all()

        menu.show_all()

        self.window.set_cursor(None)

        window.add_widget(filesScroll)

        window.start()


    # ------------------------------------
    #   Changelog window
    # ------------------------------------

    def window_changelog (self, widget):
        """
        Show the package changelog
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        window = Window(self, _("Changelog viewer"),
            _("<b>%s</b> package changelog") % self.selected["package_name"])

        # Properties
        window.set_default_size(600, 600)
        window.set_main_icon(self.category_changelog)
        window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        # ------------------------------------
        #   Editor
        # ------------------------------------

        editor = Sourceview()

        # Properties
        editor.language = "changelog"

        # Show line numbers in editor
        if self.config.get("editor", "number_line"):
            editor.line_numbers()

        # Set editor colorscheme
        colorscheme = self.config.get("editor", "colorscheme")
        if not colorscheme == None:
            editor.colorscheme = colorscheme

        editor.content = readFile("%s/local/%s-%s/changelog" % (
            self.pacman.configuration_value("DBPath"),
            self.selected["package_name"],
            self.selected["package_version"]))

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        window.add_widget(editor)

        window.start()


    # ------------------------------------
    #   FrugalBuild window
    # ------------------------------------

    def window_frugalbuild (self, widget):
        """
        Show the package FrugalBuild
        """

        window = Window(self, _("FrugalBuild viewer"),
            _("<b>%s</b> package FrugalBuild") % (
            self.selected["package_name"]))

        editor = Sourceview()

        # Properties
        window.set_default_size(600, 600)
        window.set_main_icon(self.category_frugalbuild)
        window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        editor.tab_width = 4
        editor.language = "frugalbuild"

        # Show line numbers in editor
        if self.config.get("editor", "number_line"):
            editor.line_numbers()

        # Set editor colorscheme
        colorscheme = self.config.get("editor", "colorscheme")
        if not colorscheme == None:
            editor.colorscheme = colorscheme

        editor.content = readFile(self.selected["frugalbuild"])

        window.add_widget(editor)

        window.start()


    # ------------------------------------
    #   Missing dependencies window
    # ------------------------------------

    def window_missing_dependancies (self, cellrenderer, path, treeview):
        """
        Inform user that a package need some dependencies to work
        """

        missingDependencies = False
        missingDependenciesList = list()

        dependenciesRegex = "^([A-Za-z0-9\-\+\_]*)([><=]+.*)$"


        # Get treeview informations
        model, treeiter = treeview.get_selection().get_selected()
        path = model.get_path(treeiter)[0]

        # Change package status
        self.treeview_update_rows(model, path)

        pkgStatus = model[path][0]
        pkgName = model[path][2]
        pkgDescription = model[path][10]

        repository = self.notebook_tab_name

        pkgInformations = self.pacman.package(repository, pkgName)

        saveModel = self.__model_saved[repository]

        # Get package dependencies
        dependenciesList = self.pacman.package_missing_dependancies(
            self.notebook_tab_name, pkgName)
        if len(dependenciesList) > 0:
            dependenciesList.sort()

            for depend in dependenciesList:

                # Get only not installed dependencies and not marked to install
                if not depend in self.packages_data["installation"] and \
                    not depend in self.packages_data["reinstallation"]:

                    missingDependenciesList.append(depend)


        if self.config.get("packages", "check_dependencies") == "manually":

            # Show a window if there are not installed dependencies
            if len(missingDependenciesList) > 0 and pkgStatus and \
                pkgDescription == "remove":

                window = Window(self, _("Missing dependencies"),
                    [_("Some packages are require for <b>%s</b>.") % pkgName,
                    _("Do you want to install them ?")])

                # Properties
                window.set_default_size(500, 300)
                window.set_main_icon(self.category_transaction)
                window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
                window.add_button(gtk.STOCK_ADD, gtk.RESPONSE_APPLY)
                window.set_default_response(gtk.RESPONSE_APPLY)

                # ------------------------------------
                #   Packages list
                # ------------------------------------

                listPackages = gtk.ListStore(bool, gtk.gdk.Pixbuf, str)
                treeviewPackages = gtk.TreeView(listPackages)

                columnPackages = gtk.TreeViewColumn()

                cellPackagesStatus = gtk.CellRendererToggle()
                cellPackagesImage = gtk.CellRendererPixbuf()
                cellPackagesName = gtk.CellRendererText()

                scrollPackages = gtk.ScrolledWindow()


                treeviewPackages.set_headers_visible(False)
                treeviewPackages.set_hover_selection(True)
                treeviewPackages.expand_all()

                columnPackages.pack_start(cellPackagesStatus, False)
                columnPackages.pack_start(cellPackagesImage, False)
                columnPackages.pack_start(cellPackagesName, True)

                columnPackages.add_attribute(cellPackagesStatus, "active", 0)
                columnPackages.add_attribute(cellPackagesImage, "pixbuf", 1)
                columnPackages.add_attribute(cellPackagesName, "text", 2)

                cellPackagesStatus.set_property('activatable', True)

                treeviewPackages.append_column(columnPackages)

                scrollPackages.set_policy(
                    gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                scrollPackages.add(treeviewPackages)

                cellPackagesStatus.connect('toggled',
                    self.treeview_checked_row,
                    treeviewPackages.get_model())


                window.add_widget(scrollPackages)

                # ------------------------------------
                #   Fill treeview
                # ------------------------------------

                for depend in missingDependenciesList:
                    listPackages.append([True, self.icon_installed, depend])

                window.show_all()
                if window.run() == gtk.RESPONSE_APPLY:
                    window.destroy()

                    for package in listPackages:
                        if not package[0]:
                            missingDependenciesList.remove(package[0])

                    missingDependencies = True

        elif self.config.get("packages", "check_dependencies") == "automatic":
            missingDependencies = True


        if missingDependencies:

            for package in missingDependenciesList:
                self.packages_data["installation"].append(package)

                # Add package to installation list
                if not package in self.packages_data["installation"]:
                    self.packages_data["installation"].append(package)

                if not package in saveModel:
                    saveModel.append(package)

                # Set bold weight
                bold = WEIGHT_NORMAL
                if self.config.get("colors", "use_bold"):
                    bold = WEIGHT_BOLD

                # Get modified row index
                self.treeview_update_from_data(dict(status=True,
                    icon=self.icon_to_install, name=package, color=GREEN,
                    use=True, bold=bold))

            self.statusbar_update()



    # ------------------------------------
    #   Transaction window
    # ------------------------------------

    def window_sync_packages (self, widget):
        """
        Show a window with the transaction based on the user selected packages
        """

        # Get index of frugalware repo (stable/current)
        index = self.pacman.repository_official()

        # Create a list for install and reinstall packages
        packagesInstallationList = list()

        if not len(self.packages_data["installation"]) == 0 or \
            not len(self.packages_data["remove"]) == 0 or \
            not len(self.packages_data["reinstallation"]) == 0:

            self.packages_data["installation"].sort()
            self.packages_data["remove"].sort()


            window = Window(self)

            # Properties
            window.set_default_size(600, 600)
            window.set_main_icon(self.category_transaction)
            window.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
            window.add_button(gtk.STOCK_APPLY, gtk.RESPONSE_APPLY)
            window.set_default_response(gtk.RESPONSE_APPLY)

            # --------------------------------------------------------------
            #   Installation
            # --------------------------------------------------------------

            if len(self.packages_data["installation"]) > 0 or \
                len(self.packages_data["reinstallation"]) > 0:

                tableInstallation = gtk.VBox()
                tableInstallation.set_border_width(6)


                # ------------------------------------
                #   Packages list
                # ------------------------------------

                installList = gtk.ListStore(bool, gtk.gdk.Pixbuf, str, str)

                installTreeview = gtk.TreeView()

                installColumn = gtk.TreeViewColumn()
                installCellStatus = gtk.CellRendererToggle()
                installCellImage = gtk.CellRendererPixbuf()
                installCellName = gtk.CellRendererText()
                installCellSize = gtk.CellRendererText()

                installScroll = gtk.ScrolledWindow()


                installTreeview.set_headers_visible(False)
                installTreeview.set_hover_selection(True)
                installTreeview.expand_all()

                installColumn.pack_start(installCellStatus, False)
                installColumn.pack_start(installCellImage, False)
                installColumn.pack_start(installCellName, True)
                installColumn.pack_start(installCellSize, False)

                installColumn.add_attribute(installCellStatus, "active", 0)
                installColumn.add_attribute(installCellImage, "pixbuf", 1)
                installColumn.add_attribute(installCellName, "text", 2)
                installColumn.add_attribute(installCellSize, "text", 3)

                installCellStatus.set_property('activatable', True)

                installTreeview.append_column(installColumn)
                installTreeview.set_model(installList)

                installScroll.set_policy(
                    gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                installScroll.add(installTreeview)

                installCellStatus.connect('toggled',
                    self.treeview_checked_row,
                    installTreeview.get_model())

                # ------------------------------------
                #   Flags
                # ------------------------------------

                installFlagNoConfirm = gtk.CheckButton(
                    _("Bypass all the confirmation messages"))
                installFlagDownloadOnly = gtk.CheckButton(
                    _("Download packages from the server, but don't install "
                    "them"))
                installFlagForce = gtk.CheckButton(
                    _("Don't check files conflicts and force install"))


                installFlagNoConfirm.set_active(True)

                installFlagForce.set_tooltip_text(_("If the package that is "
                    "about to be installed contains files that are already "
                    "installed, this option will cause all those files to be "
                    "overwritten. This option should be used with care, "
                    "ideally not at all"))


                # ------------------------------------
                #   Packages size
                # ------------------------------------

                installSizeLabel = gtk.Label("")

                # Get to install list
                packagesInstallationList.extend(
                    self.packages_data["installation"])

                # Get to reinstall list
                if len(self.packages_data["reinstallation"]) > 0:
                    packagesInstallationList.extend(
                        self.packages_data["reinstallation"])

                packagesInstallationList.sort()

                # Get packages size
                installValue = 0
                for package in packagesInstallationList:

                    data = self.pacman.package(index, package)

                    status, version = self.pacman.package_installed(package)

                    icon = self.icon_to_install

                    # Package is installed
                    if status:
                        icon = self.icon_upgrade

                        # Check if current package is not updatable
                        if version == data.version:
                            size = data.size

                        else:
                            size = data.compress_size

                    else:
                        size = data.compress_size

                    # Get package size
                    packageSize = 0.0
                    if not size == None:
                        packageSize = float(size)

                    # Format size and add package into treeview
                    formatSize = "%s Mb" % str(format(packageSize, '.2f'))
                    installList.append([True, icon, package, formatSize])

                    installValue += packageSize

                formatSize = "%s Mb" % str(format(installValue, '.2f'))
                installSizeLabel.set_text(_("Total size to download : %s") % \
                    formatSize)

                # ------------------------------------
                #   Integrate widgets
                # ------------------------------------

                tableInstallation.pack_start(installScroll)
                tableInstallation.pack_start(installSizeLabel,
                    False, False, 8)
                tableInstallation.pack_start(installFlagNoConfirm,
                    False, False)
                tableInstallation.pack_start(installFlagDownloadOnly,
                    False, False)
                tableInstallation.pack_start(installFlagForce,
                    False, False)


            # --------------------------------------------------------------
            #   Deletion
            # --------------------------------------------------------------

            if len(self.packages_data["remove"]) > 0:

                tableDeletion = gtk.VBox()
                tableDeletion.set_border_width(6)


                # ------------------------------------
                #   Packages list
                # ------------------------------------

                deleteList = gtk.ListStore(bool, gtk.gdk.Pixbuf, str, str)

                deleteTreeview = gtk.TreeView()

                deleteColumn = gtk.TreeViewColumn()
                deleteCellStatus = gtk.CellRendererToggle()
                deleteCellImage = gtk.CellRendererPixbuf()
                deleteCellName = gtk.CellRendererText()
                deleteCellSize = gtk.CellRendererText()

                deleteScroll = gtk.ScrolledWindow()


                deleteTreeview.set_headers_visible(False)
                deleteTreeview.set_hover_selection(True)

                deleteColumn.pack_start(deleteCellStatus, False)
                deleteColumn.pack_start(deleteCellImage, False)
                deleteColumn.pack_start(deleteCellName, True)
                deleteColumn.pack_start(deleteCellSize, False)

                deleteColumn.add_attribute(deleteCellStatus, "active", 0)
                deleteColumn.add_attribute(deleteCellImage, "pixbuf", 1)
                deleteColumn.add_attribute(deleteCellName, "text", 2)
                deleteColumn.add_attribute(deleteCellSize, "text", 3)

                deleteCellStatus.set_property('activatable', True)

                deleteTreeview.append_column(deleteColumn)
                deleteTreeview.set_model(deleteList)

                deleteScroll.set_policy(gtk.POLICY_AUTOMATIC,
                    gtk.POLICY_AUTOMATIC)
                deleteScroll.add(deleteTreeview)

                deleteCellStatus.connect('toggled',
                    self.treeview_checked_row, deleteTreeview.get_model())

                # ------------------------------------
                #   Packages size
                # ------------------------------------

                deleteSizeLabel = gtk.Label("")

                # Get packages size
                deleteValue = 0
                icon = self.icon_to_remove

                for package in self.packages_data["remove"]:

                    data = self.pacman.package(index, package)

                    # Get package size
                    packageSize = 0.0
                    if not data.size == None:
                        packageSize = float(data.size)

                    # Format size and add package into treeview
                    formatSize = "%s Mb" % str(format(packageSize, '.2f'))
                    deleteList.append([True, icon, package, formatSize])

                    deleteValue += packageSize

                formatSize = "%s Mb" % str(format(deleteValue, '.2f'))
                deleteSizeLabel.set_text(_("Total size to remove : %s") % \
                    formatSize)

                # ------------------------------------
                #   Flags
                # ------------------------------------

                deleteFlagNoConfirm = gtk.CheckButton(
                    _("Bypass all the confirmation messages"))
                deleteFlagForce = gtk.CheckButton(
                    _("Don't check files conflicts and force install"))


                deleteFlagNoConfirm.set_active(True)
                deleteFlagForce.set_tooltip_text(_("If the package that is "
                    "about to be installed contains files that are already "
                    "installed, this option will cause all those files to be "
                    "overwritten. This option should be used with care, "
                    "ideally not at all"))

                # ------------------------------------
                #   Integrate widgets
                # ------------------------------------

                tableDeletion.pack_start(deleteScroll)
                tableDeletion.pack_start(deleteSizeLabel,
                    False, False, 8)
                tableDeletion.pack_start(deleteFlagNoConfirm,
                    False, False)
                tableDeletion.pack_start(deleteFlagForce,
                    False, False)


            # --------------------------------------------------------------
            #   Integrate widgets
            # --------------------------------------------------------------

            # Add notebook to installation window
            if len(self.packages_data["installation"]) > 0 or \
                len(self.packages_data["reinstallation"]) > 0:

                # Add to delete and to install packages into notebook
                if len(self.packages_data["remove"]) > 0:

                    notebook = gtk.Notebook()

                    notebook.append_page(tableInstallation,
                        gtk.Label(_("Installation")))
                    notebook.append_page(tableDeletion,
                        gtk.Label(_("Deletion")))

                    notebook.set_border_width(4)

                    window.set_header_title(_("Transaction"))
                    window.set_header_subtitle(
                        _("These packages are going to be installed/removed."))

                    window.add_widget(notebook)

                # Add to install packages into window
                else:
                    window.set_header_title(_("Installation"))
                    window.set_header_subtitle(
                        _("These packages are going to be installed."))

                    window.add_widget(tableInstallation)

            # Add to delete packages into window
            else:
                if len(self.packages_data["remove"]) > 0:
                    window.set_header_title(_("Deletion"))
                    window.set_header_subtitle(
                        _("These packages are going to be removed."))

                    window.add_widget(tableDeletion)

            window.show_all()

            if window.start() == gtk.RESPONSE_APPLY:
                flags = dict()

                listInstallation = list()
                listRemove = list()

                # ------------------------------------
                #   To install/reinstall packages
                # ------------------------------------

                if len(self.packages_data["installation"]) > 0 or \
                    len(self.packages_data["reinstallation"]) > 0:

                    for package in installList:
                        if package[0]:
                            listInstallation.append(package[2])

                    flags["install_deps"] = self.__not_check_dependencies
                    flags["install_force"] = installFlagForce.get_active()
                    flags["install_download"] = \
                        installFlagDownloadOnly.get_active()
                    flags["install_noconfirm"] = \
                        installFlagNoConfirm.get_active()

                # ------------------------------------
                #   To remove packages
                # ------------------------------------

                if len(self.packages_data["remove"]) > 0:

                    for package in deleteList:
                        if package[0]:
                            listRemove.append(package[2])

                    flags["delete_deps"] = False
                    flags["delete_force"] = deleteFlagForce.get_active()
                    flags["delete_noconfirm"] = \
                        deleteFlagNoConfirm.get_active()

                # ------------------------------------
                #   Start synchronization
                # ------------------------------------

                if len(listInstallation) > 0 or len(listRemove) > 0:

                    action = Command()
                    action.sync()

                    flags["list_installation"] = listInstallation
                    flags["list_remove"] = listRemove

                    if action.generate(flags):
                        scriptFile = createScript(CONFIGPATH, action.command)

                        termVte = WindowVte(self, _("Packages installer"),
                            _("Using <b>%s</b> as installer script") % (
                            scriptFile), "sh %s" % scriptFile, True)

                        remove(scriptFile)

                        if termVte.success:
                            if self.pacman.system_reboot_needed(flags):
                                WindowPopup(_("Reboot is neccessary"),
                                    _("Some packages from base group have "
                                    "been update. You may reboot your "
                                    "system."), gtk.MESSAGE_WARNING)

                            # User wanna troll and remove this software
                            if "fpm-gui" in self.packages_data["remove"]:

                                # Show a realy sad face :'(
                                self.icon_interface_delete.show()

                                WindowPopup(_("pyFPM has been removed"),
                                    _("pyFPM has been removed from your "
                                    "system. Without, I can't live any longer."
                                    " So long, buddy."),
                                    self.icon_interface_delete)

                                LOG.critical(_("pyFPM has been removed"))
                                sys_exit()

                            # pyFPM or fpmd was updated or reinstalled
                            softwareUpdated = ""

                            if "fpmd" in self.packages_data["installation"] or \
                                "fpmd" in self.packages_data["reinstallation"]:
                                softwareUpdated = "fpmd"

                            if "fpm-gui" in self.packages_data["installation"] or \
                                "fpm-gui" in self.packages_data["reinstallation"]:
                                softwareUpdated = "fpm-gui"

                            if len(softwareUpdated) > 0:
                                WindowPopup(
                                    _("%s has been updated") % softwareUpdated,
                                    _("%(soft)s has been updated on your "
                                    "system. To avoid problems, %(soft)s must "
                                    "be close. Sorry for convenient.") % \
                                    {"soft": softwareUpdated},
                                    gtk.MESSAGE_WARNING)

                                sys_exit(_("%s has been updated") % (
                                    softwareUpdated))

                            # Reset interface
                            self.pacman.packages_installed(True)
                            self.reload_interface()

                            # Get not installed update
                            self.upgrade_get(True)

            self.statusbar_update()


class Splash (gtk.Window):
    """
    Splash window
    """

    def __init__(self, textArray):
        """
        Init splash window
        """

        gtk.Window.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.index = 1

        self.textArray = textArray

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_title(self.textArray[self.index])

        self.set_decorated(False)
        self.set_skip_taskbar_hint(True)
        self.set_position(gtk.WIN_POS_CENTER)

        self.set_wmclass("fpm-gui", "fpm-gui")

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.VBox()

        # ------------------------------------
        #   Backgrounds
        # ------------------------------------

        background = gtk.Image()

        self.progressbar = gtk.ProgressBar()

        # Properties
        background.set_from_file(getPixmap("fpm_gui", "data/splash.png"))

        self.progressbar.set_text(self.textArray[0])

        # ------------------------------------
        #   Add widgets into main window
        # ------------------------------------

        box.pack_start(background)
        box.pack_start(self.progressbar, False, False)

        self.add(box)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.show_all()


    def update (self):
        """
        Update progressbar informations
        """

        refresh()

        if self.index < len(self.textArray):
            progress = float(self.index) / (len(self.textArray) - 1)

            self.progressbar.set_text(self.textArray[self.index])
            self.progressbar.set_fraction(progress)
            self.progressbar.set_pulse_step(progress)

            self.index += 1

            refresh()


class PackagesList (gtk.ScrolledWindow):

    def __init__ (self, parent, name):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        if type(parent) is not Interface:
            raise ValueError(_("Wrong type for parent value"))

        self.repository_name = name

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals(parent)

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Scroll
        # ------------------------------------

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Treeview
        # ------------------------------------

        self.store = gtk.ListStore(
            bool,       # Status
            Pixbuf,     # Icon
            str,        # Name
            str,        # Installed version
            str,        # Update version
            str,        # Color
            str,        # Group
            bool,       # Color mode
            bool,       # Nobuild mode
            Weight,     # Bold value
            str)        # Description

        self.treeview = gtk.TreeView()
        self.filter_list = self.store.filter_new()
        self.sorted_list = gtk.TreeModelSort(self.filter_list)

        # Properties
        self.treeview.set_rules_hint(True)
        self.treeview.set_has_tooltip(True)
        self.treeview.set_enable_search(True)
        self.treeview.set_headers_visible(False)
        self.treeview.set_model(self.sorted_list)

        self.sorted_list.set_sort_column_id(2, gtk.SORT_ASCENDING)

        # ------------------------------------
        #   Cells
        # ------------------------------------

        self.cell_check = gtk.CellRendererToggle()
        cell_icon = gtk.CellRendererPixbuf()
        cell_name = gtk.CellRendererText()
        cell_install = gtk.CellRendererText()
        cell_update = gtk.CellRendererText()

        # Properties
        self.cell_check.set_property("active", True)
        self.cell_check.set_property("activatable", True)

        cell_install.set_alignment(.5, .5)
        cell_install.set_property("markup", True)

        cell_update.set_alignment(.5, .5)

        # ------------------------------------
        #   Columns
        # ------------------------------------

        column_check = gtk.TreeViewColumn()
        column_name = gtk.TreeViewColumn()
        column_install = gtk.TreeViewColumn()
        column_update = gtk.TreeViewColumn()

        # Properties
        column_check.pack_start(self.cell_check, True)
        column_name.pack_start(cell_icon, False)
        column_name.pack_start(cell_name, True)
        column_install.pack_start(cell_install, True)
        column_update.pack_start(cell_update, True)

        column_check.set_attributes(self.cell_check, active=0, sensitive=8)

        column_name.set_expand(True)
        column_name.set_resizable(True)
        column_name.set_sort_column_id(2)
        column_name.set_title(_("Package name"))
        column_name.add_attribute(cell_icon, "pixbuf", 1)
        column_name.set_attributes(cell_name,
            text=2, foreground=6, foreground_set=7, weight=9, weight_set=7)

        column_install.set_alignment(.5)
        column_install.set_resizable(True)
        column_install.set_sort_column_id(3)
        column_install.set_title(_("Installed version"))
        column_install.set_attributes(cell_install,
            text=3, foreground=6, foreground_set=7, weight=9, weight_set=7)

        column_update.set_alignment(.5)
        column_update.set_resizable(True)
        column_update.set_sort_column_id(4)
        column_update.set_title(_("Repository version"))
        column_update.set_attributes(cell_update,
            text=4, foreground=6, foreground_set=7, weight=9, weight_set=7)

        # ------------------------------------
        #   Add widgets into main interface
        # ------------------------------------

        self.treeview.append_column(column_check)
        self.treeview.append_column(column_name)
        self.treeview.append_column(column_install)
        self.treeview.append_column(column_update)

        self.add(self.treeview)


    def __init_signals (self, parent):
        """
        Connect signals to main interface
        """

        self.filter_list.set_visible_func(parent.filters_match)

        self.treeview.connect("button-press-event", parent.packages_select)
        self.treeview.connect("key-release-event", parent.packages_select)
        self.treeview.connect("query-tooltip", parent.tooltips_description)

        self.cell_check.connect("toggled", parent.window_missing_dependancies,
            self.treeview)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.store.clear()

        self.show_all()

        LOG.debug(
            _("%s repository tab has been created") % self.repository_name)
