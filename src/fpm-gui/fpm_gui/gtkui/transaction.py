# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from threading import Event
from threading import Thread

# Interface
import gtk

from window import Window

# Modules
from fpmd.tools.utils import getTranslation

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class TransactionThread (Thread):

    def __init__ (self, interface):
        """
        Constructor
        """

        Thread.__init__(self)

        self.interface = interface


    def run (self):
        """
        Start thread
        """

        self.interface.state = self.interface.function()

        if not self.interface.state:
            self.interface.window.response(gtk.RESPONSE_CLOSE)


class Transaction (object):

    def __init__ (self, icon, title, description, interface):
        """
        Constructor
        """

        self.maxActions = 1
        self.index = 0

        self.interface = interface
        self.pacman = interface.pacman
        self.function = interface.function

        self.rowParent = None

        self.state = False

        self.pacman.changeStockedInterface(self)


        # ------------------------------------------------------------------
        #       Grid
        # ------------------------------------------------------------------

        windowGrid = gtk.VBox()

        windowGrid.set_border_width(4)


        # ------------------------------------------------------------------
        #       Main window
        # ------------------------------------------------------------------

        self.window = Window()


        self.window.setWindowTitle(title)
        self.window.setWindowSize(600, 500)
        self.window.setWindowResizable(True)
        self.window.setWindowDeletable(False)

        self.window.setWindowParent(interface)

        self.window.setIcon(icon)
        self.window.setTitle(title)
        self.window.setDescription(description)

        self.window.addButton(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

        self.window.setDefaultButton(gtk.RESPONSE_CLOSE)
        self.window.setSensitiveButton(gtk.RESPONSE_CLOSE, False)


        # ------------------------------------------------------------------
        #       Transaction progress
        # ------------------------------------------------------------------

        self.progressbar = gtk.ProgressBar()

        labelTransaction = gtk.Label()
        # self.textTransaction = gtk.TextView()

        self.storeTransaction = gtk.TreeStore(str)
        self.treeTransaction = gtk.TreeView(self.storeTransaction)

        columnTransaction = gtk.TreeViewColumn()
        cellTransaction = gtk.CellRendererText()

        scrollTransaction = gtk.ScrolledWindow()


        self.progressbar.set_text(' ')

        labelTransaction.set_use_markup(True)
        labelTransaction.set_label("<b>%s</b>" % _("Log"))
        labelTransaction.set_alignment(0, 0.5)

        self.treeTransaction.set_headers_visible(False)
        self.treeTransaction.set_search_column(0)
        self.treeTransaction.set_has_tooltip(True)

        columnTransaction.set_sort_column_id(1)
        columnTransaction.pack_start(cellTransaction, True)
        columnTransaction.add_attribute(cellTransaction, "text", 0)

        self.treeTransaction.append_column(columnTransaction)

        # self.textTransaction.set_editable(False)

        scrollTransaction.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrollTransaction.add(self.treeTransaction)
        scrollTransaction.set_border_width(4)


        # ------------------------------------------------------------------
        #       Add widgets into main window
        # ------------------------------------------------------------------

        windowGrid.pack_start(self.progressbar, False, True, 16)
        windowGrid.pack_start(labelTransaction, False, True)
        windowGrid.pack_start(scrollTransaction, True, True, 4)

        self.window.addGridWidget(windowGrid)


    def run (self):
        """
        Run transaction and return his state
        """

        # Need to run show_all before to call transaction thread to have a
        # usable interface
        self.window.show_all()

        TransactionThread(self).start()

        self.window.run()

        # Remove interface link
        self.pacman.changeStockedInterface(None)

        return self.state


    def setMaxProgress (self, value):
        """
        Set maximum value for progressbar
        """

        self.index = 0
        self.maxActions = value


    def showMessage (self, message, parent):
        """
        Show a message in the textview
        """

        # self.textTransaction.get_buffer().insert_at_cursor(message)

        row = self.storeTransaction.append(self.rowParent, [message])

        if parent:
            self.rowParent = row

            self.treeTransaction.expand_all()
        else:
            self.rowParent = None


    def updateProgressbar (self, text=""):
        """
        Update progressbar
        """

        progressValue = float(self.index) / self.maxActions

        if progressValue >= 0 and progressValue <= 1.0:
            self.progressbar.set_fraction(progressValue)

        if len(text) > 0:
            self.progressbar.set_text(text)

        self.index += 1


    def closeInstance (self):
        """
        Restore sensitive status for close button
        """

        self.window.setSensitiveButton(gtk.RESPONSE_CLOSE, True)
