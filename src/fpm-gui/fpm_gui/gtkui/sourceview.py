# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Interface
import gtk
import gtksourceview2

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Sourceview (gtk.ScrolledWindow):

    def __init__ (self):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        self.sourceviewBuffer = gtksourceview2.Buffer()

        self.sourceviewLanguage = gtksourceview2.LanguageManager()
        self.sourceviewStyle = gtksourceview2.StyleSchemeManager()

        self.sourceviewView = gtksourceview2.View(self.sourceviewBuffer)
        self.sourceviewCompositor = \
            gtksourceview2.PrintCompositor(self.sourceviewBuffer)

        # Properties
        self.sourceviewCompositor.set_wrap_mode(gtk.WRAP_WORD)

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.add(self.sourceviewView)
        self.set_border_width(4)

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    def __set_content (self, content):
        text = ""

        for line in content:
            if len(line) > 0:
                text += str(line)

        self.sourceviewBuffer.set_text(text)

    def __get_content (self):
        return self.sourceviewBuffer.get_text()

    def __set_language (self, language):
        self.sourceviewBuffer.set_language(
            self.sourceviewLanguage.get_language(language))

    def __get_language (self):
        return self.sourceviewBuffer.get_language()

    def __set_tab_width (self, number=4):
        self.sourceviewCompositor.set_tab_width(number)

    def __get_tab_width (self):
        return self.sourceviewCompositor.get_tab_width()

    def __set_colorscheme (self, colorscheme):
        if not colorscheme == "classic":
            self.sourceviewBuffer.set_style_scheme(
                self.sourceviewStyle.get_scheme(colorscheme))

    def __get_colorscheme (self):
        return self.sourceviewBuffer.get_style_scheme()

    content = property(__get_content, __set_content)
    language = property(__get_language, __set_language)
    tab_width = property(__get_tab_width, __set_tab_width)
    colorscheme = property(__get_colorscheme, __set_colorscheme)

    def line_numbers (self):
        self.sourceviewView.set_show_line_numbers(True)
