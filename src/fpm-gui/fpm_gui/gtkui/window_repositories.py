# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Interface
import gtk

from pango import WRAP_WORD_CHAR

from modules import refresh
from window import Window

# Log
from logging import getLogger

# Modules
from fpm_gui import REGEX_MIRROR_INTEGRITY

from window import WindowPopup
from window import WindowQuestion
from modules import ExtendedListStore

from fpmd import FW_STABLE
from fpmd import FW_CURRENT

from fpmd.tools.utils import *
from fpmd.tools.config import Config
from fpmd.tools.network import Network
from fpmd.tools.package import Database
from fpmd.tools.pacmanconf import *

# Regex
from re import search
from re import findall

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-gui")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowRepositories (Window):

    def __init__ (self, widget=None, parent=None):
        """
        Constructor
        """

        Window.__init__(self, parent, _("Software Sources"),
            _("Manage repositories which contains packages that can be "
            "installed on your system."))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.interface = parent

        self.repositories = dict()

        if parent is not None:
            self.config = parent.pacmanConfig

            self.database = parent.pacman

        else:
            self.config = PacmanConfig()

            self.database = Database()
            self.database.reload_daemon()

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_main_icon(gtk.STOCK_CDROM, gtk.ICON_SIZE_DIALOG)

        self.set_help_informations(_("Repositories help"),
            _("Repositories order is important. Repositories in the top of "
            "the list are privileged towards bottom one."))

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_SAVE, gtk.RESPONSE_APPLY)
        self.set_default_response(gtk.RESPONSE_APPLY)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.VBox()

        boxRepositories = gtk.HBox()
        boxDelay = gtk.HBox()

        view = gtk.Viewport()
        scroll = gtk.ScrolledWindow()

        # Properties
        box.set_spacing(4)
        box.set_border_width(4)

        boxDelay.set_spacing(4)

        view.set_shadow_type(gtk.SHADOW_NONE)

        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scroll.set_border_width(4)

        # ------------------------------------
        #   Repositories - List
        # ------------------------------------

        self.treeviewRepositories = gtk.TreeView(
            ExtendedListStore(bool, str, int))

        columnStatus = gtk.TreeViewColumn()
        columnRepository = gtk.TreeViewColumn()

        self.cellStatus = gtk.CellRendererToggle()
        cellRepository = gtk.CellRendererText()

        scrollRepositories = gtk.ScrolledWindow()

        # Properties
        self.treeviewRepositories.set_search_column(1)
        self.treeviewRepositories.set_headers_visible(False)
        self.treeviewRepositories.set_property("enable-search", False)

        columnStatus.set_sort_column_id(0)
        columnStatus.pack_start(self.cellStatus, False)
        columnStatus.add_attribute(self.cellStatus, "active", 0)

        columnRepository.set_sort_column_id(1)
        columnRepository.pack_start(cellRepository, True)
        columnRepository.add_attribute(cellRepository, "text", 1)

        self.cellStatus.set_property("activatable", True)

        self.treeviewRepositories.append_column(columnStatus)
        self.treeviewRepositories.append_column(columnRepository)

        scrollRepositories.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrollRepositories.add(self.treeviewRepositories)

        # ------------------------------------
        #   Repositories - Move
        # ------------------------------------

        toolbarActions = gtk.Toolbar()

        self.buttonAdd = gtk.ToolButton()
        self.buttonModify = gtk.ToolButton()
        self.buttonRemove = gtk.ToolButton()

        self.buttonTop = gtk.ToolButton()
        self.buttonUp = gtk.ToolButton()
        self.buttonDown = gtk.ToolButton()
        self.buttonBottom = gtk.ToolButton()

        # Properties
        toolbarActions.set_style(gtk.TOOLBAR_ICONS)
        toolbarActions.set_orientation(gtk.ORIENTATION_VERTICAL)

        self.buttonAdd.set_stock_id(gtk.STOCK_ADD)
        self.buttonAdd.set_tooltip_text(_("Add a new repository"))

        self.buttonModify.set_stock_id(gtk.STOCK_EDIT)
        self.buttonModify.set_tooltip_text(_("Edit selected repository"))

        self.buttonRemove.set_stock_id(gtk.STOCK_REMOVE)
        self.buttonRemove.set_tooltip_text(_("Remove selected repository"))

        self.buttonTop.set_stock_id(gtk.STOCK_GOTO_TOP)
        self.buttonTop.set_tooltip_text(
            _("Move selected repository to top list"))

        self.buttonUp.set_stock_id(gtk.STOCK_GO_UP)
        self.buttonUp.set_tooltip_text(_("Up selected repository"))

        self.buttonDown.set_stock_id(gtk.STOCK_GO_DOWN)
        self.buttonDown.set_tooltip_text(_("Down selected repository"))

        self.buttonBottom.set_stock_id(gtk.STOCK_GOTO_BOTTOM)
        self.buttonBottom.set_tooltip_text(
            _("Move selected repository to bottom list"))

        toolbarActions.insert(self.buttonAdd, -1)
        toolbarActions.insert(gtk.SeparatorToolItem(), -1)
        toolbarActions.insert(self.buttonModify, -1)
        toolbarActions.insert(self.buttonRemove, -1)
        toolbarActions.insert(gtk.SeparatorToolItem(), -1)
        toolbarActions.insert(self.buttonTop, -1)
        toolbarActions.insert(self.buttonUp, -1)
        toolbarActions.insert(self.buttonDown, -1)
        toolbarActions.insert(self.buttonBottom, -1)

        # ------------------------------------
        #   Delay option
        # ------------------------------------

        labelDelay = gtk.Label()
        adjustmentDelay = gtk.Adjustment(1, 0, 365, 1, 5)
        self.spinDelay = gtk.SpinButton()
        labelDelayEnd = gtk.Label()

        # Properties
        labelDelay.set_alignment(0, 0.5)
        labelDelay.set_text(_("How long before issue a warning when the "
            "database haven't been updated (days) ?"))
        self.spinDelay.set_adjustment(adjustmentDelay)
        self.spinDelay.set_tooltip_text(_("Issue a warning when you install a "
            "package, but the local sync database haven't been updated for "
            "<x> days. Write 0 to disable."))
        labelDelayEnd.set_alignment(0, 0.5)
        labelDelayEnd.set_text(_("days."))

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        boxRepositories.pack_start(scrollRepositories)
        boxRepositories.pack_start(toolbarActions, False, False)

        boxDelay.pack_start(labelDelay)
        boxDelay.pack_start(self.spinDelay, False, False)

        box.pack_start(boxRepositories)
        box.pack_start(gtk.HSeparator(), False, False, 4)
        box.pack_start(boxDelay, False, False)

        # Add main box into scrollview
        view.add(box)
        scroll.add(view)

        self.add_widget(scroll)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.cellStatus.connect("toggled", self.activate_repository)

        self.treeviewRepositories.connect("button_press_event",
            self.mouse_event)

        self.buttonAdd.connect("clicked", WindowMirrors, self)
        self.buttonModify.connect("clicked", self.modify_repository)
        self.buttonRemove.connect("clicked", self.remove_repository)
        self.buttonTop.connect("clicked", self.move_repository)
        self.buttonUp.connect("clicked", self.move_repository)
        self.buttonDown.connect("clicked", self.move_repository)
        self.buttonBottom.connect("clicked", self.move_repository)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.__get_preferences_values()

        if self.start() == gtk.RESPONSE_APPLY:
            self.__save_configuration()

            if self.interface is not None:
                self.interface.reload_interface()
                self.interface.upgrade_get(True)

        if self.interface is not None:
            self.interface.reload_check_server()
            self.interface.statusbar_update()


    def __save_configuration (self):
        """
        Save user preferences
        """

        reload_interface = False

        self.config.option("OldDelay").value = int(self.spinDelay.get_value())

        self.config.generate_configuration()

        self.database.configuration_update(self.config.hash)
        self.database.reload_daemon()

        return reload_interface


    def __get_preferences_values (self):
        """
        Update preferences
        """

        # Set value for old delay
        delay = self.config.option("OldDelay").value
        if delay is None:
            delay = 3

        self.spinDelay.set_value(float(delay))

        # Set repositories into treeview
        model = self.treeviewRepositories.get_model()

        for repository in self.config.repositories:
            model.append(
                [not repository.commented, repository.name, repository.index])

            self.repositories[repository.name] = repository

    # ------------------------------------
    #   Methods
    # ------------------------------------

    def mouse_event (self, treeview, event):
        """
        Show modification window when user double-click on treeview
        """

        if event.type == gtk.gdk._2BUTTON_PRESS:
            x = int(event.x)
            y = int(event.y)

            selection = treeview.get_path_at_pos(x, y)

            if selection is not None:
                path, col, cellx, celly = selection
                treeview.grab_focus()
                treeview.set_cursor(path, col, 0)

                self.modify_repository(treeview)

            return True


    def modify_repository (self, widget):
        """
        Check if a repo is currently selected
        """

        selection = self.treeviewRepositories.get_selection()
        model, treeiter = selection.get_selected()

        if treeiter is not None:
            repository = self.repositories[model.get_value(treeiter, 1)]

            WindowMirrors(None, self, repository)


    def move_repository (self, widget):
        """
        Check if the mirror is pingable
        """

        selection = self.treeviewRepositories.get_selection()
        model, treeiter = selection.get_selected()

        if treeiter is not None:
            direction = widget.get_stock_id()

            if direction == gtk.STOCK_GOTO_TOP:
                model.move_after(treeiter, None)

            elif direction == gtk.STOCK_GO_UP:
                find_iter = model.iter_previous(treeiter)
                if find_iter is not None:
                    model.swap(treeiter, find_iter)

            elif direction == gtk.STOCK_GO_DOWN:
                find_iter = model.iter_next(treeiter)
                if find_iter is not None:
                    model.swap(treeiter, find_iter)

            elif direction == gtk.STOCK_GOTO_BOTTOM:
                model.move_before(treeiter, None)


    def remove_repository (self, widget):
        """
        Remove a repository
        """

        selection = self.treeviewRepositories.get_selection()
        model, treeiter = selection.get_selected()

        if treeiter is not None:
            name = model.get_value(treeiter, 1)

            if not name == FW_STABLE and not name == FW_CURRENT:
                model.remove(treeiter)

                self.config.repository_remove(self.repositories[name])

            else:
                WindowPopup(_("Remove %s repository" % name),
                    _("This repository cannot be remove !"), gtk.MESSAGE_ERROR)


    def activate_repository (self, cell, row):
        """
        Change the selected repository status
        """

        selection = self.treeviewRepositories.get_selection()
        model, treeiter = selection.get_selected()

        if not treeiter is None:
            commented, name = model[row][0], model[row][1]

            # Check if both stable and current are in the list
            repositories = 0
            for element in model:
                if element[1] == FW_STABLE or element[1] == FW_CURRENT:
                    repositories += 1

            # ------------------------------------
            #   Other repositories
            # ------------------------------------

            if repositories < 2 or (
                not name == FW_STABLE and not name == FW_CURRENT):

                model[row][0] = not model[row][0]

                repository = self.repositories[model[row][1]]
                repository.commented = not model[row][0]

            # ------------------------------------
            #   Frugalware repositories
            # ------------------------------------

            else:
                oldRepo, newRepo = None, None

                # Frugalware stable repository
                if (name == FW_STABLE and not commented) or \
                    (name == FW_CURRENT and commented):
                    oldRepo, newRepo = FW_CURRENT, FW_STABLE

                # Frugalware current repository
                elif (name == FW_CURRENT and not commented) \
                    or (name == FW_STABLE and commented):
                    oldRepo, newRepo = FW_STABLE, FW_CURRENT

                if not oldRepo is None and not newRepo is None:
                    window = WindowQuestion(_("Change main repository"),
                        _("Would you want to disable %(old)s repository and "
                        "use %(new)s instead ?") % dict(
                        old=oldRepo, new=newRepo))

                    if window.start():
                        repository_index = 0

                        for repository in model:
                            if repository[1] == FW_CURRENT:
                                current_index = model[repository_index]

                            elif repository[1] == FW_STABLE:
                                stable_index = model[repository_index]

                            repository_index += 1

                        # ------------------------------------
                        #   Stable repository
                        # ------------------------------------

                        stable_index[0] = not stable_index[0]

                        repository = self.repositories[stable_index[1]]
                        repository.commented = not stable_index[0]

                        # ------------------------------------
                        #   Current repository
                        # ------------------------------------

                        current_index[0] = not current_index[0]

                        repository = self.repositories[current_index[1]]
                        repository.commented = not current_index[0]


class WindowMirrors (Window):

    def __init__ (self, widget, parent, repository=None):
        """
        Constructor
        """

        if repository is not None:
            Window.__init__(self, parent, _("%s repository" % repository.name))

        else:
            Window.__init__(self, parent, _("New repository"))

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.interface = parent

        self.repository = repository

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_default_size(750, 550)

        self.set_main_icon(gtk.STOCK_CDROM, gtk.ICON_SIZE_MENU)

        self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self.add_button(gtk.STOCK_SAVE, gtk.RESPONSE_APPLY)
        self.set_default_response(gtk.RESPONSE_APPLY)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.VBox()

        self.boxRepositoryName = gtk.VBox()
        self.boxMirrorName = gtk.VBox()
        self.boxMirrors = gtk.HBox()

        view = gtk.Viewport()
        scroll = gtk.ScrolledWindow()

        # Properties
        box.set_spacing(8)
        box.set_border_width(4)

        self.boxRepositoryName.set_spacing(4)
        self.boxMirrorName.set_spacing(4)

        view.set_shadow_type(gtk.SHADOW_NONE)

        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scroll.set_border_width(4)

        # ------------------------------------
        #   Repository name
        # ------------------------------------

        labelRepository = gtk.Label()
        self.entryRepository = gtk.Entry()

        self.infoRepository = gtk.InfoBar()
        labelRepositoryError = gtk.Label()

        # Properties
        labelRepository.set_alignment(0, 0.5)
        labelRepository.set_label(_("Repository name"))

        self.entryRepository.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        self.infoRepository.set_message_type(gtk.MESSAGE_ERROR)
        self.infoRepository.get_content_area().add(labelRepositoryError)

        labelRepositoryError.set_label(
            _("This name is already use by another repository"))

        # ------------------------------------
        #   Mirror name
        # ------------------------------------

        labelMirror = gtk.Label()
        self.entryMirror = gtk.Entry()

        self.infoMirror = gtk.InfoBar()
        labelMirrorError = gtk.Label()

        # Properties
        labelMirror.set_alignment(0, 0.5)
        labelMirror.set_label(_("New mirror"))

        self.entryMirror.set_icon_from_stock(0, gtk.STOCK_ADD)
        self.entryMirror.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        self.infoMirror.set_message_type(gtk.MESSAGE_ERROR)
        self.infoMirror.get_content_area().add(labelMirrorError)

        labelMirrorError.set_label(_("The mirror link must be like: "
            "http://hostname/[misc/]frugalware-arch"))

        # ------------------------------------
        #   Mirrors - Treeview
        # ------------------------------------

        self.treeviewMirrors = gtk.TreeView(ExtendedListStore(bool, str))

        columnStatus = gtk.TreeViewColumn()
        columnMirror = gtk.TreeViewColumn()

        self.cellStatus = gtk.CellRendererToggle()
        self.cellMirror = gtk.CellRendererText()

        scrollMirrors = gtk.ScrolledWindow()

        # Properties
        self.treeviewMirrors.set_search_column(1)
        self.treeviewMirrors.set_headers_visible(False)
        self.treeviewMirrors.set_property("enable-search", False)

        columnStatus.set_sort_column_id(0)
        columnStatus.pack_start(self.cellStatus, False)
        columnStatus.add_attribute(self.cellStatus, "active", 0)

        columnMirror.set_sort_column_id(1)
        columnMirror.pack_start(self.cellMirror, True)
        columnMirror.add_attribute(self.cellMirror, "text", 1)

        self.cellStatus.set_property("activatable", True)

        self.cellMirror.set_property("editable", True)

        self.treeviewMirrors.append_column(columnStatus)
        self.treeviewMirrors.append_column(columnMirror)

        scrollMirrors.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrollMirrors.add(self.treeviewMirrors)

        # ------------------------------------
        #   Mirrors - Tools
        # ------------------------------------

        toolbarActions = gtk.Toolbar()

        self.buttonCheck = gtk.ToolButton()
        self.buttonRemove = gtk.ToolButton()

        self.buttonTop = gtk.ToolButton()
        self.buttonUp = gtk.ToolButton()
        self.buttonDown = gtk.ToolButton()
        self.buttonBottom = gtk.ToolButton()

        # Properties
        toolbarActions.set_style(gtk.TOOLBAR_ICONS)
        toolbarActions.set_orientation(gtk.ORIENTATION_VERTICAL)

        self.buttonCheck.set_stock_id(gtk.STOCK_CONNECT)
        self.buttonCheck.set_tooltip_text(_("Check selected mirror"))

        self.buttonRemove.set_stock_id(gtk.STOCK_REMOVE)
        self.buttonRemove.set_tooltip_text(_("Remove selected mirror"))

        self.buttonTop.set_stock_id(gtk.STOCK_GOTO_TOP)
        self.buttonTop.set_tooltip_text(
            _("Move selected mirror to top list"))

        self.buttonUp.set_stock_id(gtk.STOCK_GO_UP)
        self.buttonUp.set_tooltip_text(_("Up selected mirror"))

        self.buttonDown.set_stock_id(gtk.STOCK_GO_DOWN)
        self.buttonDown.set_tooltip_text(_("Down selected mirror"))

        self.buttonBottom.set_stock_id(gtk.STOCK_GOTO_BOTTOM)
        self.buttonBottom.set_tooltip_text(
            _("Move selected mirror to bottom list"))

        toolbarActions.insert(self.buttonCheck, -1)
        toolbarActions.insert(self.buttonRemove, -1)
        toolbarActions.insert(gtk.SeparatorToolItem(), -1)
        toolbarActions.insert(self.buttonTop, -1)
        toolbarActions.insert(self.buttonUp, -1)
        toolbarActions.insert(self.buttonDown, -1)
        toolbarActions.insert(self.buttonBottom, -1)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        self.boxRepositoryName.pack_start(labelRepository, False, False)
        self.boxRepositoryName.pack_start(self.entryRepository)

        self.boxMirrorName.pack_start(labelMirror, False, False)
        self.boxMirrorName.pack_start(self.entryMirror)

        self.boxMirrors.pack_start(scrollMirrors)
        self.boxMirrors.pack_start(toolbarActions, False, False)

        if self.repository is None:
            box.pack_start(self.boxRepositoryName, False, False)
            box.pack_start(self.infoRepository, False, False)
            box.pack_start(gtk.HSeparator(), False, False)

        box.pack_start(self.boxMirrorName, False, False)
        box.pack_start(self.infoMirror, False, False)
        box.pack_start(self.boxMirrors)

        view.add(box)
        scroll.add(view)

        self.add_widget(scroll)


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.entryRepository.connect("changed", self.check_repository)
        self.entryRepository.connect("icon-press", self.check_repository)

        self.entryMirror.connect("activate", self.check_mirror)
        self.entryMirror.connect("icon-press", self.check_mirror)

        self.cellStatus.connect("toggled", self.activate_mirror)
        self.cellMirror.connect("edited", self.modify_mirror)

        self.buttonCheck.connect("clicked", self.ping_mirror)
        self.buttonRemove.connect("clicked", self.remove_mirror)
        self.buttonTop.connect("clicked", self.move_mirror)
        self.buttonUp.connect("clicked", self.move_mirror)
        self.buttonDown.connect("clicked", self.move_mirror)
        self.buttonBottom.connect("clicked", self.move_mirror)


    def __start_interface (self):
        """
        Load all interface elements
        """

        self.show_all()

        self.infoRepository.hide()
        self.infoMirror.hide()

        if self.repository is not None:
            self.__get_preferences_values()

        if self.run() == gtk.RESPONSE_APPLY:
            self.__save_configuration()

        self.destroy()


    def __save_configuration (self):
        """
        Save user preferences
        """

        mirrors_list = list()

        # Get all the mirrors in treeview
        model = self.treeviewMirrors.get_model()
        for row in model:
            status, name = model.get(row.iter, 0, 1)
            mirrors_list.append(PacmanOption(name, not status))

        # Create a new repository
        if self.repository is None:
            repository_name = self.entryRepository.get_text()

            self.repository = PacmanRepository(-1, repository_name, False)

            model = self.interface.treeviewRepositories.get_model()
            model.insert(-1, [True, repository_name, -1])

            self.interface.repositories[repository_name] = self.repository

            self.interface.config.repository_add(self.repository)

        self.repository.mirrors = mirrors_list


    def __get_preferences_values (self):
        """
        Update preferences
        """

        for mirror in self.repository.mirrors:
            model = self.treeviewMirrors.get_model()
            model.append([not mirror.commented, mirror.value])


    def check_repository (self, widget, pos=None, event=None):
        """
        Check if a name is already use by another repository
        """

        if pos is None:
            repository = widget.get_text()
            repositories = self.interface.config.repositories

            self.set_sensitive(gtk.RESPONSE_APPLY, False)

            if any(x.name == repository for x in repositories):
                self.infoRepository.show()

            else:
                self.infoRepository.hide()

                if len(repository) > 0:
                    self.set_sensitive(gtk.RESPONSE_APPLY, True)

        elif pos == 1:
            self.infoRepository.hide()

            widget.set_text('')


    def check_mirror (self, widget, pos=0, event=None):
        """
        Check if a mirror can be using by pacman-g2
        """

        if pos == 0:
            url = widget.get_text()

            if search(REGEX_MIRROR_INTEGRITY, url) is None:
                self.infoMirror.show()

            else:
                self.infoMirror.hide()

                if len(url) > 0:
                    model = self.treeviewMirrors.get_model()
                    model.append([True, url])

                    widget.set_text('')

        elif pos == 1:
            self.infoMirror.hide()

            widget.set_text('')


    def move_mirror (self, widget):
        """
        Check if the mirror is pingable
        """

        model, treeiter = self.treeviewMirrors.get_selection().get_selected()

        if treeiter is not None:
            direction = widget.get_stock_id()

            if direction == gtk.STOCK_GOTO_TOP:
                model.move_after(treeiter, None)

            elif direction == gtk.STOCK_GO_UP:
                find_iter = model.iter_previous(treeiter)
                if find_iter is not None:
                    model.swap(treeiter, find_iter)

            elif direction == gtk.STOCK_GO_DOWN:
                find_iter = model.iter_next(treeiter)
                if find_iter is not None:
                    model.swap(treeiter, find_iter)

            elif direction == gtk.STOCK_GOTO_BOTTOM:
                model.move_before(treeiter, None)


    def remove_mirror (self, widget):
        """
        Remove a repository
        """

        model, treeiter = self.treeviewMirrors.get_selection().get_selected()
        if treeiter is not None:
            model.remove(treeiter)


    def modify_mirror (self, widget, row, text):
        """
        Valid change for edited url
        """

        model, treeiter = self.treeviewMirrors.get_selection().get_selected()
        if treeiter is not None:
            model[row][1] = text


    def ping_mirror (self, widget):
        """
        Check if the mirror is pingable
        """

        model, treeiter = self.treeviewMirrors.get_selection().get_selected()

        if treeiter is not None:
            status, name = model.get(treeiter, 0, 1)

            if Network(name).checkUrl(10):
                WindowPopup(_("Mirror status"),
                    _("No problem with the selected mirror"))

            else:
                model[treeiter][0] = False

                WindowPopup(_("Mirror status"),
                    _("The selected mirror can't be access"),
                    gtk.MESSAGE_ERROR)


    def activate_mirror (self, widget, row):
        """
        Change the selected mirror status
        """

        model, treeiter = self.treeviewMirrors.get_selection().get_selected()
        if treeiter is not None:
            model[row][0] = not model[row][0]
