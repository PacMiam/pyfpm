#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages

from os.path import join
from os.path import dirname

setup(
    name = "fpm-gui",
    version = "1.3rc2",
    author = "PacMiam",
    author_email = "PacMiam@gmx.fr",
    description = "Graphical packages manager for Frugalware",
    url = 'https://frugalware.org',
    license = "GPLv3",

    packages = ['fpm_gui', 'fpm_gui.gtkui', 'fpm_gui.data'],
    include_package_data = True,

    entry_points = {
        'console_scripts': [
            'fpm-gui = fpm_gui.main:main',
        ],
    },

    classifiers = [
        "Programming Language :: Python :: 2.7",
        "Development Status :: Stable",
        "License :: GPL3",
        "Operating System :: GNU/Linux",
        "Topic :: Communications",
        "Topic :: Utilities",
    ],
)

