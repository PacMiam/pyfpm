# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           network.py
#
#  Network manager
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import version_info

if(version_info[0] < 3):
    from urllib2 import Request
    from urllib2 import urlopen
    from urllib2 import URLError

else:
    from urllib.request import Request
    from urllib.request import urlopen
    from urllib.request import urlretrieve
    from urllib.error import URLError

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Network (object):

    def __init__ (self, url):
        """
        Constructor

        :return: Object Network
        :rtype: Network
        """

        self.url = url


    def checkUrl (self, timeout=10):
        """
        Try to open an url and get his status

        :return: Url status
        :rtype: bool
        """

        try:
            handle = urlopen(Request(self.url), timeout=timeout)
            return True

        except:
            return False


    def downloadContent (self, target=".", timeout=10):
        """
        Download content from url

        :param str target: Element to download (default ".")

        :return: None
        """

        if(version_info[0] < 3):

            try:
                webFile = urlopen(self.url, timeout=timeout)

                localFile = open(target, 'w')
                localFile.write(webFile.read())

                webFile.close()
                localFile.close()

                return True

            except URLError as err:
                return False

        else:
            try:
                webFile = urlretrieve(self.url, target)
                return True

            except URLError as err:
                return False
