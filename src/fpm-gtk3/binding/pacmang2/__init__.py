# -*- coding: utf-8 -*-

#  ----------------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ----------------------------------------------------------------------------

#  ----------------------------------------------------------------------------
#    Modules
#  ----------------------------------------------------------------------------

from enum import Enum

#  ----------------------------------------------------------------------------
#    Enum
#  ----------------------------------------------------------------------------

class Fpmd(Enum):
    BusName = "org.frugalware.fpmd"
    BusPath = "/org/frugalware/fpmd/object"
    Stable = "frugalware"
    Current = "frugalware-current"


class Groups(Enum):
    Applications = ("applications-office", "(^[x]?apps.*)")
    Chroot = ("security-high", "(^fw32.*)")
    Development = ("applications-development", "(^devel.*)")
    Documentation = ("system-help", "(^docs.*)")
    Enlightenment = ("package-x-generic", "(^e17.*)")
    Fonts = ("preferences-desktop-font", "(^.*fonts.*)")
    Games = ("applications-games", "(^games.*)")
    Gnome = ("package-x-generic", "(^gnome.*)")
    Kde = ("package-x-generic", "(^kde.*)")
    Library = ("applications-utilities", "(^compat-.*|^[x]?lib.*|^qt.*-libs)")
    Locales = ("preferences-desktop-locale", "(^locale.*)")
    Lxde = ("package-x-generic", "(^lxde.*)")
    Mate = ("package-x-generic", "(^mate.*)")
    Multimedia = ("applications-multimedia", "(^[x]?multimedia.*)")
    Network = ("applications-internet", "(^network.*)")
    System = ("utilities-system-monitor", "(^base.*|^core.*|^chroot.*)")
    Xfce = ("package-x-generic", "(^xfce4.*)")
    Xorg = ("video-display", "(^x11.*|^xorg-(?!fonts).*)")
