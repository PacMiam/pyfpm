# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           utils.py
#
#  Miscellaneous functions
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os import remove
from os import listdir
from os import environ
from os.path import join
from os.path import isdir
from os.path import isfile
from os.path import exists
from os.path import getsize
from os.path import basename

from sys import version_info
from glob import glob
from struct import unpack
from codecs import open as codecOpen

# Logging
from logging import INFO
from logging import DEBUG
from logging import ERROR
from logging import Formatter
from logging import FileHandler
from logging import StreamHandler

# Package
from pkg_resources import require
from pkg_resources import _manager

# Regex
from re import split
from re import search

# System
from subprocess import Popen
from subprocess import PIPE
from sys import version_info

# Translation
from gettext import gettext
from gettext import textdomain
from gettext import bindtextdomain

# ------------------------------------------------------------------
#   Fix
# ------------------------------------------------------------------

if(version_info[0] >= 3):
    long = int

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def file_handler (logger, path):
    """
    Set log file for a specific Logger

    :param Logger logger: Logger
    :param str path: Log file path
    """

    handler = FileHandler(path)
    handler.setFormatter(
        Formatter("%(levelname)s » %(asctime)s - %(funcName)s » %(message)s",
        datefmt="%D %H:%M"))
    handler.setLevel(ERROR)

    return handler


def console_handler (logger, debug=False):
    """
    Set log file for a specific Logger

    :param Logger logger: Logger
    :param str path: Log file path
    """

    handler = StreamHandler()
    handler.setFormatter(Formatter("%(levelname)s » %(message)s"))

    if debug:
        handler.setLevel(DEBUG)
    else:
        handler.setLevel(INFO)

    return handler


def getTranslation (eggName):
    """
    Return the translation

    :param str apps: Application name

    :return: Object Gettext
    :rtype: Gettext
    """

    try:
        bindtextdomain(eggName, "/usr/share/locale")
        textdomain(eggName)

        return gettext

    except:
        return gettext


def color (text, value="white", bold=False):
    """
    Return the right escape sequence color

    :param str text: Text to show with selected color
    :param str value: Color name (default white)
    :param bool bold: Get bold color (default False)

    :return: Escape sequence
    :rtype: str
    """

    colors = dict(black="30", red="31", green="32", yellow="33", blue="34",
        purple="35", cyan="36", grey="37", white="38")

    boldValue = "0"
    if bold:
        boldValue = "1"

    return ("\033[%s;%sm%s\033[0;38m" % (boldValue, colors.get(value), text))


def splitRegex (pattern, value):
    """
    Split the following string : [repo] name

    :param str value: Specific string to split

    :return: Split string
    :rtype: list (str repositoryName, str packageName)
    """

    result = split(pattern, value)

    if len(result) > 1:
        return result[1:-1]

    return result


def check_xorg ():
    """
    Check if Xorg server is running

    :return: X server status
    :rtype: bool
    """

    try:
        if not environ["DISPLAY"] == None:
            return True

        else:
            return False

    except:
        return False


def getPixmap (eggName, path):
    """
    Provides easy access to data in a python egg

    Thanks Deluge :)
    """

    return require(eggName)[0].get_resource_filename(
        _manager, join(*(eggName.split('.') + [path])))


def getType (value):
    """
    Return correct type for value
    """

    if not value == None:

        # int
        if not search("^\d+$", str(value)) == None:
            return int(value)

        # float
        elif not search("^\d+\.\d*$", str(value)) == None:
            return float(value)

        # boolean
        elif value in ["True", "true"]:
            return True

        elif value in ["False", "false"]:
            return False

    return value


def checkString (value):
    """
    Replace specific characters by html codes
    """

    if not value == None:

        correspondance = [["&", "&amp;"], ["<", "&lt;"], [">", "&gt;"]]

        for code in correspondance:
            value = value.replace(code[0], code[1])

    return value


def formatSize (value):
    """
    Format specific value to MB size
    """

    return format(float(long(value)/1024)/1024, '.2f')


def getFolderContent (path, regex="*", split=False):
    """
    Get all the files in a specific folder path

    :param str path: File path
    :param str  regex: Regex use to obtain files list
    :param bool split: If True, get only file name instead of full path

    :return: Folder files list
    :rtype: list (str fileName)
    """

    if self.check:

        files = glob("%s%s" % (str(path), str(regex)))

        if split:
            for element in files:
                files[files.index(element)] = basename(element)

        return files


def getFolderSize (path):
    """
    Get folder size capacity

    :return: Folder capacity
    :rtype: int
    """

    total_size = getsize(path)

    for item in listdir(path):

        itempath = join(path, item)

        if isfile(itempath):
            total_size += getsize(itempath)

        elif isdir(itempath):
            total_size += getFolderSize(itempath)

    return total_size


def cleanFolder (path, regex="*"):
    """
    Delete all the files in folder path for a specific regex

    :param str path: File path
    :param str regex: Regex use to remove specific files in folder

    :return: None
    """

    if exists(path):
        for element in glob("%s%s" % (str(path), str(regex))):
            remove(element)


def getPermissionFromOctect (value):
    """
    Get a string like "-r-xr--r--" with 0544

    :param int value: Unix permissions

    :return: Unix permissions string
    :rtype: str
    """

    permsList = ["---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx"]

    text = "d"
    if value[0] == "0":
        text = "-"

    # Fix for python 3
    x = 2
    if(version_info[0] < 3):
        x = 1

    for number in value[x:]:
        text += str(permsList[int(number)])

    return text



def getSizeFromImage (path):
    """
    Get image size.

    Based on http://markasread.net/post/17551554979/get-image-size-info-using-\
        pure-python-code

    :param str path: Image path

    :return: Width and Height
    :rtype: int, int
    """

    data = open(path, "rb").read()

    data = str(data)
    size = len(data)

    # Gif image
    if (size >= 10) and data[:6] in ('GIF87a', 'GIF89a'):
        w, h = unpack("<HH", data[6:10])

        return int(w), int(h)

    # Png image
    elif ((size >= 24) and data.startswith('\211PNG\r\n\032\n') \
        and (data[12:16] == 'IHDR')):
        w, h = unpack(">LL", data[16:24])

        return int(w), int(h)

    # Old png image
    elif (size >= 16) and data.startswith('\211PNG\r\n\032\n'):
        w, h = unpack(">LL", data[8:16])

        return int(w), int(h)

    # Jpeg image
    elif (size >= 2) and data.startswith('\377\330'):
        jpeg = StringIO(data)
        jpeg.read(2)

        b = jpeg.read(1)

        try:

            while (b and ord(b) != 0xDA):

                while (ord(b) != 0xFF):
                    b = jpeg.read

                while (ord(b) == 0xFF):
                    b = jpeg.read(1)

                if (ord(b) >= 0xC0 and ord(b) <= 0xC3):
                    jpeg.read(3)

                    h, w = unpack(">HH", jpeg.read(4))
                    break

                else:
                    jpeg.read(int(unpack(">H", jpeg.read(2))[0])-2)

                b = jpeg.read(1)

            return int(w), int(h)

        except:
            pass

    return -1, -1


def writeFile (path, content, mode='w'):
    """
    Write content into file

    :param str path: File path
    :param str content: Text buffer
    :param char mode: File open mode (w/a)
    """

    pipe = open(path, mode)

    pipe.write(str(content))

    pipe.close


def readFile (path):
    """
    Get file content

    :param str path: File path

    :return: text buffer
    :rtype: str
    """

    pipe = open(path, 'r')
    pipe.seek(0)

    content = ''.join(pipe.readlines())

    pipe.close()

    return content
