# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           config.py
#
#  Configuration parser
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import version_info

# Log
from logging import getLogger
from logging.config import fileConfig

# Fpmd
from fpmd.tools.utils import getType
from fpmd.tools.utils import getTranslation

# ------------------------------------------------------------------
#   Fix
# ------------------------------------------------------------------

if(version_info[0] >= 3):
    from configparser import SafeConfigParser
    from configparser import Error as ConfigError
    from configparser import MissingSectionHeaderError

else:
    from ConfigParser import SafeConfigParser
    from ConfigParser import Error as ConfigError
    from ConfigParser import MissingSectionHeaderError

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpmd")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Config (object):

    def __init__ (self, path):
        """
        Constructor
        """

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.__data = dict()

        self.__path = path

        # ------------------------------------
        #   Log
        # ------------------------------------

        self.__log = getLogger()

        # ------------------------------------
        #   Read configuration
        # ------------------------------------

        self.__read_config()

    # ------------------------------------
    #   Getter/Setter
    # ------------------------------------

    def get (self, key, option=None):

        if key in self.__data.keys():

            if option is None:
                return self.__data[key]

            elif option in self.__data[key]:
                return getType(self.__data[key][option])

        return None


    def set (self, key, option, value):

        if key in self.__data.keys():
            self.__data[key][option] = str(value)

        else:
            raise (KeyError, key)


    @property
    def data (self):
        return self.__data

    @property
    def sections (self):
        return self.__data.keys()

    # ------------------------------------
    #   Methods
    # ------------------------------------

    def __read_config (self):
        """
        Read configuration file and store data into a dictionnary
        """

        try:
            parser = SafeConfigParser()
            parser.read(self.__path)

            for section in parser.sections():
                self.__data[section] = dict(parser.items(section))

        except MissingSectionHeaderError:
            self.__log.error(_("Missing section into configuration file"))

        except Exception as error:
            self.__log.error(
                _("Cannot parse configuration file: %s" % str(error)))


    def write_configuration (self):
        """
        Write data into configuration file
        """

        if len(self.__data) > 0:

            parser = SafeConfigParser()

            # Set sections
            for section in self.__data.keys():
                parser.add_section(section)

                # Set options for current section
                for option in self.__data[section]:
                    parser.set(section, option, self.__data[section][option])

            with open(self.__path, 'w') as pipe:
                parser.write(pipe)


    def update_configuration (self, data):
        """
        Update configuration file with default values

        :param dict data: Default values
        """

        for section in data.keys():

            # Add new sections
            self.add_section(section, data[section])

            # Add new options
            for option in data[section].keys():
                if not option in self.__data[section]:
                    self.__data[section][option] = str(data[section][option])


    def add_section (self, key, value=dict()):
        """
        Add a new section into configuration file

        :param str key: New section to add
        :param str value: Section value (Default: dict())
        """

        if not key in self.__data.keys():
            self.__data[key] = value
