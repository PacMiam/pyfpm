# -*- coding: utf-8 -*-

#  ----------------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ----------------------------------------------------------------------------

#  ----------------------------------------------------------------------------
#    Modules
#  ----------------------------------------------------------------------------

# Interpreter
from binding.pacmang2 import *
from binding.pacmang2.package import Database

# Regex
from re import match

# Thread
from gi.repository.GObject import idle_add
from gi.repository.GObject import source_remove

#  ----------------------------------------------------------------------------
#    Class
#  ----------------------------------------------------------------------------

class Interpreter(object):

    def __init__ (self, interface):
        """
        Constructor
        """

        self.database = Database()

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.interface = interface("Pacman-G2 binding")

        self.groups_data = dict()

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.__start_interface()


    def __generate_groups_data(self):
        """
        Generate data for generic groups
        """

        for group in Groups:
            icon, regex = group.value

            self.groups_data[group.name] = dict(
                icon=icon, regex=regex, groups=list())

            self.interface.append_group(icon, group.name)

        for group in self.database.groups(Fpmd.Current.value):
            for key in self.groups_data.keys():
                if match(self.groups_data[key]["regex"], str(group)):
                    self.groups_data[key]["groups"].append(str(group))
                    break


    def load_packages(self, repository, group):
        """
        Load packages from a specific group into treeview
        """

        yield True

        packages = self.database.packages(repository, group)

        for package in packages:

            yield False

            # Check package status
            status = False
            if self.database.package_installed(package)[0]:
                status = True

            # Get package informations
            data = self.database.package(repository, package)

            # Add package into treeview
            self.interface.append_package(status, u"<b>%s</b> - %s\n%s" % (
                data.name, data.version, data.description))

            if ((packages.index(package) + 1) % 5 == 0):
                yield True

        yield False


    def start_thread(self, loader, thread=None):
        """
        Start a gobject thread
        """

        if thread is not None and not thread == 0:
            source_remove(thread)

        thread = idle_add(loader)


    def __start_interface(self):
        """
        Start interface with current binding
        """

        self.__generate_groups_data()

        self.database.packages_installed()

        self.start_thread(
            self.load_packages(Fpmd.Current.value, "xapps-extra"))

        self.interface.start_interface()

