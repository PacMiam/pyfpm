#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

#  ----------------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ----------------------------------------------------------------------------

#  ----------------------------------------------------------------------------
#    Modules
#  ----------------------------------------------------------------------------

# System
from sys import exit
from sys import argv
from pkgutil import find_loader
from argparse import ArgumentParser

# Binding
import binding

# Display
from ui.interface import MainWindow

#  ----------------------------------------------------------------------------
#    Functions
#  ----------------------------------------------------------------------------

def main(*args):

    # ------------------------------------
    #   Arguments
    # ------------------------------------

    if len(argv) >= 1:

        parser = ArgumentParser(description="Graphical packages manager",
            epilog="(C) 2015 Frugalware Developer Team (GPLv3)",
            conflict_handler="resolve")

        parser.add_argument("-v", "--version", action="version",
            version="fpm-gtk3 0.1 Licence GPLv3",
            help="show the current version")
        parser.add_argument("binding", action="store",
            help="binding to use")

        args = parser.parse_args()

    # ------------------------------------
    #   Binding
    # ------------------------------------

    # try:
    loader = find_loader("binding.%s.interpreter" % args.binding)
    loader.load_module().Interpreter(MainWindow)

    # except ImportError:
        # exit("Cannot load %s binding" % args.binding)


if __name__ == "__main__":
    main()
