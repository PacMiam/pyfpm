# -*- coding: utf-8 -*-

#  ----------------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ----------------------------------------------------------------------------

#  ----------------------------------------------------------------------------
#    Modules
#  ----------------------------------------------------------------------------

# System
from gi import require_version

require_version('Gtk', '3.0')

# Display
from ui import *

from gi.repository import Gtk
from gi.repository.GdkPixbuf import Pixbuf

#  ----------------------------------------------------------------------------
#    Class
#  ----------------------------------------------------------------------------

class MainWindow(Gtk.Window):

    def __init__(self, subtitle):
        """
        Constructor
        """

        Gtk.Window.__init__(self)

        # ==========================================
        #   Variable
        # ==========================================

        self.subtitle = subtitle

        self.informations_function = None

        # ==========================================
        #   Initialize Window
        # ==========================================

        self.__init_widgets()
        self.__init_signals()


    def __init_widgets(self):
        """
        Initialize main widgets
        """

        # ==========================================
        #   Window
        # ==========================================

        self.set_title("Packages")
        self.set_icon_name(Icon.Package.value)

        self.set_resizable(True)
        self.set_default_size(1024, 800)

        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)

        self.set_wmclass("gem", "gem")

        # ==========================================
        #   Grids
        # ==========================================

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.box_buttons = Gtk.Box()
        self.box_left_side = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.box_right_side = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.paned = Gtk.HPaned()

        # Properties
        Gtk.StyleContext.add_class(
            self.box_buttons.get_style_context(), "linked")

        self.box_left_side.set_spacing(8)
        self.box_left_side.set_border_width(4)
        self.box_right_side.set_spacing(8)
        self.box_right_side.set_border_width(4)

        self.paned.set_position(250)

        # ==========================================
        #   Header
        # ==========================================

        self.header = Gtk.HeaderBar()

        # Properties
        self.header.set_title("Packages")
        self.header.set_subtitle(self.subtitle)
        self.header.set_show_close_button(True)
        self.header.set_decoration_layout(":close")

        # ==========================================
        #   Header toolbar
        # ==========================================

        self.header_apply = Gtk.Button()
        self.header_reset = Gtk.Button()
        self.header_update = Gtk.Button()

        # Properties
        self.header_apply.add(Gtk.Image.new_from_icon_name(
            "system-software-install", Gtk.IconSize.SMALL_TOOLBAR))
        self.header_reset.add(Gtk.Image.new_from_icon_name(
            "edit-clear", Gtk.IconSize.SMALL_TOOLBAR))
        self.header_update.add(Gtk.Image.new_from_icon_name(
            "system-software-update", Gtk.IconSize.SMALL_TOOLBAR))

        # ==========================================
        #   Header menu
        # ==========================================

        self.header_menu = Gtk.MenuButton()

        self.menu = Gtk.Menu()
        self.label_about = Gtk.Label()
        self.menu_about = Gtk.ImageMenuItem()
        self.label_quit = Gtk.Label()
        self.menu_quit = Gtk.ImageMenuItem()

        # Properties
        self.header_menu.add(Gtk.Image.new_from_icon_name(
            "applications-system", Gtk.IconSize.SMALL_TOOLBAR))

        self.menu_about.set_label("About")
        self.menu_about.set_image(Gtk.Image.new_from_icon_name(
            "help-about", Gtk.IconSize.MENU))
        self.menu_quit.set_label("Quit")
        self.menu_quit.set_image(Gtk.Image.new_from_icon_name(
            "application-exit", Gtk.IconSize.MENU))

        self.header_menu.set_popup(self.menu)

        # ==========================================
        #   Informations bar
        # ==========================================

        self.informations = Gtk.InfoBar()

        self.image_informations = Gtk.Image()
        self.label_informations = Gtk.Label()

        # Properties
        self.label_informations.set_use_markup(True)

        # ==========================================
        #   Search
        # ==========================================

        self.entry_search = Gtk.Entry()

        # Properties
        self.entry_search.set_placeholder_text("Search packages")
        self.entry_search.set_icon_from_icon_name(1, "edit-clear")

        # ==========================================
        #   Treeview groups
        # ==========================================

        self.treeview_groups = ScrolledTreeview([
            Cell.Pixbuf, Cell.Text ])

        # Properties
        self.treeview_groups.set_headers_visible(False)
        self.treeview_groups.set_column_properties(
            0, sortable=False, size=Gtk.IconSize.DND)
        self.treeview_groups.set_column_properties(
            1, "Name", True)

        # ==========================================
        #   Treeview packages
        # ==========================================

        self.treeview_packages = ScrolledTreeview([
            Cell.Toggle, Cell.Pixbuf, Cell.Text ])

        # Properties
        self.treeview_packages.set_column_properties(
            0, sortable=False)
        self.treeview_packages.set_column_properties(
            1, sortable=False, size=Gtk.IconSize.DIALOG)
        self.treeview_packages.set_column_properties(
            2, "Package", True)

        # ==========================================
        #   Add widgets into interface
        # ==========================================

        self.add(self.box)

        # Window
        self.set_titlebar(self.header)

        self.box.pack_start(self.informations, False, True, 0)
        self.box.pack_start(self.paned, True, True, 0)

        # Header
        self.box_buttons.pack_end(self.header_reset, False, True, 0)
        self.box_buttons.pack_end(self.header_apply, False, True, 0)

        self.header.pack_start(self.box_buttons)
        self.header.pack_start(self.header_update)

        self.header.pack_end(self.header_menu)

        # Menu
        self.menu.insert(self.menu_about, -1)
        self.menu.insert(self.menu_quit, -1)

        # Informations bar
        self.informations.get_content_area().add(self.image_informations)
        self.informations.get_content_area().add(self.label_informations)

        # Grid
        self.box_left_side.pack_start(self.entry_search, False, True, 0)
        self.box_left_side.pack_start(self.treeview_groups, True, True, 0)
        self.box_right_side.pack_start(self.treeview_packages, True, True, 0)

        self.paned.add1(self.box_left_side)
        self.paned.add2(self.box_right_side)


    def __init_signals(self):
        """
        Initialize main signals
        """

        # ==========================================
        #   Window
        # ==========================================

        self.connect("delete-event", self.stop_interface)

        # ==========================================
        #   Informations bar
        # ==========================================

        self.informations.connect("response", self.close_informations)

        # ==========================================
        #   Search
        # ==========================================

        self.entry_search.connect("icon_press", self.clear_entry)


    def start_interface(self):
        """
        Start program correctly
        """

        self.informations.set_no_show_all(True)
        self.menu.show_all()
        self.show_all()

        Gtk.main()


    def stop_interface(self, widget, event):
        """
        Quit program correctly
        """

        Gtk.main_quit()


    def open_informations(self, text=None, message=Gtk.MessageType.INFO,
        buttons=None):
        """
        Show a banner with some informations to user
        """

        if not text is None:
            # FIXME: During my tests, infobar didn't change background color
            self.informations.set_message_type(message)

            self.label_informations.set_markup(text)

            # ==========================================
            #   Informations icon
            # ==========================================

            if message == Gtk.MessageType.WARNING:
                self.image_informations.set_from_icon_name(
                    Icon.Warning.value, Gtk.IconSize.DND)

            elif message == Gtk.MessageType.QUESTION:
                self.image_informations.set_from_icon_name(
                    Icon.Question.value, Gtk.IconSize.DND)

            elif message == Gtk.MessageType.ERROR:
                self.image_informations.set_from_icon_name(
                    Icon.Error.value, Gtk.IconSize.DND)

            else:
                self.image_informations.set_from_icon_name(
                    Icon.Information.value, Gtk.IconSize.DND)

            # ==========================================
            #   Action buttons
            # ==========================================

            if buttons is not None and type(buttons) is list:
                self.informations.set_show_close_button(False)

                for button in buttons:
                    self.informations.add_button(button[0], button[1])

            else:
                self.informations.set_show_close_button(True)

            self.informations.show()
            self.informations.get_content_area().show_all()

        else:
            self.informations.hide()


    def close_informations(self, widget=None, response_id=None):
        """
        Chose informations bar
        """

        if response_id == Gtk.ResponseType.ACCEPT and \
            self.informations_function is not None:
            self.informations_function()

        elif response_id == Gtk.ResponseType.CANCEL:
            self.informations_function = None

        self.informations.hide()


    def clear_groups(self):
        """
        Clear groups treeview
        """

        self.treeview_groups.clear()


    def append_package(self, status, package):
        """
        Append a group in groups treeview
        """

        self.treeview_packages.append([status, Icon.Package.value, package])


    def append_group(self, icon, group):
        """
        Append a group in groups treeview
        """

        self.treeview_groups.append([icon, group])


    def clear_entry(self, widget, icon, event):
        """
        Clear an entry widget
        """

        if icon == Gtk.EntryIconPosition.SECONDARY:
            widget.set_text(str())


class ScrolledTreeview(Gtk.ScrolledWindow):

    def __init__(self, columns_type):
        """
        Constructor
        """

        Gtk.ScrolledWindow.__init__(self)

        # ==========================================
        #   Initialize variables
        # ==========================================

        if type(columns_type) is not list:
            raise(ValueError("Expected a list not a %s" % type(columns_type)))

        self.columns_type = columns_type

        # ==========================================
        #   Initialize Window
        # ==========================================

        self.__init_widgets()


    def __init_widgets(self):
        """
        Initialize main widgets
        """

        # ==========================================
        #   Model store
        # ==========================================

        data = list()

        for column in self.columns_type:
            if column in [Cell.Combo, Cell.Pixbuf, Cell.Text]:
                data.append(str)
            elif column in [Cell.Toggle]:
                data.append(bool)
            elif column in [Cell.Progress, Cell.Spin]:
                data.append(float)

        self.store = Gtk.ListStore.new(tuple(data))

        # ==========================================
        #   Treeview
        # ==========================================

        self.treeview = Gtk.TreeView()

        # Properties
        self.treeview.set_model(self.store)

        # ==========================================
        #   Add widgets into interface
        # ==========================================

        self.add(self.treeview)

        self.show_all()


    def set_headers_visible(self, value):
        """
        Show or hide treeview header
        """

        self.treeview.set_headers_visible(value)


    def set_column_properties(self, index, title=str(), expand=False,
        sortable=True, size=Gtk.IconSize.SMALL_TOOLBAR):
        """
        Append a column into treeview
        """

        if not index in range(len(self.columns_type)):
            raise(ValueError("Index not in columns range"))

        column_type = self.columns_type[index]

        # ==========================================
        #   Column
        # ==========================================

        column = Gtk.TreeViewColumn()

        # Properties
        column.set_title(title)
        column.set_expand(expand)

        if sortable:
            column.set_sort_column_id(index)

        # ==========================================
        #   Cell
        # ==========================================

        if column_type == Cell.Toggle:
            cell = Gtk.CellRendererToggle()

            # Properties
            column.pack_start(cell, True)
            column.set_attributes(cell, active=index)

        elif column_type == Cell.Progress:
            cell = Gtk.CellRendererProgress()

            # Properties
            column.pack_start(cell, True)

        elif column_type == Cell.Pixbuf:
            cell = Gtk.CellRendererPixbuf()

            # Properties
            column.pack_start(cell, True)
            column.add_attribute(cell, "icon-name", index)
            cell.set_property("stock-size", size)

        elif column_type == Cell.Text:
            cell = Gtk.CellRendererText()

            # Properties
            column.pack_start(cell, True)
            column.add_attribute(cell, "markup", index)

        # ==========================================
        #   Append column to treeview
        # ==========================================

        self.treeview.append_column(column)


    def clear(self):
        """
        Clear data from treeview
        """

        self.store.clear()


    def append(self, values):
        """
        Append data into treeview
        """

        if type(values) is not list:
            raise(ValueError("Expected a list as values"))

        if len(values) > len(self.columns_type):
            raise(ValueError("Wrong data sequence"))

        self.store.append(values)
