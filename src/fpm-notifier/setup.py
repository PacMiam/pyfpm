#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages

from os.path import join
from os.path import dirname

setup(
    name = "fpm-notifier",
    version = "1.3rc2",
    author = "PacMiam",
    author_email = "PacMiam@gmx.fr",
    description = "Packages upgrade notifier for Frugalware",
    url = 'https://frugalware.org',
    license = "GPLv3",

    packages = ['fpm_notifier', 'fpm_notifier.gtkui', 'fpm_notifier.data'],
    include_package_data = True,

    entry_points = {
        'console_scripts': [
            'fpm-notifier = fpm_notifier.main:main',
        ],
    },

    classifiers = [
        "Programming Language :: Python :: 2.7",
        "Development Status :: Stable",
        "License :: GPL3",
        "Operating System :: GNU/Linux",
        "Topic :: Communications",
        "Topic :: Utilities",
    ],
)

