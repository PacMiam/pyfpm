# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                          main.py
#
#  Gtk+2 front-end
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from sys import argv
from sys import exit as sys_exit
from argparse import ArgumentParser

# Pyfpm
from fpm_gui import __version__
from fpm_gui import __copyrigth__

from fpmd.tools.utils import check_xorg
from fpmd.tools.utils import getTranslation
from fpmd.tools.package import start_daemon

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-notifier")

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def main (*args):

    # ------------------------------------
    #   Arguments
    # ------------------------------------

    if len(argv) >= 1:

        parser = ArgumentParser(
            description="Packages upgrade notifier for Frugalware",
            epilog=__copyrigth__, conflict_handler="resolve")

        parser.add_argument("-v", "--version", action="version",
            version="fpm-notifier %s Licence GPLv3" % __version__,
            help="show the current version")
        parser.add_argument("-d", "--debug", action="store_true",
            help="use debug mode")

        args = parser.parse_args()

    # ------------------------------------
    #   Graphical session
    # ------------------------------------

    if check_xorg():

        # ------------------------------------
        #   Check FPMd connection
        # ------------------------------------

        if start_daemon() is not None:

            # ------------------------------------
            #   Start pyfun interface
            # ------------------------------------

            try:
                from gtkui.interface import Interface

                Interface(args.debug)

            except Exception as error:
                return _("Can't open fpm-notifier window: %s" % (str(error)))

    else:
        return _("No display available")


if __name__ == "__main__":
    sys_exit(main())
