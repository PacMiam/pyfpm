# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                          interface.py
#
#  Gtk+2 front-end - Main interface
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Files
from os import remove

# Interface
import gtk
import pango
from gobject import threads_init
from gobject import timeout_add_seconds

from notify2 import Notification
from notify2 import init as notifyInit

# Log
from logging import getLogger

# Pyfpm
from fpm_gui import *
from fpm_gui import __version__

from fpm_gui.gtkui.window import WindowPopup
from fpm_gui.gtkui.window_vte import WindowVte
from fpm_gui.gtkui.modules import createScript

from fpmd.tools.config import Config
from fpmd.tools.utils import getPixmap
from fpmd.tools.utils import getTranslation
from fpmd.tools.package import Command
from fpmd.tools.package import Database

# Pyfun
from window import WindowAbout

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-notifier")

LOG = getLogger()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Interface (object):

    def __init__ (self, debugMode):
        """
        Constructor
        """

        threads_init()

        # ------------------------------------
        #   Private variables
        # ------------------------------------

        self.debug = debugMode

        self.__update_max = 10

        self.__listPackages = list()

        # ------------------------------------
        #   Interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        # ------------------------------------
        #   Icons
        # ------------------------------------

        # Get user icon theme
        self.__icontheme = gtk.icon_theme_get_default()

        # ------------------------------------
        #   Notification icon
        # ------------------------------------

        self.__status_icon = gtk.status_icon_new_from_icon_name(ICON_AVAILABLE)

        # Properties
        self.__status_icon.set_title("fpm-notifier")
        self.__status_icon.set_name("fpm-notifier")

        # ------------------------------------
        #   Status menu
        # ------------------------------------

        self.__status_menu = gtk.Menu()

        self.menu_refresh = gtk.ImageMenuItem(_("Refresh databases"))
        self.menu_upgrade = gtk.ImageMenuItem(_("Upgrade system"))
        self.menu_separator = gtk.SeparatorMenuItem()
        self.menu_about = gtk.ImageMenuItem(_("About"))
        self.menu_quit = gtk.ImageMenuItem(_("Quit"))

        self.__cache_menu = [self.menu_refresh, self.menu_upgrade,
            self.menu_separator, self.menu_about, self.menu_quit]

        # Properties
        self.menu_refresh.set_image(gtk.image_new_from_stock(
            gtk.STOCK_REFRESH, gtk.ICON_SIZE_MENU))
        self.menu_upgrade.set_image(gtk.image_new_from_stock(
            gtk.STOCK_APPLY, gtk.ICON_SIZE_MENU))
        self.menu_about.set_image(gtk.image_new_from_stock(
            gtk.STOCK_ABOUT, gtk.ICON_SIZE_MENU))
        self.menu_quit.set_image(gtk.image_new_from_stock(
            gtk.STOCK_QUIT, gtk.ICON_SIZE_MENU))

        self.__status_menu.append(self.menu_refresh)
        self.__status_menu.append(self.menu_upgrade)
        self.__status_menu.append(self.menu_separator)
        self.__status_menu.append(self.menu_about)
        self.__status_menu.append(self.menu_quit)

        self.__status_menu.show_all()

        self.menu_upgrade.hide()


    def __init_signals (self):
        """
        Connect signals to main interface
        """

        self.__status_icon.connect("popup-menu", self.menu_show)

        self.menu_refresh.connect("activate", self.databases_update)
        self.menu_upgrade.connect("activate", self.system_sync)
        self.menu_about.connect("activate", WindowAbout, self)
        self.menu_quit.connect("activate", self.__close_interface)


    def __start_interface (self):
        """
        Load all interface elements
        """

        # ------------------------------------
        #   Database
        # ------------------------------------

        # Create a database instance to access to fpmd
        self.__pacman = Database()
        self.__pacman.debug = self.debug

        # Reload fpmd data
        self.__pacman.reload_daemon()

        # ------------------------------------
        #   Notification
        # ------------------------------------

        try:
            notifyInit("fpm-notifier")

        except Exception as err:
            LOG.error(_("Can't access to notification daemon: %s") % str(err))

        # ------------------------------------
        #   Upgrade
        # ------------------------------------

        self.databases_update()

        timeout_add_seconds(3600, self.databases_update)

        gtk.main()


    def __close_interface (self, widget, *event):
        """
        Terminate fpm-notifier instance
        """

        gtk.main_quit()

    # ------------------------------------
    #   Interface functions
    # ------------------------------------

    def reload_interface (self):
        """
        Reset all the interface
        """

        self.__listPackages = list()

        for widget in self.__status_menu.get_children():
            widget.set_sensitive(True)

        # Reset status icon data
        self.__status_icon.set_from_icon_name(ICON_AVAILABLE)
        self.__status_icon.set_tooltip_text("")

        self.refresh()

        # Get update packages list
        listUpdate = self.__pacman.packages_upgrade()
        for name, version in listUpdate:
            self.__listPackages.append(name)

        if len(listUpdate) > 0:

            text = _("There is 1 update available.")
            if len(listUpdate) > 1:
                text = _("There are %d updates available.") % len(listUpdate)

            self.notification_show(
                _("fpm-notifier"), text, ICON_UPGRADE_FALLBACK)

            self.__status_icon.set_from_icon_name(ICON_UPGRADE_FALLBACK)
            self.__status_icon.set_tooltip_text(text)

            listMenu = listUpdate
            if len(listUpdate) > self.__update_max:
                listMenu = listUpdate[0:self.__update_max]
                more_packages = True

            else:
                more_packages = False

            self.__status_menu.insert(gtk.SeparatorMenuItem(), 0)

            # Show available packages
            index = 0
            for name, version in listMenu:
                menu = gtk.ImageMenuItem("%s - %s" % (name, version))
                menu.set_image(gtk.image_new_from_pixbuf(self.icon_load(
                    name, fallback=ICON_AVAILABLE)))

                self.__status_menu.insert(menu, index)
                self.__status_menu.show_all()

                index += 1

            # Create a submenu with missing packages
            if more_packages:
                submenu = gtk.Menu()
                submenu_label = gtk.MenuItem("...")

                self.__status_menu.insert(submenu_label, index)

                submenu_label.set_submenu(submenu)

                index = 0
                for name, version in listUpdate[self.__update_max:]:
                    menu = gtk.ImageMenuItem("%s - %s" % (name, version))
                    menu.set_image(gtk.image_new_from_pixbuf(self.icon_load(
                        name, fallback=ICON_AVAILABLE)))

                    submenu.insert(menu, index)

                    index += 1

            self.__status_menu.show_all()

        else:
            text = _("Your system is up-to-date.")

            self.notification_show(_("fpm-notifier"), text, ICON_AVAILABLE)

            self.__status_icon.set_from_icon_name(ICON_AVAILABLE)
            self.__status_icon.set_tooltip_text(text)

            self.__status_menu.insert(gtk.MenuItem(text), 0)
            self.__status_menu.insert(gtk.SeparatorMenuItem(), 1)


    def menu_show (self, widget, eventButton, eventTime):
        """
        Show context menu when user right click on notification icon
        """

        self.__status_menu.popup(None, None, gtk.status_icon_position_menu,
            eventButton, eventTime, widget)

        self.__status_menu.show()


    def notification_show (self, title, description, icon=""):
        """
        Send a notification
        """

        try:
            notify = Notification(title, description, icon)
            notify.show()

        except:
            pass


    def icon_load (self, icon, size=16, fallback="image-missing"):
        """
        Get an icon from data folder
        """

        try:
            return self.__icontheme.load_icon(icon, size, 0)

        except:
            return self.__icontheme.load_icon(fallback, size, 0)


    @staticmethod
    def refresh ():
        """
        Refresh the interface
        """

        try :
            while gtk.events_pending():
                gtk.main_iteration()
        except:
            pass

    # ------------------------------------
    #   FPMd functions
    # ------------------------------------

    def databases_update (self, widget=None, *event):
        """
        Update pacman-g2 databases
        """

        for widget in self.__status_menu.get_children():
            if not widget in self.__cache_menu:
                self.__status_menu.remove(widget)

        self.menu_upgrade.hide()
        self.menu_refresh.set_sensitive(False)

        self.__pacman.system_upgrade()

        self.reload_interface()

        return True


    def system_sync (self, widget=None):
        """
        Install available update
        """

        action = Command()
        action.sync()

        self.menu_upgrade.hide()

        self.menu_refresh.set_sensitive(False)
        self.menu_quit.set_sensitive(False)

        flags = dict(list_installation=self.__listPackages,
            install_noconfirm=True, install_force=True)

        if action.generate(flags):
            scriptFile = createScript(CONFIGPATH, action.command)

            termVte = WindowVte(None, _("Packages installer"),
                _("Using <b>%s</b> as installer script") % (
                scriptFile), "sh %s" % scriptFile, True)

            remove(scriptFile)

            if termVte.success:
                if self.__pacman.system_reboot_needed(flags):
                    WindowPopup(_("Reboot is neccessary"), _("Some packages "
                        "from base group have been update. You may reboot "
                        "your system."), gtk.MESSAGE_WARNING)

                if "fpmd" in self.__listPackages:
                    WindowPopup(_("fpmd has been updated"), _("fpmd has been "
                        "updated on your system. To avoid problems, fpmd must "
                        "be close. Sorry for convenient."), gtk.MESSAGE_WARNING)

                    sys_exit(_("fpmd has been updated"))

                self.__pacman.reload_daemon()

                self.databases_update()

        else:
            WindowPopup(_("Error during command generation"),
                _("Can't generate command for %s") % "system_sync",
                gtk.MESSAGE_ERROR)

        self.reload_interface()
