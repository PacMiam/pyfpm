# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                       window.py
#
#  Gtk+2 front-end - Pyfun extra windows
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# Interface
import gtk

# Log
from logging import getLogger

# Pyfpm
from fpm_gui import __version__
from fpm_gui import __website__
from fpm_gui import __copyrigth__

from fpmd.tools.utils import getTranslation

# ------------------------------------------------------------------
#   Global variables
# ------------------------------------------------------------------

_ = getTranslation("fpm-notifier")

LOG = getLogger("fpm-notifier")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class WindowPackages (object):

    def __init__ (self, widget, parent):
        """
        Constructor
        """

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.mainWindow = gtk.Window()

        self.mainWindow.set_title("fpm-notifier")
        self.mainWindow.set_wmclass("fpm-notifier", "fpm-notifier")
        self.mainWindow.set_role("fpm-notifier")

        self.mainWindow.set_icon_name("fpm-notifier")

        self.mainWindow.set_default_size(500, 700)

        # ------------------------------------
        #   Main grids
        # ------------------------------------

        mainGrid = gtk.VBox()
        titleGrid = gtk.Table()

        titleGrid.set_row_spacings(2)
        titleGrid.set_border_width(6)

        # ------------------------------------
        #   Main title
        # ------------------------------------

        mainWindowIcon = gtk.Image()
        mainWindowIconAlignment = gtk.Alignment(0, 0, 0, 0)

        mainWindowTitle = gtk.Label()
        mainWindowDescription = gtk.Label("test")

        mainWindowIconAlignment.add(mainWindowIcon)
        mainWindowIcon.set_from_pixbuf(
            self.getIcon(ICON_UPGRADE, 48, ICON_UPGRADE_FALLBACK))

        mainWindowTitle.set_alignment(0, 0.5)
        mainWindowTitle.set_use_markup(True)
        mainWindowTitle.set_markup("<span font='18'><b>%s</b></span>" % \
            _("Updates availables"))

        mainWindowDescription.set_alignment(0, 0.5)

        # ------------------------------------
        #   Main toolbar
        # ------------------------------------

        toolbarMenu = [
            [gtk.STOCK_APPLY, _("Intall updates"), None],
            [gtk.STOCK_REFRESH, _("Refresh databases"), None],
            [],
            [gtk.STOCK_PREFERENCES, _("Preferences"), None],
            [],
            [gtk.STOCK_QUIT, _("Quit"), None]]

        mainToolbar = gtk.Toolbar()

        mainToolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        mainToolbar.set_style(gtk.TOOLBAR_ICONS)

        for entry in toolbarMenu:
            if len(entry) > 0:
                mainToolbar.insert_stock(
                    entry[0], entry[1], None, entry[2], None, -1)
            else:
                mainToolbar.insert_space(-1)

        # ------------------------------------
        #   Main cellview
        # ------------------------------------

        self.mainTreeview = gtk.TreeView(gtk.ListStore(
            bool,                   # Status
            gtk.gdk.Pixbuf,         # Icon
            str,                    # Name
            str))                   # Version

        columnPackage = gtk.TreeViewColumn(_("Package name"))
        cellCheckbox = gtk.CellRendererToggle()
        cellIcon = gtk.CellRendererPixbuf()
        cellText = gtk.CellRendererText()

        columnVersion = gtk.TreeViewColumn(_("Version"))
        cellVersion = gtk.CellRendererText()

        mainTreeviewScroll = gtk.ScrolledWindow()

        columnPackage.pack_start(cellCheckbox, False)
        columnPackage.add_attribute(cellCheckbox, "active", 0)
        columnPackage.set_attributes(cellCheckbox, active=0)
        columnPackage.pack_start(cellIcon, False)
        columnPackage.add_attribute(cellIcon, "pixbuf", 1)
        columnPackage.pack_start(cellText, True)
        columnPackage.add_attribute(cellText, "markup", 2)

        columnVersion.pack_start(cellVersion, False)
        columnVersion.add_attribute(cellVersion, "markup", 3)

        columnPackage.set_sort_column_id(2)
        columnVersion.set_sort_column_id(3)

        self.mainTreeview.append_column(columnPackage)
        self.mainTreeview.append_column(columnVersion)

        self.mainTreeview.set_headers_visible(False)
        self.mainTreeview.set_enable_search(False)
        self.mainTreeview.set_search_column(1)

        mainTreeviewScroll.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        mainTreeviewScroll.add_with_viewport(self.mainTreeview)
        mainTreeviewScroll.set_border_width(6)
        mainTreeviewScroll.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Main statubar
        # ------------------------------------

        self.mainStatusbar = gtk.Statusbar()

        # ------------------------------------
        #   Manage widgets
        # ------------------------------------

        # Window
        titleGrid.attach(mainWindowIconAlignment, 0, 1, 0, 2,
            xoptions=gtk.SHRINK, yoptions=gtk.FILL, xpadding=4)
        titleGrid.attach(mainWindowTitle, 1, 2, 0, 1,
            yoptions=gtk.FILL)
        titleGrid.attach(mainWindowDescription, 1, 2, 1, 2,
            yoptions=gtk.FILL, xpadding=4)

        mainGrid.pack_start(titleGrid, False, False)
        mainGrid.pack_start(mainToolbar, False, False)
        mainGrid.pack_start(mainTreeviewScroll)
        mainGrid.pack_start(self.mainStatusbar, False, False)

        self.mainWindow.add(mainGrid)

        self.mainWindow.show_all()


class WindowAbout (gtk.AboutDialog):

    def __init__ (self, widget, parent):
        """
        Constructor
        """

        gtk.AboutDialog.__init__(self)

        self.set_program_name("fpm-notifier")
        self.set_version(__version__)
        self.set_logo(parent.icon_load("fpm-notifier", 96))
        self.set_comments(_("Packages upgrade notifier for Frugalware"))
        self.set_website(__website__)
        self.set_copyright(__copyrigth__)

        # Authors
        self.set_authors(
            [_("Active developpers"),
            "\tAurélien Lubert (PacMiam)",
            _("Logo"),
            "\tTango Desktop Project (GPLv3)"])

        # License
        self.set_license("fpm-notifier is free software: you can "
            "redistribute it and/or modify it under the terms of the GNU "
            "General Public License as published by the Free Software "
            "Foundation, either version 3 of the License, or (at your option) "
            "any later version.\n\nfpm-notifier is distributed in the "
            "hope that it will be useful, but WITHOUT ANY WARRANTY; without "
            "even the implied warranty of MERCHANTABILITY or FITNESS FOR A "
            "PARTICULAR PURPOSE. See the GNU General Public License for more "
            "details.\n\nCheck /usr/share/doc/pyfun-%s/LICENSE for more "
            "informations.\n\nhttp://www.gnu.org/licenses/" % __version__)
        self.set_wrap_license(True)

        self.run()


        self.destroy()
