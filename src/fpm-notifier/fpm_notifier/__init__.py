# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#                           __init__.py
#
#  PyFUN - Common values
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Informations
# ------------------------------------------------------------------

__version__ = "1.3rc2"
__website__ = "http://www.frugalware.org"
__copyrigth__ = "(C) 2012-2015 Frugalware Developer Team (GPLv3)"
